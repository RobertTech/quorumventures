<?php
/**
 * @package ZT Highslide Plugin for Joomla! 
 * @author http://www.ZooTemplate.com
 * @copyright (C) 2011- ZooTemplate.com
 * @license PHP files are GNU/GPL
**/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.plugin.plugin'); 
class plgContentZt_Highslide extends JPlugin{
	function plgContentZt_Highslide( &$subject, $params )
	{
		parent::__construct( $subject, $params );
	}
	function onPrepareContent($article){
		//Get plugin info
		$plugin = JPluginHelper::getPlugin('content','zt_highslide'); 
		$pluginParams	= new JRegistry( $plugin->params); 
		//Simple check to determine whether to continue or not
		if ( JString::strpos( $article->text, 'zt_highslide' ) === false ) {
			return true;
		} 
		require_once('plugins/content/zt_highslide/zt_highslide/hssyntaxparser.php'); 
		
		$this->hsSettings($pluginParams);
		
		$parser = new HighslideParser;
		$article->text = $parser->parse($article->text, array(&$this, 'hsTypeSelect'));
		return $article->text;		
	}
	function onContentPrepare($context, &$article, &$params, $page = 0){
		//Get plugin info
		$plugin = JPluginHelper::getPlugin('content','zt_highslide'); 
		$pluginParams	= new JRegistry( $plugin->params); 
		
		//Simple check to determine whether to continue or not
		if ( JString::strpos( $article->text, 'zt_highslide' ) === false ) {
			return true;
		} 
		require_once('plugins/content/zt_highslide/zt_highslide/hssyntaxparser.php'); 
		
		$this->hsSettings($pluginParams);
		
		$parser = new HighslideParser;
		$article->text = $parser->parse($article->text, array(&$this, 'hsTypeSelect'));
		return $article->text;		
	}
	function hsTypeSelect($attributeArr, $plgText)
	{
		global $_paththum;
		$paras = ''; 
		$overrideArr ='';
		$paras = $this->parseAttributes($attributeArr);

		foreach($paras as $k=>$value){
			$arr_type = array('type', 'url', 'src', 'repeat', 'contentid', 'interval', 'images_folder', 'position', 'class');
			if(!in_array(trim($k), $arr_type)){
				if(is_numeric($value)){
					$overrideArr .= $k.":".$value.",";
				}
				else  $overrideArr .= $k.":'".$value."',";
			}
		}
		if($overrideArr!=''){
			$overrideArr = substr($overrideArr, 0, strlen($overrideArr)-1);
		}
		$paras['overrideArr'] = $overrideArr;
		
		if(!isset($paras['url'])) $paras['url'] = '';
		
		if(isset($paras['src']) && $paras['src']!='') $paras['url'] = $paras['src'];
		
		$id_number = 'href_'.rand(10000, 99999).rand(1000, 9999).rand(100, 999);
		$id_li = "'li_".$id_number."'";
		$id = "'highslide-html-".$id_number."'";
		if (!isset($paras['type'])) {
			$paras['type'] = '';
		}
		switch($paras['type']){
			case "img":	
				return $this->typeHsImage($paras, $plgText, $id_number, $_paththum);
			case "slideshow-caption":
				return $this->typeHsSlideshowCaption($paras, $plgText, $id_number, $_paththum);
			case "gallery":
				return $this->typeHsGallery($paras, $plgText, $id_number, $_paththum);	
			case "html":
				return $this->typeHsHtml($paras, $plgText, $id_number, $id, $id_li);
			case "flash":				
				return $this->typeHsFlash($paras, $plgText, $id_number, $id, $_paththum);
			case "iframe":
				return $this->typeHsIframe($paras, $plgText, $id_number, $id);
			case "modules":				
				return $this->typeHsModules($paras, $plgText, $id_number, $id, $id_li);
		}
	}
	
	function hsSettings($pluginParams){ 
		global $app; 
		$document =& JFactory::getDocument();
		$hs_base = JURI::base().'plugins/content'; 
		if($pluginParams->get('showClose') == "1")
			$document->addStyleSheet($hs_base.'/zt_highslide/zt_highslide/css/showClose.css');
		else 
			$document->addStyleSheet($hs_base.'/zt_highslide/zt_highslide/css/hideClose.css');
		if($pluginParams->get('showMove') == "1")
			$document->addStyleSheet($hs_base.'/zt_highslide/zt_highslide/css/showMove.css');
		else
			$document->addStyleSheet($hs_base.'/zt_highslide/zt_highslide/css/hideMove.css');
		if($pluginParams->get('control_type') == "icon")
			$document->addStyleSheet($hs_base.'/zt_highslide/zt_highslide/css/iconControl.css');
		
		$document->addStyleSheet($hs_base.'/zt_highslide/zt_highslide/css/highslide-styles.css');
		$document->addStyleSheet($hs_base.'/zt_highslide/zt_highslide/css/highslide.css');
		//$document->addScript($hs_base."/zt_highslide/zt_highslide/js/highslide-full.packed.js");
		$document->addScript($hs_base."/zt_highslide/zt_highslide/js/highslide-full.js");
		$document->addScript($hs_base."/zt_highslide/zt_highslide/js/highslide.config.js");
		$document->addScript($hs_base."/zt_highslide/zt_highslide/js/more_functions.js");
		$document->addScript($hs_base."/zt_highslide/zt_highslide/js/swfobject.js");
		$document->addScriptDeclaration("hs.graphicsDir = '".$hs_base."/zt_highslide/zt_highslide/graphics/'; hs.showCredits = false; hs.align = '".$pluginParams->get('pu_position')."'; hs.dimmingOpacity =". (float)trim($pluginParams->get('dimming_opac'))."; ");
		 
		if($pluginParams->get('outlineType') == "0")		$document->addScriptDeclaration(" hs.outlineType = '';");
		elseif($pluginParams->get('outlineType') == "1")	$document->addScriptDeclaration("hs.outlineType = 'outer-glow';");
		elseif($pluginParams->get('outlineType') == "2")	$document->addScriptDeclaration("hs.outlineType = 'rounded-white';");
		elseif($pluginParams->get('outlineType') == "3")	$document->addScriptDeclaration("hs.outlineType = 'drop-shadow';");
		elseif($pluginParams->get('outlineType') == "4")	$document->addScriptDeclaration("hs.outlineType = 'beveled';");
		elseif($pluginParams->get('outlineType') == "5")	$document->addScriptDeclaration("hs.outlineType = 'glossy-dark';");
		elseif($pluginParams->get('outlineType') == "6")	$document->addScriptDeclaration("hs.outlineType = 'rounded-black';"); 

		
		$paththumb = '';
		$GLOBALS['_paththum'] = $paththumb;
	}
	
	function parseAttributes($attributes) {
		$attributes = html_entity_decode ($attributes, ENT_QUOTES);
		$regex = "/\s*([^=\s]+)\s*=\s*('([^']*)'|\"([^\"]*)\"|([^\s]*))/";
		preg_match_all($regex, $attributes, $matches);
		
		 $array_of_attributes = null;
		 if(count($matches)){
			$array_of_attributes = array();
				for ($i=0;$i<count($matches[1]);$i++){ 
				  $key = $matches[1][$i];
				  $val = $matches[3][$i]?$matches[3][$i]:($matches[4][$i]?$matches[4][$i]:$matches[5][$i]);
				  $array_of_attributes[$key] = $val;
				}
		  }
		  return $array_of_attributes;
	}
	
	function hsNoImage($id_number){		
		$hsImage = '<div class="highslide-html-content" id="highslide-html-'.$id_number.'">
							<div class="highslide-header">
								<ul>
									<li class="highslide-move">
										<a href="#" onclick="return false">Move</a>
									</li>
									<li class="highslide-close">
										<a href="#" onclick="return hs.close(this)">Close</a>
									</li>
								</ul>	    
							</div>
							<div class="highslide-body" style="text-align:center;width:90%;">
								<br/>
								<span style="color:red; font-weight:bold; font-size: 14px;">Image not found!!!</span>
								<br/>								
							</div>
							<div class="highslide-footer">
								<div>
									<span class="highslide-resize" title="Resize">
										<span></span>
									</span>
								</div>
							</div>
						</div>';
		return $hsImage;
	}
	
	function isImage( $ext ){
		$image_types =  array(1 => "gif", "jpeg", "png", "jpg" ); 
		return in_array( $ext, $image_types );
	}

	function typeHsImage($paras, $plgText, $id_number, $_paththum){
		$rootPath = JPATH_SITE.DS;
		$img_path = trim($paras['url']);
		$hsImage = '';
		$img_thumb = $img_path;
		$width = isset($paras['width'])?'width="'.$paras['width'].'"':'';
		$paras['url'] = trim($paras['url']);
		
		if(substr($img_path,0,4)!='http'){
			$img_thumb = $img_path = JURI::base().$_paththum.$paras['url'];
		}
		if(substr($img_path,0,4)!='http' && !is_file($rootPath.$_paththum.$paras['url'])){			
			$id = "'highslide-html-".$id_number."'";
			$hsImage .='<a id="'.$id_number.'" class="highslide" href="#"  onclick="return hs.htmlExpand(this, {contentId: '.$id.'}); return false;">';
			$hsImage .= $plgText;
			if(!isset($paras['display']) || $paras['display']!='none'){
				$class = isset($paras['class'])?$paras['class']:'';
				$hsImage .= '<img '.$width.'  alt="'.$paras['url'].'" src="'.$img_path.'"  class="'.$class.'" />';
			}
			$hsImage .= '</a>';
			$hsImage .= $this->hsNoImage($id_number);
		}
		else{
			
			$hsImage ='<div>
						<a id="'.$id_number.'" class="highslide" href="'.$img_path.'" 
							onclick="return hs.expand(this, {'.$paras['overrideArr'].'});">';
			if(!isset($paras['display']) || ($paras['display']!='none')){
				$class = isset($paras['class'])?$paras['class']:'';
				$hsImage .= '<img  alt="'.$paras['url'].'" src="'.$img_thumb.'"  class="'.$class.'" '.$width.' />';
			}
			$hsImage .= $plgText.'</a>';			
			$hsImage .= '</div>';
		}
		return $hsImage;
	}
	
	function typeHsFlash($paras, $plugText, $id_number, $id, $_paththum){
		$rootPath = JPATH_SITE.DS;
		$name_flash = $paras['url'];
		if(substr($name_flash,0,4)!='http'){
			$name_flash_uri = JURI::base().$_paththum.$name_flash;
		} else {
			$name_flash_uri=$name_flash;
		}
		$html = '';
		
		if($paras['overrideArr']!='') $paras['overrideArr'] = ','.$paras['overrideArr'];
		if(substr($name_flash,0,4)!='http' && !is_file($rootPath.$_paththum.$name_flash)){			
			$html .='<a id="'.$id_number.'" class="highslide" href="#"  onclick="return hs.htmlExpand(this, {contentId: '.$id.'}); return false;">';
			$html .= $plugText;			
			$html .= '</a>';
			$html .= $this->hsNoImage($id_number);
		}
		else{
			$swfObjectOptions = '{ 
								   maincontentId: '.$id.',	
								   objectType: \'swf\',
								   allowSizeReduction: false,
								   outlineType: \'rounded-white\',
								   
								   wmode:\'transparent\',
								   preserveContent: true, 
								   objectWidth: '.$paras['width'].', 
								   objectHeight: '.$paras['height'].',
								   maincontentText: \''.JTEXT::_('You need to upgrade your flash player').'\' '.$paras['overrideArr'].' } ';
								   
		echo  '
		<script type="text/javascript">
		function good(){
		hs.skin.contentWrapper =
	\'<div id="flash" class="highslide-header">  \'+
		\'<h1>This is a custom header</h1>\'+
		\'<a href="#" title="{hs.lang.closeTitle}" \'+
				\'onclick="return hs.close(this)">\'+
			\'<span>More</span></a>\'+
	\'</div>\'+
	\'<div class="highslide-body"></div>\'+
	\'<div class="highslide-footer"><div>\'+
		\'<b>This is a custom footer</b>\'+
	\'</div></div>\' }</script>';
			$html.= '<div>
					    <a href="'.$name_flash_uri.'" id="'.$id_number.'" onclick="return hs.htmlExpand(this, '.$swfObjectOptions.'); " class="highslide ">
						'.$plugText.'
					    </a>
				    </div>';
		}

		//$html .= '<div id='.$id.'>jadsdsd </div>';
		return $html;
	}
	
	function typeHsSlideshowCaption($paras, $content, $id_number, $_paththum){
		$rootPath = '';
		$rootPath = JPATH_SITE.DS;		
		if (strpos($paras['url'], ",")===false) {
			$imgArr = preg_split ('/\n/', $paras['url']);
		}
		else{
			$imgArr = preg_split ('/,/', $paras['url']);
		}
		$id = "'highslide-html-".$id_number."'";
		$html = '<div>';
		if ($paras['overrideArr']=='') {
			$paras['overrideArr'] = "outlineType:'rounded-white'";
		}
		if(!isset($paras['images_folder'])) $paras['images_folder'] = '';
		
		for($i=0; $i<count($imgArr); $i++){
			$img_path  = trim($imgArr[$i]);
			$img_thumb = trim($imgArr[$i]);
			if( substr($img_path,0,4)!='http'){
				$img_thumb = JURI::base().$paras['images_folder'].$img_path;
			}
			$ext = strtolower(substr(strrchr($img_path, '.'), 1)); 
			if( !$this->isImage(strtolower($ext)) ){ continue; }
			$width = isset($paras['width'])?'width="'.$paras['width'].'"':'';
			if( substr($img_path,0,4)!='http' && !is_file( $rootPath.$paras['images_folder'].$img_path ) ){
				$html .='<a  id="thumb'.$i.'" class="highslide" href="#"  onclick="return hs.htmlExpand(this, {contentId: '.$id.'}); return false;">';
				$html .= 	'<img src="'.JURI::base().$paras['images_folder'].$img_path.'" alt="'.$img_path.'" title="Click to enlarge"    '.$width .' />';			
				$html .= '</a>';
				echo $this->hsNoImage($id_number);
			}
			else{
				$html .= '<a  id="thumbnail_'.$i.'" href="'.$img_thumb.'" style="padding-right:5px;"	
								class="highslide" onclick="return hs.expand(this,{'.$paras['overrideArr'].',captionId:\''.$id_number.'\', slideshowGroup:\''.$id_number.'\'});">';
				$html .= 	'<img src="'.$img_thumb.'" alt="'.$img_thumb.'" title="Click to enlarge"   '.$width.' />';
				$html .= '</a>';
			}
		}
		
		//Get plugin info
		$pluginHtml = JPluginHelper::getPlugin('content','zt_highslide'); 
		$pluginParams	= new JRegistry( $pluginHtml->params); 
		//echo $pluginParams->get('control_type'); die();
		
 $html .= '<div class="highslide-caption" id="'.$id_number.'">';
		if($pluginParams->get('control_type') == "icon") 
				$html .= '<style type="text/css">
						.highslide-caption #prev_zoo{
							background: url(../plugins/content/zt_highslide/zt_highslide/graphics/previous.png) no-repeat;
							background-position: 0 0px;
							margin: 1px 2px 1px 10px;
							display: block;
							width: 32px;
							height: 32px;
							text-indent: 10000px;
						}
						.highslide-caption #next_zoo{
							background: url(../plugins/content/zt_highslide/zt_highslide/graphics/next.png) no-repeat;
							background-position: 0 0px;
							margin: 1px 2px 1px 10px;
							display: block;
							width: 32px;
							height: 32px;
							text-indent: 10000px;
						}
						.highslide-caption #close_zoo {	
							background: url(../plugins/content/zt_highslide/zt_highslide/graphics/close.png) no-repeat;
							background-position: 0 2px;
							margin: 1px 2px 1px 10px;
							display: block;
							width: 32px;
							height: 32px;
							text-indent: 10000px;
						}
						.highslide-caption #move_zoo {	
							background: url(../plugins/content/zt_highslide/zt_highslide/graphics/move.png) no-repeat;
							background-position: 0 0px;
							margin: 1px 2px 1px 10px;
							display: block;
							width: 32px;
							height: 32px;
							text-indent: 10000px;
						}
					</style>'; 

		$html .= '
			<a href="#" id="prev_zoo" onclick="return hs.previous(this); return false;" class="control" style="float:left; display: block">
				Previous
			</a>
			<a href="#" id="next_zoo" onclick="return hs.next(this); return false;" class="control" 
					style="float:left; display: block; text-align: right; margin-left: 50px">
				Next
			</a>';
						
		if($pluginParams->get('showClose') == "1"){ 
			$html .= '<a href="#" id="close_zoo" onclick="return hs.close(this); return false;" class="control">Close</a>';
		}		
		if($pluginParams->get('showMove') == "1"){
			$html .= '<a href="#" id="move_zoo" onclick="return false" class="highslide-move control">Move</a>';
		}				
		$html .= 	'<div style="clear:both">  </div>
					</div>
					</div>';		
	
		return $html;
	}
	
	function typeHsGallery($paras, $content, $id_number, $_paththum){
		$rootPath = '';
		$rootPath = JPATH_SITE.DS;		
		if (strpos($paras['url'], ",")===false) {
			$imgArr = preg_split ('/\n/', $paras['url']);
		}
		else{
			$imgArr = preg_split ('/,/', $paras['url']);
		}
		$id = "'highslide-html-".$id_number."'";
		$html = "";
		if (!isset($paras['interval'])) $paras['interval'] = 3000;
		if (!isset($paras['repeat'])) $paras['repeat'] = 'true'; //for case: custom syntax in content of zt_highslide does not have 'repeat' attribute 
		if ($paras['repeat']==1) $paras['repeat'] = 'true';
			else $paras['repeat'] = 'false';
		$html = '<script type="text/javascript" > 
		hs.addSlideshow({
			slideshowGroup: \''.$id_number.'\',
			interval: '.$paras['interval'].',
			repeat: '.$paras['repeat'].',
			useControls: true,
			fixedControls: \'fit\',
			overlayOptions: {
				className: \'text-controls\',
				opacity: \'0.95\',
				position: \'bottom center\',
				offsetX: \'0\',
				offsetY: \'-60\',
				relativeTo: \'viewport\',
				hideOnMouseOut: false
			},
			thumbstrip: {
				mode: \'horizontal\',
				position: \'bottom center\',
				relativeTo: \'viewport\'
			}

		}); </script>';
		$html .= '<div class="highslide-gallery"><ul>';
		if ($paras['overrideArr']=='') {
			$paras['overrideArr'] = "outlineType:'rounded-white'";
		}
		if(!isset($paras['images_folder'])) $paras['images_folder'] = '';
		
		for($i=0; $i<count($imgArr); $i++){
			$img_path  = trim($imgArr[$i]);
			$img_thumb = trim($imgArr[$i]);
			if( substr($img_path,0,4)!='http'){
				$img_thumb = JURI::base().$paras['images_folder'].$img_path;
			}
			$ext = strtolower(substr(strrchr($img_path, '.'), 1)); 
			if( !$this->isImage(strtolower($ext)) ){ continue; }
			$width = isset($paras['width'])?'width="'.$paras['width'].'"':'';
			if( substr($img_path,0,4)!='http' && !is_file( $rootPath.$paras['images_folder'].$img_path ) ){
				$html .='<a  id="thumb'.$i.'" class="highslide" href="#"  onclick="return hs.htmlExpand(this, {contentId: '.$id.'}); return false;">';
				$html .= 	'<img src="'.JURI::base().$paras['images_folder'].$img_path.'" alt="'.$img_path.'" title="Click to enlarge"    '.$width .' />';			
				$html .= '</a>';
				echo $this->hsNoImage($id_number);
			}
			else{
				$html .= '<li><a  id="thumbnail_'.$i.'" href="'.$img_thumb.'" style="padding-right:5px;"	
								class="highslide" onclick="return hs.expand(this,{'.$paras['overrideArr'].', numberPosition: \'heading\', transitions: [\'expand\', \'crossfade\'], slideshowGroup:\''.$id_number.'\'});">';
				$html .= 	'<img src="'.$img_thumb.'" alt="'.$img_thumb.'" title="Click to enlarge"   '.$width.' />';
				$html .= '</a></li>';
			}
		}
		$html .= 	'</ul><div style="clear:both"></div></div>';		
	
		return $html;
	}
	
	function typeHsIframe($paras, $content, $id_number, $id){
		if($paras['overrideArr']!='') $paras['overrideArr'] = ','.$paras['overrideArr'];
		
		$html  = '<div>';
		$html .= '<a id="'.$id_number.'" href="'.$paras['url'].'" onclick="return hs.htmlExpand(this, { objectType: \'iframe\''.$paras['overrideArr'].'  } )">
						'.$content.'
				  </a>';
		
		$html .= '</div>';			
		return $html;
	}
	
	function typeHsModules($paras, $content, $id_number, $id, $id_li){						
		$paras['content'] = '';
		$jsId = rand();
		
		$a = ''; 
		$positionArr = array();
		if(isset($paras['position'])) $positionArr =  preg_split ('/,/', $paras['position']);
		
		$mod_html = '';
		for($i=0; $i<count($positionArr); $i++){
			$list =  JModuleHelper::getModules(trim($positionArr[$i]));
			for($j=0; $j<count($list); $j++){
				if($list[$j]->module!='mod_zt_highslide'){
					$mod_html .= "<div class=\"modules-title\"><h3>".$list[$j]->title."</h3></div>";
					$mod_html .= "<div style=\"position:relative\">";	
					$mod_html .= JModuleHelper::renderModule($list[$j], array());
					$mod_html .= "</div>";	
					$mod_html .= "<br/>";
				}
			}
		}
					
		if($mod_html==''){
			$paras['content'] .= '<div class="highslide-body" style="text-align:center;width:90%;">
								<br/>
								Modue does not exist or is unpublished!
								<br/>
								<br/><br/>
						</div>';
		}
		else{
			$paras['content'] .= $mod_html;
		}
		return $this->hsCreateHtml($paras, $content, $id_number, $id, $id_li, $jsId);					
	}
	function hsCreateHtml($paras, $plugText, $id_number, $id, $id_li, $jsId){	
							
		if($paras['overrideArr']!=''){ $paras['overrideArr']=",".$paras['overrideArr']; }
		$html = '<a href="#zoo" id="'.$id_number.'" onclick="if(!hs.htmlExpand(this, { contentId: '.$id.$paras['overrideArr'].'})) display_none('.$id_li.'); return false;">'.$plugText.'</a>';

		$html .='<div class="highslide-html-content" id='.$id.'>';
		
		//Get plugin info
		$pluginHtml = JPluginHelper::getPlugin('content','zt_highslide');
		$pluginParams	= new JParameter( $pluginHtml->params);
		
		//if($pluginParams->get('showMove') == "1") $html .= $this->moveHs();
		$html .= $this->moveHs();
		
		$html .= '<div class="highslide-body" id="'.$jsId.'">';
		$html .= $paras['content'];
		
		//if($pluginParams->get('showClose') == "1") $html .= $this->closeHs();
		$html .= $this->closeHs();
		//else $html .= '</div></div>';
		
		return $html;
	}
	
	function typeHsHtml($paras, $plugText, $id_number, $id, $id_li){
		$paras['content'] = '';
		$jsId = rand(); 
		if(isset($paras['contentid'])&& $paras['contentid']!=''){
			$paras['content'] = "<script type='text/javascript'>
				if(document.getElementById('".$paras['contentid']."')!=null){
					document.getElementById('".$jsId."').innerHTML = document.getElementById('".$paras['contentid']."').innerHTML;
					document.getElementById('".$paras['contentid']."').innerHTML ='';
				}
			  </script>";
		}
		return $this->hsCreateHtml($paras, $plugText, $id_number, $id, $id_li, $jsId);
	}
	function moveHs(){
		$mv = '									    
				<div class="highslide-header" >										
					<ul >
						<li class="highslide-move">
							<a href="#" onclick="return false">Move</a>
						</li>											
					</ul>	    
				</div>';
		return $mv;
	}
	function closeHs(){
		$html = '</div>
					<hr/>
					<div class="highslide-footer" style="margin-top:-10px; height:23px;">
						<div>
							<span class="" title="Resize" >
								<span>
									<ul id="ul_footer">
											<li class="highslide-close" style="top:3px; float:right" >
												<a href="#" onclick="return hs.close(this)">Close</a>
											</li>
										</ul>	    
								</span>
							</span>
						</div>
					</div>
				</div>';	
		return $html;
	}
	
}