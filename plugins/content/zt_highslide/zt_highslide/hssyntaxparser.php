<?php
/**
 * @package ZT Highslide Plugin for Joomla! 1.5
 * @author http://www.ZooTemplate.com
 * @copyright (C) 2011- ZooTemplate.com
 * @license PHP files are GNU/GPL
**/
	// no direct access
	defined( '_JEXEC' ) or die( 'Restricted access' );
	
	class HighslideParser{
		
		var $_callback = '';
		
		function parseCustomTag($custom_tag){
			$arr_temp = preg_split('/zt_highslide/', $custom_tag);
			if(count($arr_temp)<2) return 0; //not tag
			if($arr_temp[0]=='{'){
				$tail = substr($arr_temp[1], strlen($arr_temp[1])-2, 2);
				if($tail != "/}") return 1; //open tag
				else return 2; //full tag
			}
			if($arr_temp[0]=='{/') return 3; //close tag
			return 0;
		}
		function parse($input_content, $callback){
			$this->_callback = $callback;
			$opened = false;
			$attributeArr = '';
			$tagContent = '';
			
			$split_patern = "/({[\/]?zt_highslide[^}]*})/";
			$arr_content = preg_split($split_patern, $input_content, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
			
			$output_content = '';
			foreach ($arr_content as $arr_item){
				if($this->parseCustomTag($arr_item) == 1){ //open tag
					if ($opened) {
		    			$output_content .= $this->callBack ($attributeArr, $tagContent);
		    			$opened = false;
		    		}
		    		$attributeArr = substr($arr_item, 13, strlen($arr_item)-14);
		    		$tagContent = '';
		    		$opened = true;
		    		
		    		continue;
				}
				if($this->parseCustomTag($arr_item) == 2){ //fulltag
					if ($opened) {
		    			$output_content .= $this->callBack ($attributeArr, $tagContent);
		    			$opened = false;
		    		}
		    		$attributeArr = substr($arr_item, 13,strlen(@$item)-14);
		    		$tagContent = '';
		    		$output_content .= $this->callBack ($attributeArr, $tagContent);
		    		continue;
				}
				if($this->parseCustomTag($arr_item) == 3){ //close tag
					$output_content .= $this->callBack ($attributeArr, $tagContent);
		  			$opened = false;
		    		continue;
				}
				if ($opened) {
		  			$tagContent .= $arr_item;
		  		} else {
		  			$output_content .= $arr_item;
		  		}	
			}
			
			return $output_content;
		}
		function callBack ($attributeArr, $tagContent) {
			if (is_array($this->_callback) && count($this->_callback) >= 2) {
				$callbackobj = $this->_callback[0];
				$callbackmethod = $this->_callback[1]; 
				return $callbackobj->$callbackmethod($attributeArr, $tagContent);
			} 
		}	
	}
?>