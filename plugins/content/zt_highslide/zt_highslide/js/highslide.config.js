/**
*	Site-specific configuration settings for Highslide JS
*/
//hs.graphicsDir = 'highslide/graphics/';
hs.outlineType = 'custom';
hs.dimmingOpacity = 0.5;
hs.fadeInOut = true;
hs.easing = 'easeInCirc';
hs.outlineWhileAnimating = 0;
hs.align = 'center';
hs.padToMinWidth = true;
hs.allowMultipleInstances = false;
hs.blockRightClick = true;
hs.numberOfImagesToPreload = 1;
hs.captionEval = 'this.a.title';
hs.captionOverlay.position = 'below';


// Add the slideshow controller

/* hs.addSlideshow({
	slideshowGroup: 'group1',
	interval: 5000,
	repeat: false,
	useControls: true,
	fixedControls: 'fit',
	overlayOptions: {
		className: 'text-controls',
		opacity: '0.95',
		position: 'bottom center',
		offsetX: '0',
		offsetY: '-60',
		relativeTo: 'viewport',
		hideOnMouseOut: false
	},
	thumbstrip: {
		mode: 'horizontal',
		position: 'bottom center',
		relativeTo: 'viewport'
	}

}); */

// gallery config object

/* var config1 = {
	slideshowGroup: 'group1',
	numberPosition: 'heading',
	transitions: ['expand', 'crossfade']
}; */
