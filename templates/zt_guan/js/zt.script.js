//JS script for Joomla template
window.addEvent('load', function(){

	var StyleCookie = new Hash.Cookie('ztGuanStyleCookieSite');
	var settings = { colors: '' };
	var style_1, style_2, style_3, style_4;
	new Asset.css(StyleCookie.get('colors'));

	/* Style 1 */
	if($('ztcolor1')){$('ztcolor1').addEvent('click', function(e) {
		e = new Event(e).stop();
		if (style_1) style_1.empty();
		new Asset.css(ztpathcolor + 'blue.css', {id: 'blue'});
		style_1 = $('blue');
		settings['colors'] = ztpathcolor + 'blue.css';
		StyleCookie.empty();
		StyleCookie.extend(settings);
	});}

	/* Style 2 */
	if($('ztcolor2')){$('ztcolor2').addEvent('click', function(e) {
		e = new Event(e).stop();
		if (style_2) style_2.empty();
		new Asset.css(ztpathcolor + 'green.css', {id: 'green'});
		style_2 = $('gray');
		settings['green'] = ztpathcolor + 'green.css';
		StyleCookie.empty();
		StyleCookie.extend(settings);
	});}

	/* Style 3 */
	if($('ztcolor3')){$('ztcolor3').addEvent('click', function(e) {
		e = new Event(e).stop();
		if (style_3) style_3.empty();
		new Asset.css(ztpathcolor + 'gray.css', {id: 'gray'});
		style_3 = $('gray');
		settings['colors'] = ztpathcolor + 'gray.css';
		StyleCookie.empty();
		StyleCookie.extend(settings);
	});}
	
	/* Style 4 */
	if($('ztcolor4')){$('ztcolor4').addEvent('click', function(e) {
		e = new Event(e).stop();
		if (style_4) style_4.empty();
		new Asset.css(ztpathcolor + 'violet.css', {id: 'violet'});
		style_4 = $('violet');
		settings['colors'] = ztpathcolor + 'violet.css';
		StyleCookie.empty();
		StyleCookie.extend(settings);
	});}

});