<?php
/**
 * @copyright	Copyright (C) 2008 - 2012 ZooTemplate.com. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
<head>
	<jdoc:include type="head" />
	<?php JHTML::_('behavior.mootools'); ?>
	<?php JHTML::_('behavior.caption', true); ?>
	<?php
		$document = JFactory::getDocument();
		$document->addStyleSheet($ztTools->baseurl() . 'templates/system/css/system.css');
		$document->addStyleSheet($ztTools->baseurl() . 'templates/system/css/general.css');
		$document->addStyleSheet($ztTools->templateurl() . 'css/default.css');
		$document->addStyleSheet($ztTools->templateurl() . 'css/template.css');
		$document->addStyleSheet($ztTools->templateurl() . 'css/typo.css');
		if($ztrtl == 'rtl') {
			$document->addStyleSheet($ztTools->templateurl() . 'css/template_rtl.css');
			$document->addStyleSheet($ztTools->templateurl() . 'css/typo_rtl.css');
		}
		if($this->params->get('zt_change_color')) {
			$document->addScript($ztTools->templateurl() . 'js/zt.script.js');
		}
		$document->addScript($ztTools->templateurl().'js/zt.script.js');
	?>
	<link href="<?php echo $ztTools->parse_ztcolor_cookie($ztcolorstyle); ?>" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		var baseurl = "<?php echo $ztTools->baseurl() ; ?>";
		var ztpathcolor = '<?php echo $ztTools->templateurl(); ?>/css/colors/';
		var tmplurl = '<?php echo $ztTools->templateurl();?>';
		var CurrentFontSize = parseInt('<?php echo $ztTools->getParam('zt_font');?>');
	</script>
	<!--[if lte IE 7]>
	<link rel="stylesheet" href="<?php echo $ztTools->templateurl(); ?>css/ie7.css" type="text/css" />
	<![endif]-->
</head>
<body id="bd" class="fs<?php echo $ztTools->getParam('zt_font'); ?> <?php echo $ztTools->getParam('zt_display'); ?> <?php echo $ztTools->getParam('zt_display_style'); ?> <?php echo $ztrtl; ?>">
<div id="zt-wrapper">
	<div id="zt-wrapper-inner">
	
	<div id="zt-header" class="clearfix">
		<div id="zt-header-inner">

			<?php if($this->countModules('top') || $this->countModules('topmenu') || $ztTools->getParam('zt_function')) : ?>
			<div id="zt-top">
				<div class="zt-wrapper">
					<?php if($this->countModules('top')) : ?>	
						<div id="zt-top-left">
							<jdoc:include type="modules" name="top" />
						</div>
					<?php endif; ?>

					<?php if($this->countModules('topmenu') || $ztTools->getParam('zt_function')) : ?>
						<div id="zt-top-right">
							<div id="zt-tools">
								<?php echo $changecolor; ?>
							</div>
							<?php if($this->countModules('topmenu')) : ?>
							<div id="zt-topmenu">
								<jdoc:include type="modules" name="topmenu" />
							</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>

			<div id="zt-userwrap1" class="png clearfix">
				<div class="zt-wrapper">
					<div id="zt-logo">
						<a class="logo" href="<?php echo $ztTools->baseurl() ; ?>" title="<?php echo $ztTools->sitename() ; ?>">
							<span><?php echo $ztTools->sitename() ; ?></span>
						</a>
						
					</div>
					<div id="zt-mainmenu" class="png"  >
						<div id="zt-mainmenu-inner" class="png">
							  <?php if($menustyle == 'split' || $menustyle == 'submoo') : ?>
							  <?php $menu->show(0,0); ?>
							  <?php else : ?>
								  <?php $menu->show(); ?>
							  <?php endif; ?>
						</div>
					</div>
				</div>
			</div>	
			
			<?php if($this->countModules('slideshow')) : ?>
			<div id="zt-userwrap3" class="clearfix">				
				<div class="zt-wrapper">
					<div id="zt-userwrap3-inner">
						<jdoc:include type="modules" name="slideshow" />
					</div>
				</div>
			</div>		
			<div id="zt-userwrap3-bottom" class="clearfix"></div>
			<?php endif; ?>
				
		</div>
	</div>
   
		
	
	
	
	<!-- MAINBODY -->
	<div id="zt-mainbody" class="clearfix">
		<div id="zt-mainbody-inner">
		
			<?php
			$spotlight = array ('user1','user2','user3','user4');
			$consl = $ztTools->calSpotlight($spotlight,$ztTools->isOP()?100:100,'%');
			if( $consl) :
			?>
			
			<div class="zt-wrapper">
			
				<?php if($this->countModules('user1')) : ?>
				<div id="zt-user1" class="zt-user zt-box<?php echo $consl['user1']['class']; ?>" style="width: <?php echo $consl['user1']['width']; ?>;">
					<div class="zt-box-inside">
						<jdoc:include type="modules" name="user1" style="ztxhtml" />
					</div>
				</div>
				<?php endif; ?>
                
				<?php if($this->countModules('user2')) : ?>
				<div id="zt-user2" class="zt-user zt-box<?php echo $consl['user2']['class']; ?>" style="width: <?php echo $consl['user2']['width']; ?>;">
					<div class="zt-box-inside">
						<jdoc:include type="modules" name="user2" style="ztxhtml" />
					</div>
				</div>
				<?php endif; ?>
                
				<?php if($this->countModules('user3')) : ?>
				<div id="zt-user3" class="zt-user zt-box<?php echo $consl['user3']['class']; ?>" style="width: <?php echo $consl['user3']['width']; ?>;">
					<div class="zt-box-inside">
						<jdoc:include type="modules" name="user3" style="ztxhtml" />
					</div>
				</div>
				<?php endif; ?>
                
                <?php if($this->countModules('user4')) : ?>
				<div id="zt-user4" class="zt-user zt-box<?php echo $consl['user4']['class']; ?>" style="width: <?php echo $consl['user4']['width']; ?>;">
					<div class="zt-box-inside">
						<jdoc:include type="modules" name="user4" style="ztxhtml" />
					</div>
				</div>
				<?php endif; ?>
		
			</div>
			<?php endif; ?>
			
			
			<!-- CONTAINER -->
			<div class="zt-wrapper png">
				<div id="zt-container<?php echo $zt_width;?>" class="clearfix">
					
					<div id="zt-content">
						<div id="zt-content-inner">
						
							<?php if($this->countModules('user5')) : ?>
								<div id="zt-user5" class="clearfix">
									<jdoc:include type="modules" name="user5" style="ztrounded" />
								</div>
							<?php endif; ?>
                                
							<div id="zt-component" class="clearfix">
								<jdoc:include type="message" />
								<jdoc:include type="component" />
							</div>
    
						   <?php if($this->countModules('user6')) : ?>
							   <div id="zt-user6" class="clearfix">
									<jdoc:include type="modules" name="user6" style="ztrounded" />
							   </div>
							<?php endif; ?>
                           
						</div>
					</div>
				
					<?php if($this->countModules('right')) : ?>
					<div id="zt-right">
						<div id="zt-right-inner">
							<jdoc:include type="modules" name="right" style="ztrounded" />
						</div>
					</div>
					<?php endif; ?>

				</div>	
			
			<!-- END CONTAINER -->
				</div>
		</div>
	</div>
	<!-- END MAINBODY -->
	 <?php
	$spotlight = array ('col1','col2','col3');
	$botsl1 = $ztTools->calSpotlight($spotlight,$ztTools->isOP()?100:99,'%');
	if( $botsl1 ) :
	?>
	<div id="zt-userwrap4" class="clearfix">
		<div class="zt-wrapper">
			<div id="zt-userwrap4-inner">
				<div class="zt-tc">
					<div class="zt-tl"></div>
					<div class="zt-tr"></div>
				</div>
				<div class="zt-c">
				<div class="zt-c2">
					<?php if($this->countModules('col1')) : ?>
						<div id="zt-col1" class="zt-user zt-box<?php echo $botsl1['col1']['class']; ?>" style="width: <?php echo $botsl1['col1']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="col1" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('col2')) : ?>
						<div id="zt-col2" class="zt-user zt-box<?php echo $botsl1['col2']['class']; ?>" style="width: <?php echo $botsl1['col2']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="col2" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('col3')) : ?>
						<div id="zt-col3" class="zt-user zt-box<?php echo $botsl1['col3']['class']; ?>" style="width: <?php echo $botsl1['col3']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="col3" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
				</div>
				</div>
			</div>	
		</div>
	</div>
	<?php endif; ?>
   
	<?php if($this->countModules('inset')) : ?>
	<div id="zt-inset" class="clearfix">
		<div class="zt-wrapper">
			<div id="zt-inset-inner">
				<jdoc:include type="modules" name="inset" style="ztxhtml" />
			</div>
		</div>
	</div>
	<?php endif; ?>

    <?php
		$spotlight = array ('user7','user8','user9','user10');
		$botsl2 = $ztTools->calSpotlight($spotlight,$ztTools->isOP()?100:99, '%');
		if( $botsl2 ) :
	?>
	<div id="zt-userwrap5" class="clearfix">
		<div class="zt-wrapper png">
			<div id="zt-userwrap5-inner" >
				<div class="zt-spotlight">
				
					<?php if($this->countModules('user7')): ?>
						<div id="zt-user7" class="zt-user zt-box<?php echo $botsl2['user7']['class']; ?>" style="width:<?php echo $botsl2['user7']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user7" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('user8')) : ?>
						<div id="zt-user8" class="zt-user zt-box<?php echo $botsl2['user8']['class']; ?>" style="width:<?php echo $botsl2['user8']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user8" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('user9')) : ?>
						<div id="zt-user9" class="zt-user zt-box<?php echo $botsl2['user9']['class']; ?>" style="width:<?php echo $botsl2['user9']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user9" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('user10')) : ?>
						<div id="zt-user10" class="zt-user zt-box<?php echo $botsl2['user10']['class']; ?>" style="width:<?php echo $botsl2['user10']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user10" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
				</div>
			</div>
			
		</div>
	</div>
	<?php endif; ?>			
	
	
    <div id="zt-bottom" class="clearfix">
		<div id="zt-bottom-inner">
			
			<div id="zt-userwrap6">
				<div class="zt-wrapper">
				<?php
				$spotlight = array ('user11','user12','user13','user14','user15');
				$botsl3 = $ztTools->calSpotlight ($spotlight,$ztTools->isOP()?100:100,'%');
				if( $botsl3 ) :
				?>
				
					<?php if($this->countModules('user11')) : ?>
						<div id="zt-user11" class="zt-user zt-box<?php echo $botsl3['user11']['class']; ?>" style="width: <?php echo $botsl3['user11']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user11" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('user12')) : ?>
						<div id="zt-user12" class="zt-user zt-box<?php echo $botsl3['user12']['class']; ?>" style="width: <?php echo $botsl3['user12']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user12" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('user13')) : ?>
						<div id="zt-user13" class="zt-user zt-box<?php echo $botsl3['user13']['class']; ?>" style="width: <?php echo $botsl3['user13']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user13" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('user14')) : ?>
						<div id="zt-user14" class="zt-user " style="width: <?php echo $botsl3['user14']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user14" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
					
					<?php if($this->countModules('user15')) : ?>
						<div id="zt-user15" class="zt-user " style="width: <?php echo $botsl3['user15']['width']; ?>;">
							<div class="zt-box-inside">
								<jdoc:include type="modules" name="user15" style="ztxhtml" />
							</div>
						</div>
					<?php endif; ?>
		
				<?php endif; ?>
			</div>

		</div>

		<div id="zt-userwrap7" class="png clearfix">
			<div class="zt-wrapper">
				<div id="zt-userwrap7-inner">
					<div id="zt-footer">
						<div id="zt-footer-inner">
							<jdoc:include type="modules" name="footer" />
						</div>
					</div>
					<div id="zt-copyright">
						<div id="zt-copyright-inner">
							Copyright &copy; <?php echo date('Y'); ?>. Quorum Ventures Limited. All rights reserved. <br />Website Powered by <a href="http://www.softzonegroup.com" title="Powered by Softzone Limited" target="_blank">Softzone Limited.</a>


						</div>
					</div>
				</div>
			</div>
		</div>	
			
		</div>
	</div>
	
	
	</div>
</div>

</body>
</html>