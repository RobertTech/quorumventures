<?php
/**
 * @package ZT Highslide Module for Joomla!
 * @author http://www.ZooTemplate.com
 * @copyright (C) 2011- ZooTemplate.com
 * @license PHP files are GNU/GPL
**/

defined('_JEXEC') or die( 'Restricted access' ); 
jimport('joomla.form.formfield');
class JFormFieldHsTypelist extends JFormField {

	var	$type = 'HsTypelist';

	function getInput(){
		return JElementHsTypelist::fetchElement($this->name, $this->value, $this->element, $this->options['control']);
	} 
} 
jimport('joomla.html.parameter.element');
class JElementHsTypelist extends JElement
{
	var	$_name = 'HsTypelist';
	function fetchElement($name, $value, &$node, $control_name)
	{	
		$options = array ();
		foreach ($node->children() as $option)
		{ 
			$val	= $option->getAttribute('value'); 
			$text	= $option->data();
			$options[] = JHTML::_('select.option', $val, JText::_($text));
		}
		$class = ( $node->attributes('class') ? 'class="'.$node->attributes('class').'"' : 'class="inputbox"' );
		$class .= " onchange=\"javascript: switchType(this)\""; 
		$strOptions = JHTML::_('select.genericlist',  $options, ''.$this->name.'', $class, 'value', 'text', $this->value, $control_name.$this->name);		
		  
		$strOptions .= "<script type=\"text/javascript\" language=\"javascript\">				
					function getElementsByTypeX (typeX) {
					  if (!typeX) return;
					  var els=[];					 
					  $$(document.adminForm.elements).each(function(el){ 
						if (el.id.test(typeX+'_')) els.push(el);
					  });
					  return els;
					}
					
					function switchType (slCtrol) {
						seldel = slCtrol.options[slCtrol.selectedIndex];
						groupsel = seldel.value.split('-')[0];
					  $$(slCtrol.options).each(function (el) {
					  	var typeX = el.value.split('-')[0];
  						var typeXs = getElementsByTypeX (typeX);
						//alert(typeXs);
  						if (!typeXs) return;
						var disabled = (groupsel==typeX)?'':'disabled';
  						typeXs.each(function(g){
  							g.disabled = disabled;
  						});
					  });
					}
					
					function disableAllEls(){
					  var selectct = $('jformparamstype');
					  switchType(selectct);
					}

					function initialize() {
					   disableAllEls();
					   document.adminForm.onsubmit = enableAllEls;
					}

					function enableAllEls(){
					  var selectct = $('jformparamstype');
					  $$(selectct.options).each(function (el) {
					  	var typeX = el.value.split('-')[0];
  						var typeXs = getElementsByTypeX (typeX);
  						if (!typeXs) return;
  						typeXs.each(function(g){
  							g.disabled = '';
  						});
					  });
					}

					window.addEvent('load', initialize);
				</script>"; 
		return $strOptions;
	}
}
?>
